from datetime import time
import unittest
from main import time_rounder, time_diff_calculator, time_window
from time_parse import parse_time


class TestTimeRounder(unittest.TestCase):
    
    # These tests will try to follow the readme.md for consistency
    def test_fifty_seven(self):

        this_time = time(hour=8, minute=57, second=0)
        result = time_rounder(this_time)
        desired_result = time(hour=9, minute=0, second=0)
        self.assertEquals(result, desired_result)

    def test_fifty_one(self):

        this_time = time(hour=8, minute=51, second=0)
        result = time_rounder(this_time)
        desired_result = time(hour=8, minute=54, second=0)
        self.assertEquals(result, desired_result)

    def test_eleven(self):

        this_time = time(hour=8, minute=11, second=0)
        result = time_rounder(this_time)
        desired_result = time(hour=8, minute=12, second=0)
        self.assertEquals(result, desired_result)

    def test_one(self):

        this_time = time(hour=8, minute=1, second=0)
        result = time_rounder(this_time)
        desired_result = time(hour=8, minute=0, second=0)
        self.assertEquals(result, desired_result)


    def test_zero(self):

        this_time = time(hour=8, minute=0, second=0)
        result = time_rounder(this_time)
        desired_result = time(hour=8, minute=0, second=0)
        self.assertEquals(result, desired_result)


    def test_forty_one(self):

        this_time = time(hour=8, minute=41, second=0)
        result = time_rounder(this_time)
        desired_result = time(hour=8, minute=42, second=0)
        self.assertEquals(result, desired_result)

class TestTimeDiffCalculator(unittest.TestCase):

    def test_one(self):
        out_time = time_diff_calculator(3.75, time(12, 0, 0))
        self.assertEquals(out_time, time(15, 48, 0))

    def test_two(self):
        out_time = time_diff_calculator(8, time(8, 0, 0))
        self.assertEquals(out_time, time(16, 0, 0))

    def test_three(self):
        out_time = time_diff_calculator(1.88, time(15, 21, 0))
        self.assertEquals(out_time, time(17, 12, 0))

    def test_four(self):
        out_time = time_diff_calculator(4.31, time(8, 6, 0))
        self.assertEquals(out_time, time(12, 24, 0))


    # Added these tests based on email from Hannah of real Novatime behavior.
    def test_five(self):
        out_time = time_diff_calculator(3.2, time(7, 50, 0))
        self.assertEquals(out_time, time(11, 0, 0))

    def test_six(self):
        out_time = time_diff_calculator(3.7, time(13, 20, 0))
        self.assertEquals(out_time, time(17, 0, 0))

    def test_seven(self):
        out_time = time_diff_calculator(3.1, time(7, 56, 0))
        self.assertEquals(out_time, time(11, 0, 0))

    def test_eight(self):
        out_time = time_diff_calculator(2.9, time(7, 51, 0))
        self.assertEquals(out_time, time(10, 48, 0))

    def test_nine(self):
        out_time = time_diff_calculator(5.2, time(11, 48, 0))
        self.assertEquals(out_time, time(17, 0, 0))

class TimeWindowTests(unittest.TestCase):
    def test_thirty_one(self):
        input_time = time(8,31,0)
        function_output = time_window(input_time)
        self.assertEquals(function_output, (time(8,27,0), time(8,32,0)))

    def test_fifty_six(self):
        input_time = time(8,56,0)
        function_output = time_window(input_time)
        self.assertEquals(function_output, (time(8,51,0), time(8,56,0)))

    def test_fifty_nine(self):
        input_time = time(8,59,0)
        function_output = time_window(input_time)
        self.assertEquals(function_output, (time(8,57,0), time(9,2,0)))

    def test_one(self):
        input_time = time(8,1,0)
        function_output = time_window(input_time)
        self.assertEquals(function_output, (time(7,57,0), time(8,2,0)))

    def test_three(self):
        input_time = time(8,3,0)
        function_output = time_window(input_time)
        self.assertEquals(function_output, (time(8,3,0), time(8,8,0)))

class TestTimeParser(unittest.TestCase):

    def test_700(self):
        result = parse_time('7:00AM')
        self.assertEqual(time(7,0,0), result)

    def test_1900(self):
        result = parse_time('7:00PM')
        self.assertEqual(time(19,0,0), result)

    def test_700_no_colon(self):
        result = parse_time('700')
        self.assertEqual(time(7,0,0), result)

    def test_735_no_colon(self):
        result = parse_time('735')
        self.assertEqual(time(7,35,0), result)

    def test_bad_ampm(self):
        result = parse_time('7:00AMampm')
        self.assertEqual(time(7,0,0), result)

    def test_mix_of_good_and_bad(self):
        result = parse_time('f 1249')
        self.assertEqual(time(12,49,0), result)

    def test_nine_pm_with_a_space(self):
        result = parse_time('6: 02       PM')
        self.assertEqual(time(18,2,0), result)

    def test_tons_of_extra_spaces(self):
        result = parse_time('9:00 PM')
        self.assertEqual(time(21,0,0), result)

    def test_leading_zeroes_with_valid_input(self):
        with self.assertRaises(ValueError):
            result = parse_time('00911AM')

    def test_noon_input(self):
        result = parse_time('12:09PM')
        self.assertEqual(time(12,9,0), result)

    def test_garbage_input(self):
        with self.assertRaises(ValueError):
                result = parse_time('asdfasdfASDFASDF')

    def test_military_time_with_pm(self):

        with self.assertRaises(ValueError):
            result = parse_time('1941am')