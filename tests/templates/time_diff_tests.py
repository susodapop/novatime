import unittest
from datetime import time
from main import time_diff_calculator

class TestTimeDiffGenerated(unittest.TestCase):
	{% for row in data %}
    def test{{ row[0] }}(self):
        out_time = time_diff_calculator({{row[5]}}, time({{row[1]}}, {{row[2]}}, 0))
        self.assertEquals(out_time, time({{row[3]}}, {{row[4]}}, 0))
    {% endfor %}