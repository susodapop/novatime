import unittest
from datetime import time
from main import time_diff_calculator

class TestTimeDiffGenerated(unittest.TestCase):
	
    def test0001(self):
        out_time = time_diff_calculator(3.8, time(8, 0, 0))
        self.assertEquals(out_time, time(11, 48, 0))
    
    def test0002(self):
        out_time = time_diff_calculator(5.3, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test0003(self):
        out_time = time_diff_calculator(5.2, time(7, 14, 0))
        self.assertEquals(out_time, time(12, 24, 0))
    
    def test0004(self):
        out_time = time_diff_calculator(3.4, time(13, 27, 0))
        self.assertEquals(out_time, time(16, 54, 0))
    
    def test0005(self):
        out_time = time_diff_calculator(4, time(7, 2, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0006(self):
        out_time = time_diff_calculator(4.5, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test0007(self):
        out_time = time_diff_calculator(3.9, time(7, 12, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0008(self):
        out_time = time_diff_calculator(6.2, time(12, 8, 0))
        self.assertEquals(out_time, time(18, 18, 0))
    
    def test0009(self):
        out_time = time_diff_calculator(14.3, time(6, 51, 0))
        self.assertEquals(out_time, time(21, 12, 0))
    
    def test0010(self):
        out_time = time_diff_calculator(5.3, time(6, 37, 0))
        self.assertEquals(out_time, time(11, 54, 0))
    
    def test0011(self):
        out_time = time_diff_calculator(3.5, time(12, 45, 0))
        self.assertEquals(out_time, time(16, 18, 0))
    
    def test0012(self):
        out_time = time_diff_calculator(5.2, time(7, 6, 0))
        self.assertEquals(out_time, time(12, 18, 0))
    
    def test0013(self):
        out_time = time_diff_calculator(3.5, time(12, 54, 0))
        self.assertEquals(out_time, time(16, 24, 0))
    
    def test0014(self):
        out_time = time_diff_calculator(4.9, time(7, 35, 0))
        self.assertEquals(out_time, time(12, 30, 0))
    
    def test0015(self):
        out_time = time_diff_calculator(6, time(12, 58, 0))
        self.assertEquals(out_time, time(19, 0, 0))
    
    def test0016(self):
        out_time = time_diff_calculator(5, time(7, 57, 0))
        self.assertEquals(out_time, time(13, 0, 0))
    
    def test0017(self):
        out_time = time_diff_calculator(4.2, time(13, 29, 0))
        self.assertEquals(out_time, time(17, 42, 0))
    
    def test0018(self):
        out_time = time_diff_calculator(4.2, time(7, 53, 0))
        self.assertEquals(out_time, time(12, 6, 0))
    
    def test0019(self):
        out_time = time_diff_calculator(4.1, time(12, 28, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test0020(self):
        out_time = time_diff_calculator(8.2, time(7, 5, 0))
        self.assertEquals(out_time, time(15, 18, 0))
    
    def test0021(self):
        out_time = time_diff_calculator(6.8, time(7, 9, 0))
        self.assertEquals(out_time, time(14, 0, 0))
    
    def test0022(self):
        out_time = time_diff_calculator(2.1, time(14, 23, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test0023(self):
        out_time = time_diff_calculator(6.2, time(7, 7, 0))
        self.assertEquals(out_time, time(13, 18, 0))
    
    def test0024(self):
        out_time = time_diff_calculator(3.6, time(13, 45, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test0025(self):
        out_time = time_diff_calculator(5.4, time(7, 17, 0))
        self.assertEquals(out_time, time(12, 42, 0))
    
    def test0026(self):
        out_time = time_diff_calculator(3.3, time(13, 20, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test0027(self):
        out_time = time_diff_calculator(5.6, time(7, 9, 0))
        self.assertEquals(out_time, time(12, 48, 0))
    
    def test0028(self):
        out_time = time_diff_calculator(4.3, time(13, 1, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test0029(self):
        out_time = time_diff_calculator(8.3, time(7, 7, 0))
        self.assertEquals(out_time, time(15, 24, 0))
    
    def test0030(self):
        out_time = time_diff_calculator(3.9, time(7, 7, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0031(self):
        out_time = time_diff_calculator(5.8, time(12, 24, 0))
        self.assertEquals(out_time, time(18, 12, 0))
    
    def test0032(self):
        out_time = time_diff_calculator(5.1, time(7, 11, 0))
        self.assertEquals(out_time, time(12, 18, 0))
    
    def test0033(self):
        out_time = time_diff_calculator(4.6, time(12, 49, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test0034(self):
        out_time = time_diff_calculator(6.7, time(7, 5, 0))
        self.assertEquals(out_time, time(13, 48, 0))
    
    def test0035(self):
        out_time = time_diff_calculator(3, time(14, 22, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test0036(self):
        out_time = time_diff_calculator(5.1, time(7, 58, 0))
        self.assertEquals(out_time, time(13, 6, 0))
    
    def test0037(self):
        out_time = time_diff_calculator(3.2, time(13, 59, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test0038(self):
        out_time = time_diff_calculator(5.6, time(7, 9, 0))
        self.assertEquals(out_time, time(12, 48, 0))
    
    def test0039(self):
        out_time = time_diff_calculator(2.7, time(13, 42, 0))
        self.assertEquals(out_time, time(16, 24, 0))
    
    def test0040(self):
        out_time = time_diff_calculator(7, time(7, 11, 0))
        self.assertEquals(out_time, time(14, 12, 0))
    
    def test0041(self):
        out_time = time_diff_calculator(2.5, time(14, 45, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test0042(self):
        out_time = time_diff_calculator(5.1, time(10, 19, 0))
        self.assertEquals(out_time, time(15, 24, 0))
    
    def test0043(self):
        out_time = time_diff_calculator(5.5, time(7, 14, 0))
        self.assertEquals(out_time, time(12, 42, 0))
    
    def test0044(self):
        out_time = time_diff_calculator(2.2, time(13, 15, 0))
        self.assertEquals(out_time, time(15, 30, 0))
    
    def test0045(self):
        out_time = time_diff_calculator(1.1, time(15, 29, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test0046(self):
        out_time = time_diff_calculator(5, time(7, 8, 0))
        self.assertEquals(out_time, time(12, 6, 0))
    
    def test0047(self):
        out_time = time_diff_calculator(3, time(12, 34, 0))
        self.assertEquals(out_time, time(15, 36, 0))
    
    def test0048(self):
        out_time = time_diff_calculator(5.5, time(7, 10, 0))
        self.assertEquals(out_time, time(12, 42, 0))
    
    def test0049(self):
        out_time = time_diff_calculator(2.5, time(13, 10, 0))
        self.assertEquals(out_time, time(15, 42, 0))
    
    def test0050(self):
        out_time = time_diff_calculator(4.9, time(7, 6, 0))
        self.assertEquals(out_time, time(12, 0, 0))
    
    def test0051(self):
        out_time = time_diff_calculator(3.1, time(12, 29, 0))
        self.assertEquals(out_time, time(15, 36, 0))
    
    def test0052(self):
        out_time = time_diff_calculator(7.1, time(7, 35, 0))
        self.assertEquals(out_time, time(14, 42, 0))
    
    def test0053(self):
        out_time = time_diff_calculator(1, time(15, 10, 0))
        self.assertEquals(out_time, time(16, 12, 0))
    
    def test0054(self):
        out_time = time_diff_calculator(3, time(7, 58, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0055(self):
        out_time = time_diff_calculator(4.9, time(12, 29, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test0056(self):
        out_time = time_diff_calculator(5.6, time(7, 55, 0))
        self.assertEquals(out_time, time(13, 30, 0))
    
    def test0057(self):
        out_time = time_diff_calculator(2.4, time(14, 21, 0))
        self.assertEquals(out_time, time(16, 48, 0))
    
    def test0058(self):
        out_time = time_diff_calculator(4.7, time(7, 47, 0))
        self.assertEquals(out_time, time(12, 30, 0))
    
    def test0059(self):
        out_time = time_diff_calculator(2, time(12, 43, 0))
        self.assertEquals(out_time, time(14, 42, 0))
    
    def test0060(self):
        out_time = time_diff_calculator(0.5, time(15, 17, 0))
        self.assertEquals(out_time, time(15, 48, 0))
    
    def test0061(self):
        out_time = time_diff_calculator(1.2, time(15, 48, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test0062(self):
        out_time = time_diff_calculator(4.9, time(8, 4, 0))
        self.assertEquals(out_time, time(13, 0, 0))
    
    def test0063(self):
        out_time = time_diff_calculator(3, time(14, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test0064(self):
        out_time = time_diff_calculator(5.6, time(7, 55, 0))
        self.assertEquals(out_time, time(13, 30, 0))
    
    def test0065(self):
        out_time = time_diff_calculator(3.2, time(14, 0, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test0066(self):
        out_time = time_diff_calculator(5.1, time(7, 10, 0))
        self.assertEquals(out_time, time(12, 18, 0))
    
    def test0067(self):
        out_time = time_diff_calculator(4.5, time(12, 49, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test0068(self):
        out_time = time_diff_calculator(3.9, time(7, 7, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0069(self):
        out_time = time_diff_calculator(4.7, time(7, 15, 0))
        self.assertEquals(out_time, time(12, 0, 0))
    
    def test0070(self):
        out_time = time_diff_calculator(4.8, time(12, 29, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test0071(self):
        out_time = time_diff_calculator(5.5, time(11, 54, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test0072(self):
        out_time = time_diff_calculator(5.5, time(7, 6, 0))
        self.assertEquals(out_time, time(12, 36, 0))
    
    def test0073(self):
        out_time = time_diff_calculator(4.1, time(13, 0, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test0074(self):
        out_time = time_diff_calculator(5.3, time(12, 6, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test0075(self):
        out_time = time_diff_calculator(6.9, time(7, 3, 0))
        self.assertEquals(out_time, time(14, 0, 0))
    
    def test0076(self):
        out_time = time_diff_calculator(5.2, time(6, 53, 0))
        self.assertEquals(out_time, time(12, 6, 0))
    
    def test0077(self):
        out_time = time_diff_calculator(5.2, time(12, 37, 0))
        self.assertEquals(out_time, time(17, 48, 0))
    
    def test0078(self):
        out_time = time_diff_calculator(5.3, time(12, 8, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test0079(self):
        out_time = time_diff_calculator(6.2, time(7, 5, 0))
        self.assertEquals(out_time, time(13, 18, 0))
    
    def test0080(self):
        out_time = time_diff_calculator(4, time(13, 50, 0))
        self.assertEquals(out_time, time(17, 48, 0))
    
    def test0081(self):
        out_time = time_diff_calculator(4.7, time(12, 11, 0))
        self.assertEquals(out_time, time(16, 54, 0))
    
    def test0082(self):
        out_time = time_diff_calculator(5, time(7, 7, 0))
        self.assertEquals(out_time, time(12, 6, 0))
    
    def test0083(self):
        out_time = time_diff_calculator(5.7, time(6, 48, 0))
        self.assertEquals(out_time, time(12, 30, 0))
    
    def test0084(self):
        out_time = time_diff_calculator(4.9, time(13, 0, 0))
        self.assertEquals(out_time, time(17, 54, 0))
    
    def test0085(self):
        out_time = time_diff_calculator(5.1, time(12, 9, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test0086(self):
        out_time = time_diff_calculator(6.5, time(6, 57, 0))
        self.assertEquals(out_time, time(13, 30, 0))
    
    def test0087(self):
        out_time = time_diff_calculator(3.1, time(13, 56, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test0088(self):
        out_time = time_diff_calculator(4.3, time(7, 41, 0))
        self.assertEquals(out_time, time(12, 0, 0))
    
    def test0089(self):
        out_time = time_diff_calculator(3.6, time(12, 41, 0))
        self.assertEquals(out_time, time(16, 18, 0))
    
    def test0090(self):
        out_time = time_diff_calculator(5.6, time(7, 5, 0))
        self.assertEquals(out_time, time(12, 42, 0))
    
    def test0091(self):
        out_time = time_diff_calculator(6, time(7, 10, 0))
        self.assertEquals(out_time, time(13, 12, 0))
    
    def test0092(self):
        out_time = time_diff_calculator(3.8, time(13, 38, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test0093(self):
        out_time = time_diff_calculator(5.5, time(12, 6, 0))
        self.assertEquals(out_time, time(17, 36, 0))
    
    def test0094(self):
        out_time = time_diff_calculator(4.8, time(7, 7, 0))
        self.assertEquals(out_time, time(11, 54, 0))
    
    def test0095(self):
        out_time = time_diff_calculator(4.9, time(12, 29, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test0096(self):
        out_time = time_diff_calculator(5.5, time(12, 10, 0))
        self.assertEquals(out_time, time(17, 42, 0))
    
    def test0097(self):
        out_time = time_diff_calculator(7.1, time(7, 7, 0))
        self.assertEquals(out_time, time(14, 12, 0))
    
    def test0098(self):
        out_time = time_diff_calculator(0, time(16, 58, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test0099(self):
        out_time = time_diff_calculator(8, time(7, 13, 0))
        self.assertEquals(out_time, time(15, 12, 0))
    
    def test0100(self):
        out_time = time_diff_calculator(1.6, time(15, 43, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test0101(self):
        out_time = time_diff_calculator(5.7, time(12, 9, 0))
        self.assertEquals(out_time, time(17, 54, 0))
    
    def test0102(self):
        out_time = time_diff_calculator(6.5, time(7, 6, 0))
        self.assertEquals(out_time, time(13, 36, 0))
    
    def test0103(self):
        out_time = time_diff_calculator(3.1, time(14, 8, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test0104(self):
        out_time = time_diff_calculator(5.9, time(12, 9, 0))
        self.assertEquals(out_time, time(18, 6, 0))
    
    def test0105(self):
        out_time = time_diff_calculator(4.8, time(6, 44, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test0106(self):
        out_time = time_diff_calculator(6.5, time(10, 58, 0))
        self.assertEquals(out_time, time(17, 30, 0))
    
    def test0107(self):
        out_time = time_diff_calculator(4, time(7, 7, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0108(self):
        out_time = time_diff_calculator(5.6, time(11, 39, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test0109(self):
        out_time = time_diff_calculator(5.3, time(12, 11, 0))
        self.assertEquals(out_time, time(17, 30, 0))
    
    def test0110(self):
        out_time = time_diff_calculator(5.5, time(6, 45, 0))
        self.assertEquals(out_time, time(12, 18, 0))
    
    def test0111(self):
        out_time = time_diff_calculator(4.7, time(7, 9, 0))
        self.assertEquals(out_time, time(11, 54, 0))
    
    def test0112(self):
        out_time = time_diff_calculator(5, time(12, 23, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test0113(self):
        out_time = time_diff_calculator(5.1, time(7, 10, 0))
        self.assertEquals(out_time, time(12, 18, 0))
    
    def test0114(self):
        out_time = time_diff_calculator(4.5, time(12, 47, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test0115(self):
        out_time = time_diff_calculator(5.8, time(11, 21, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test0116(self):
        out_time = time_diff_calculator(4.9, time(6, 41, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test0117(self):
        out_time = time_diff_calculator(4.8, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 48, 0))
    
    def test0118(self):
        out_time = time_diff_calculator(5.6, time(12, 4, 0))
        self.assertEquals(out_time, time(17, 42, 0))
    
    def test0119(self):
        out_time = time_diff_calculator(5, time(6, 40, 0))
        self.assertEquals(out_time, time(11, 42, 0))
    
    def test0120(self):
        out_time = time_diff_calculator(4, time(7, 41, 0))
        self.assertEquals(out_time, time(11, 42, 0))
    
    def test0121(self):
        out_time = time_diff_calculator(5.9, time(12, 17, 0))
        self.assertEquals(out_time, time(18, 12, 0))
    
    def test0122(self):
        out_time = time_diff_calculator(5.4, time(12, 4, 0))
        self.assertEquals(out_time, time(17, 30, 0))
    
    def test0123(self):
        out_time = time_diff_calculator(6, time(6, 24, 0))
        self.assertEquals(out_time, time(12, 24, 0))
    
    def test0124(self):
        out_time = time_diff_calculator(4.3, time(12, 56, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test0125(self):
        out_time = time_diff_calculator(5.3, time(11, 51, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test0126(self):
        out_time = time_diff_calculator(3.7, time(7, 7, 0))
        self.assertEquals(out_time, time(10, 48, 0))
    
    def test0127(self):
        out_time = time_diff_calculator(3.4, time(7, 56, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test0128(self):
        out_time = time_diff_calculator(4.1, time(11, 52, 0))
        self.assertEquals(out_time, time(16, 0, 0))
    
    def test0129(self):
        out_time = time_diff_calculator(3.7, time(7, 14, 0))
        self.assertEquals(out_time, time(10, 54, 0))
    
    def test0130(self):
        out_time = time_diff_calculator(4.4, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 24, 0))
    
    def test0131(self):
        out_time = time_diff_calculator(5, time(6, 43, 0))
        self.assertEquals(out_time, time(11, 42, 0))
    
    def test0132(self):
        out_time = time_diff_calculator(3.8, time(12, 14, 0))
        self.assertEquals(out_time, time(16, 0, 0))
    
    def test0133(self):
        out_time = time_diff_calculator(4.6, time(7, 6, 0))
        self.assertEquals(out_time, time(11, 42, 0))
    
    def test0134(self):
        out_time = time_diff_calculator(3.2, time(12, 16, 0))
        self.assertEquals(out_time, time(15, 30, 0))
    
    def test0135(self):
        out_time = time_diff_calculator(5.6, time(6, 37, 0))
        self.assertEquals(out_time, time(12, 12, 0))
    
    def test0136(self):
        out_time = time_diff_calculator(4, time(7, 0, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0137(self):
        out_time = time_diff_calculator(5.4, time(11, 30, 0))
        self.assertEquals(out_time, time(16, 54, 0))
    
    def test0138(self):
        out_time = time_diff_calculator(3.3, time(7, 13, 0))
        self.assertEquals(out_time, time(10, 30, 0))
    
    def test0139(self):
        out_time = time_diff_calculator(5.4, time(11, 54, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test0140(self):
        out_time = time_diff_calculator(4.4, time(7, 8, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test0141(self):
        out_time = time_diff_calculator(5.4, time(11, 59, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test0142(self):
        out_time = time_diff_calculator(2.6, time(15, 43, 0))
        self.assertEquals(out_time, time(18, 18, 0))
    
    def test0143(self):
        out_time = time_diff_calculator(4, time(7, 4, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0144(self):
        out_time = time_diff_calculator(5.5, time(11, 41, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test0145(self):
        out_time = time_diff_calculator(4.1, time(7, 7, 0))
        self.assertEquals(out_time, time(11, 12, 0))
    
    def test0146(self):
        out_time = time_diff_calculator(5.6, time(11, 44, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test0147(self):
        out_time = time_diff_calculator(5.4, time(11, 52, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test0148(self):
        out_time = time_diff_calculator(4.4, time(6, 51, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test0149(self):
        out_time = time_diff_calculator(6, time(11, 48, 0))
        self.assertEquals(out_time, time(17, 48, 0))
    
    def test0150(self):
        out_time = time_diff_calculator(5.1, time(12, 14, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test0151(self):
        out_time = time_diff_calculator(4.5, time(7, 22, 0))
        self.assertEquals(out_time, time(11, 54, 0))
    
    def test0152(self):
        out_time = time_diff_calculator(4.9, time(12, 26, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test0153(self):
        out_time = time_diff_calculator(4, time(7, 8, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0154(self):
        out_time = time_diff_calculator(5.7, time(11, 38, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test0155(self):
        out_time = time_diff_calculator(5.2, time(11, 52, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test0156(self):
        out_time = time_diff_calculator(5.1, time(7, 22, 0))
        self.assertEquals(out_time, time(12, 30, 0))
    
    def test0157(self):
        out_time = time_diff_calculator(4.5, time(12, 55, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test0158(self):
        out_time = time_diff_calculator(6.6, time(10, 57, 0))
        self.assertEquals(out_time, time(17, 36, 0))
    
    def test0159(self):
        out_time = time_diff_calculator(4.6, time(6, 43, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test0160(self):
        out_time = time_diff_calculator(4.6, time(7, 8, 0))
        self.assertEquals(out_time, time(11, 42, 0))
    
    def test0161(self):
        out_time = time_diff_calculator(5.1, time(12, 11, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test0162(self):
        out_time = time_diff_calculator(5.9, time(11, 20, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test0163(self):
        out_time = time_diff_calculator(5.3, time(6, 43, 0))
        self.assertEquals(out_time, time(12, 0, 0))
    
    def test0164(self):
        out_time = time_diff_calculator(5.2, time(12, 36, 0))
        self.assertEquals(out_time, time(17, 48, 0))
    
    def test0165(self):
        out_time = time_diff_calculator(5.4, time(12, 5, 0))
        self.assertEquals(out_time, time(17, 30, 0))
    
    def test0166(self):
        out_time = time_diff_calculator(7.9, time(7, 7, 0))
        self.assertEquals(out_time, time(15, 0, 0))
    
    def test0167(self):
        out_time = time_diff_calculator(3.4, time(7, 9, 0))
        self.assertEquals(out_time, time(10, 36, 0))
    
    def test0168(self):
        out_time = time_diff_calculator(5.8, time(11, 23, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test0169(self):
        out_time = time_diff_calculator(3.9, time(7, 8, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0170(self):
        out_time = time_diff_calculator(5.9, time(11, 37, 0))
        self.assertEquals(out_time, time(17, 30, 0))
    
    def test0171(self):
        out_time = time_diff_calculator(4.9, time(12, 12, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test0172(self):
        out_time = time_diff_calculator(4.4, time(7, 6, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test0173(self):
        out_time = time_diff_calculator(4, time(7, 7, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0174(self):
        out_time = time_diff_calculator(5.7, time(11, 36, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test0175(self):
        out_time = time_diff_calculator(5.1, time(12, 12, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test0176(self):
        out_time = time_diff_calculator(5.7, time(6, 41, 0))
        self.assertEquals(out_time, time(12, 24, 0))
    
    def test0177(self):
        out_time = time_diff_calculator(4.1, time(12, 57, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test0178(self):
        out_time = time_diff_calculator(5.1, time(12, 10, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test0179(self):
        out_time = time_diff_calculator(4.6, time(6, 45, 0))
        self.assertEquals(out_time, time(11, 24, 0))
    
    def test0180(self):
        out_time = time_diff_calculator(0, time(7, 7, 0))
        self.assertEquals(out_time, time(7, 6, 0))
    
    def test0181(self):
        out_time = time_diff_calculator(4.5, time(7, 7, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test0182(self):
        out_time = time_diff_calculator(5.2, time(12, 8, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test0183(self):
        out_time = time_diff_calculator(6.6, time(10, 42, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test0184(self):
        out_time = time_diff_calculator(6.3, time(10, 51, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test0185(self):
        out_time = time_diff_calculator(7.6, time(6, 44, 0))
        self.assertEquals(out_time, time(14, 18, 0))
    
    def test0186(self):
        out_time = time_diff_calculator(1.8, time(14, 45, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test0187(self):
        out_time = time_diff_calculator(5.4, time(7, 19, 0))
        self.assertEquals(out_time, time(12, 42, 0))
    
    def test0188(self):
        out_time = time_diff_calculator(3.5, time(13, 7, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test0189(self):
        out_time = time_diff_calculator(4.2, time(6, 47, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0190(self):
        out_time = time_diff_calculator(5, time(11, 28, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test0191(self):
        out_time = time_diff_calculator(4.9, time(6, 44, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test0192(self):
        out_time = time_diff_calculator(4.1, time(11, 58, 0))
        self.assertEquals(out_time, time(16, 6, 0))
    
    def test0193(self):
        out_time = time_diff_calculator(5, time(7, 33, 0))
        self.assertEquals(out_time, time(12, 36, 0))
    
    def test0194(self):
        out_time = time_diff_calculator(5.4, time(12, 56, 0))
        self.assertEquals(out_time, time(18, 18, 0))
    
    def test0195(self):
        out_time = time_diff_calculator(6.2, time(7, 10, 0))
        self.assertEquals(out_time, time(13, 24, 0))
    
    def test0196(self):
        out_time = time_diff_calculator(0.2, time(14, 3, 0))
        self.assertEquals(out_time, time(14, 18, 0))
    
    def test0197(self):
        out_time = time_diff_calculator(3.5, time(7, 50, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test0198(self):
        out_time = time_diff_calculator(5.4, time(11, 50, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test0199(self):
        out_time = time_diff_calculator(3.9, time(7, 9, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0200(self):
        out_time = time_diff_calculator(4.4, time(12, 4, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test0201(self):
        out_time = time_diff_calculator(4, time(7, 13, 0))
        self.assertEquals(out_time, time(11, 12, 0))
    
    def test0202(self):
        out_time = time_diff_calculator(4.1, time(11, 47, 0))
        self.assertEquals(out_time, time(15, 54, 0))
    
    def test0203(self):
        out_time = time_diff_calculator(4.2, time(7, 10, 0))
        self.assertEquals(out_time, time(11, 24, 0))
    
    def test0204(self):
        out_time = time_diff_calculator(4.5, time(12, 0, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test0205(self):
        out_time = time_diff_calculator(3.9, time(7, 59, 0))
        self.assertEquals(out_time, time(11, 54, 0))
    
    def test0206(self):
        out_time = time_diff_calculator(5.1, time(12, 26, 0))
        self.assertEquals(out_time, time(17, 30, 0))
    
    def test0207(self):
        out_time = time_diff_calculator(3.5, time(7, 27, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0208(self):
        out_time = time_diff_calculator(5.4, time(11, 31, 0))
        self.assertEquals(out_time, time(16, 54, 0))
    
    def test0209(self):
        out_time = time_diff_calculator(3.9, time(7, 6, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0210(self):
        out_time = time_diff_calculator(5.6, time(11, 33, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test0211(self):
        out_time = time_diff_calculator(3.5, time(7, 27, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0212(self):
        out_time = time_diff_calculator(5.2, time(11, 33, 0))
        self.assertEquals(out_time, time(16, 48, 0))
    
    def test0213(self):
        out_time = time_diff_calculator(4.6, time(6, 40, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test0214(self):
        out_time = time_diff_calculator(4.5, time(12, 7, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test0215(self):
        out_time = time_diff_calculator(3.7, time(7, 15, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0216(self):
        out_time = time_diff_calculator(5, time(11, 59, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test0217(self):
        out_time = time_diff_calculator(4, time(7, 6, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0218(self):
        out_time = time_diff_calculator(4.3, time(11, 45, 0))
        self.assertEquals(out_time, time(16, 6, 0))
    
    def test0219(self):
        out_time = time_diff_calculator(4.2, time(6, 47, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0220(self):
        out_time = time_diff_calculator(5.4, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test0221(self):
        out_time = time_diff_calculator(3.7, time(7, 22, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0222(self):
        out_time = time_diff_calculator(5, time(12, 5, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test0223(self):
        out_time = time_diff_calculator(3.9, time(7, 11, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0224(self):
        out_time = time_diff_calculator(4.7, time(11, 46, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test0225(self):
        out_time = time_diff_calculator(4.1, time(7, 15, 0))
        self.assertEquals(out_time, time(11, 24, 0))
    
    def test0226(self):
        out_time = time_diff_calculator(5.5, time(11, 49, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test0227(self):
        out_time = time_diff_calculator(4.3, time(6, 42, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0228(self):
        out_time = time_diff_calculator(5.5, time(11, 34, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test0229(self):
        out_time = time_diff_calculator(4, time(8, 0, 0))
        self.assertEquals(out_time, time(12, 0, 0))
    
    def test0230(self):
        out_time = time_diff_calculator(4, time(13, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test0231(self):
        out_time = time_diff_calculator(4, time(8, 0, 0))
        self.assertEquals(out_time, time(12, 0, 0))
    
    def test0232(self):
        out_time = time_diff_calculator(4, time(13, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test0233(self):
        out_time = time_diff_calculator(4, time(8, 0, 0))
        self.assertEquals(out_time, time(12, 0, 0))
    
    def test0234(self):
        out_time = time_diff_calculator(4, time(13, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test0235(self):
        out_time = time_diff_calculator(4, time(7, 15, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test0236(self):
        out_time = time_diff_calculator(4.8, time(11, 48, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test0237(self):
        out_time = time_diff_calculator(4.3, time(6, 47, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0238(self):
        out_time = time_diff_calculator(4.3, time(11, 36, 0))
        self.assertEquals(out_time, time(15, 54, 0))
    
    def test0239(self):
        out_time = time_diff_calculator(3.9, time(7, 12, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0240(self):
        out_time = time_diff_calculator(3.9, time(12, 2, 0))
        self.assertEquals(out_time, time(15, 54, 0))
    
    def test0241(self):
        out_time = time_diff_calculator(3.7, time(7, 19, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0242(self):
        out_time = time_diff_calculator(5.2, time(11, 45, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test0243(self):
        out_time = time_diff_calculator(4.1, time(7, 5, 0))
        self.assertEquals(out_time, time(11, 12, 0))
    
    def test0244(self):
        out_time = time_diff_calculator(5.4, time(11, 42, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test0245(self):
        out_time = time_diff_calculator(3.9, time(7, 8, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0246(self):
        out_time = time_diff_calculator(5.3, time(11, 53, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test0247(self):
        out_time = time_diff_calculator(4.6, time(7, 9, 0))
        self.assertEquals(out_time, time(11, 48, 0))
    
    def test0248(self):
        out_time = time_diff_calculator(4.2, time(12, 17, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test0249(self):
        out_time = time_diff_calculator(4.4, time(8, 5, 0))
        self.assertEquals(out_time, time(12, 30, 0))
    
    def test0250(self):
        out_time = time_diff_calculator(3.7, time(12, 58, 0))
        self.assertEquals(out_time, time(16, 42, 0))
    
    def test0251(self):
        out_time = time_diff_calculator(4.1, time(7, 20, 0))
        self.assertEquals(out_time, time(11, 24, 0))
    
    def test0252(self):
        out_time = time_diff_calculator(5.7, time(11, 56, 0))
        self.assertEquals(out_time, time(17, 36, 0))
    
    def test0253(self):
        out_time = time_diff_calculator(3.5, time(7, 40, 0))
        self.assertEquals(out_time, time(11, 12, 0))
    
    def test0254(self):
        out_time = time_diff_calculator(5.4, time(11, 49, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test0255(self):
        out_time = time_diff_calculator(4, time(7, 7, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0256(self):
        out_time = time_diff_calculator(5.4, time(11, 43, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test0257(self):
        out_time = time_diff_calculator(3.5, time(7, 48, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test0258(self):
        out_time = time_diff_calculator(3.8, time(11, 44, 0))
        self.assertEquals(out_time, time(15, 30, 0))
    
    def test0259(self):
        out_time = time_diff_calculator(4, time(7, 11, 0))
        self.assertEquals(out_time, time(11, 12, 0))
    
    def test0260(self):
        out_time = time_diff_calculator(5.5, time(11, 46, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test0261(self):
        out_time = time_diff_calculator(4.5, time(7, 12, 0))
        self.assertEquals(out_time, time(11, 42, 0))
    
    def test0262(self):
        out_time = time_diff_calculator(5, time(12, 16, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test0263(self):
        out_time = time_diff_calculator(4.8, time(7, 10, 0))
        self.assertEquals(out_time, time(12, 0, 0))
    
    def test0264(self):
        out_time = time_diff_calculator(4.3, time(12, 44, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test0265(self):
        out_time = time_diff_calculator(4.1, time(7, 14, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test0266(self):
        out_time = time_diff_calculator(4.4, time(11, 38, 0))
        self.assertEquals(out_time, time(16, 0, 0))
    
    def test0267(self):
        out_time = time_diff_calculator(4.7, time(7, 46, 0))
        self.assertEquals(out_time, time(12, 30, 0))
    
    def test0268(self):
        out_time = time_diff_calculator(3.7, time(13, 19, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test0269(self):
        out_time = time_diff_calculator(3.7, time(7, 48, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test0270(self):
        out_time = time_diff_calculator(5.2, time(11, 56, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test0271(self):
        out_time = time_diff_calculator(7.4, time(8, 0, 0))
        self.assertEquals(out_time, time(15, 24, 0))
    
    def test0272(self):
        out_time = time_diff_calculator(0.7, time(15, 53, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test0273(self):
        out_time = time_diff_calculator(8.3, time(7, 26, 0))
        self.assertEquals(out_time, time(15, 42, 0))
    
    def test0274(self):
        out_time = time_diff_calculator(0.9, time(16, 5, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test0275(self):
        out_time = time_diff_calculator(6.2, time(7, 27, 0))
        self.assertEquals(out_time, time(13, 42, 0))
    
    def test0276(self):
        out_time = time_diff_calculator(2.8, time(14, 11, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test0277(self):
        out_time = time_diff_calculator(4.1, time(7, 22, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test0278(self):
        out_time = time_diff_calculator(4.1, time(12, 30, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test0279(self):
        out_time = time_diff_calculator(3.2, time(7, 52, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0280(self):
        out_time = time_diff_calculator(4.9, time(11, 38, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test0281(self):
        out_time = time_diff_calculator(3.6, time(7, 54, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test0282(self):
        out_time = time_diff_calculator(4.7, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 42, 0))
    
    def test0283(self):
        out_time = time_diff_calculator(4.3, time(7, 16, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test0284(self):
        out_time = time_diff_calculator(3.4, time(12, 5, 0))
        self.assertEquals(out_time, time(15, 30, 0))
    
    def test0285(self):
        out_time = time_diff_calculator(3.5, time(7, 50, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test0286(self):
        out_time = time_diff_calculator(4.6, time(12, 5, 0))
        self.assertEquals(out_time, time(16, 42, 0))
    
    def test0287(self):
        out_time = time_diff_calculator(3.7, time(7, 54, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test0288(self):
        out_time = time_diff_calculator(4.5, time(12, 2, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test0289(self):
        out_time = time_diff_calculator(3.7, time(7, 51, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test0290(self):
        out_time = time_diff_calculator(4.1, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 6, 0))
    
    def test0291(self):
        out_time = time_diff_calculator(3.5, time(7, 29, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0292(self):
        out_time = time_diff_calculator(5.4, time(11, 39, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test0293(self):
        out_time = time_diff_calculator(3.4, time(7, 44, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0294(self):
        out_time = time_diff_calculator(4.9, time(11, 51, 0))
        self.assertEquals(out_time, time(16, 48, 0))
    
    def test0295(self):
        out_time = time_diff_calculator(5, time(7, 24, 0))
        self.assertEquals(out_time, time(12, 24, 0))
    
    def test0296(self):
        out_time = time_diff_calculator(2.2, time(14, 25, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test0297(self):
        out_time = time_diff_calculator(3.2, time(7, 53, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0298(self):
        out_time = time_diff_calculator(4.8, time(11, 41, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test0299(self):
        out_time = time_diff_calculator(3.6, time(7, 47, 0))
        self.assertEquals(out_time, time(11, 24, 0))
    
    def test0300(self):
        out_time = time_diff_calculator(5, time(12, 3, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test0301(self):
        out_time = time_diff_calculator(4.2, time(7, 47, 0))
        self.assertEquals(out_time, time(12, 0, 0))
    
    def test0302(self):
        out_time = time_diff_calculator(4.1, time(12, 30, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test0303(self):
        out_time = time_diff_calculator(3.3, time(7, 53, 0))
        self.assertEquals(out_time, time(11, 12, 0))
    
    def test0304(self):
        out_time = time_diff_calculator(5.2, time(11, 56, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test0305(self):
        out_time = time_diff_calculator(3.4, time(7, 55, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test0306(self):
        out_time = time_diff_calculator(4.1, time(11, 53, 0))
        self.assertEquals(out_time, time(16, 0, 0))
    
    def test0307(self):
        out_time = time_diff_calculator(5.8, time(7, 52, 0))
        self.assertEquals(out_time, time(13, 42, 0))
    
    def test0308(self):
        out_time = time_diff_calculator(2.5, time(14, 31, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test0309(self):
        out_time = time_diff_calculator(3.2, time(7, 53, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0310(self):
        out_time = time_diff_calculator(4.6, time(11, 58, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test0311(self):
        out_time = time_diff_calculator(3.3, time(7, 50, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0312(self):
        out_time = time_diff_calculator(5.1, time(11, 57, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test0313(self):
        out_time = time_diff_calculator(3.6, time(7, 51, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test0314(self):
        out_time = time_diff_calculator(4.7, time(12, 22, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test0315(self):
        out_time = time_diff_calculator(3.3, time(7, 58, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test0316(self):
        out_time = time_diff_calculator(4.4, time(12, 7, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test0317(self):
        out_time = time_diff_calculator(3, time(7, 57, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0318(self):
        out_time = time_diff_calculator(4.9, time(11, 38, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test0319(self):
        out_time = time_diff_calculator(3.5, time(7, 50, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test0320(self):
        out_time = time_diff_calculator(5.1, time(11, 57, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test0321(self):
        out_time = time_diff_calculator(4.1, time(7, 53, 0))
        self.assertEquals(out_time, time(12, 0, 0))
    
    def test0322(self):
        out_time = time_diff_calculator(4, time(12, 37, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test0323(self):
        out_time = time_diff_calculator(3.6, time(7, 51, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test0324(self):
        out_time = time_diff_calculator(4.6, time(12, 4, 0))
        self.assertEquals(out_time, time(16, 42, 0))
    
    def test0325(self):
        out_time = time_diff_calculator(3.3, time(7, 47, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0326(self):
        out_time = time_diff_calculator(4.7, time(11, 50, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test0327(self):
        out_time = time_diff_calculator(3, time(7, 51, 0))
        self.assertEquals(out_time, time(10, 54, 0))
    
    def test0328(self):
        out_time = time_diff_calculator(5.5, time(11, 43, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test0329(self):
        out_time = time_diff_calculator(3.4, time(7, 54, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test0330(self):
        out_time = time_diff_calculator(5.6, time(11, 51, 0))
        self.assertEquals(out_time, time(17, 30, 0))
    
    def test0331(self):
        out_time = time_diff_calculator(3.6, time(7, 53, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test0332(self):
        out_time = time_diff_calculator(4.6, time(12, 0, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test0333(self):
        out_time = time_diff_calculator(3.6, time(8, 1, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test0334(self):
        out_time = time_diff_calculator(4.5, time(12, 7, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test0335(self):
        out_time = time_diff_calculator(3.5, time(7, 50, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test0336(self):
        out_time = time_diff_calculator(4.6, time(11, 55, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test0337(self):
        out_time = time_diff_calculator(3.9, time(7, 40, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test0338(self):
        out_time = time_diff_calculator(4.9, time(12, 6, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test0339(self):
        out_time = time_diff_calculator(3.3, time(7, 46, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0340(self):
        out_time = time_diff_calculator(5, time(11, 41, 0))
        self.assertEquals(out_time, time(16, 42, 0))
    
    def test0341(self):
        out_time = time_diff_calculator(3.1, time(7, 55, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0342(self):
        out_time = time_diff_calculator(5.8, time(11, 34, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test0343(self):
        out_time = time_diff_calculator(2.4, time(7, 49, 0))
        self.assertEquals(out_time, time(10, 12, 0))
    
    def test0344(self):
        out_time = time_diff_calculator(5, time(12, 16, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test0345(self):
        out_time = time_diff_calculator(3.5, time(7, 50, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test0346(self):
        out_time = time_diff_calculator(5.2, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test0347(self):
        out_time = time_diff_calculator(3.4, time(7, 48, 0))
        self.assertEquals(out_time, time(11, 12, 0))
    
    def test0348(self):
        out_time = time_diff_calculator(5.2, time(12, 5, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test0349(self):
        out_time = time_diff_calculator(3.4, time(7, 50, 0))
        self.assertEquals(out_time, time(11, 12, 0))
    
    def test0350(self):
        out_time = time_diff_calculator(4.6, time(12, 34, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test0351(self):
        out_time = time_diff_calculator(3.6, time(7, 54, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test0352(self):
        out_time = time_diff_calculator(5, time(12, 5, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test0353(self):
        out_time = time_diff_calculator(3.3, time(7, 52, 0))
        self.assertEquals(out_time, time(11, 12, 0))
    
    def test0354(self):
        out_time = time_diff_calculator(4.8, time(11, 42, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test0355(self):
        out_time = time_diff_calculator(4.1, time(7, 15, 0))
        self.assertEquals(out_time, time(11, 24, 0))
    
    def test0356(self):
        out_time = time_diff_calculator(5, time(11, 49, 0))
        self.assertEquals(out_time, time(16, 48, 0))
    
    def test0357(self):
        out_time = time_diff_calculator(4.2, time(7, 12, 0))
        self.assertEquals(out_time, time(11, 24, 0))
    
    def test0358(self):
        out_time = time_diff_calculator(5.2, time(11, 51, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test0359(self):
        out_time = time_diff_calculator(3.7, time(7, 48, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test0360(self):
        out_time = time_diff_calculator(4.9, time(11, 59, 0))
        self.assertEquals(out_time, time(16, 54, 0))
    
    def test0361(self):
        out_time = time_diff_calculator(4.2, time(7, 15, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test0362(self):
        out_time = time_diff_calculator(4.8, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 48, 0))
    
    def test0363(self):
        out_time = time_diff_calculator(4.9, time(7, 12, 0))
        self.assertEquals(out_time, time(12, 6, 0))
    
    def test0364(self):
        out_time = time_diff_calculator(3.9, time(12, 33, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test0365(self):
        out_time = time_diff_calculator(3.7, time(7, 18, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0366(self):
        out_time = time_diff_calculator(5.9, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test0367(self):
        out_time = time_diff_calculator(4.6, time(7, 1, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test0368(self):
        out_time = time_diff_calculator(5.3, time(12, 8, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test0369(self):
        out_time = time_diff_calculator(4, time(7, 7, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0370(self):
        out_time = time_diff_calculator(5.6, time(11, 33, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test0371(self):
        out_time = time_diff_calculator(4.8, time(7, 3, 0))
        self.assertEquals(out_time, time(11, 54, 0))
    
    def test0372(self):
        out_time = time_diff_calculator(4.7, time(12, 17, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test0373(self):
        out_time = time_diff_calculator(4.1, time(7, 11, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test0374(self):
        out_time = time_diff_calculator(5.6, time(11, 45, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test0375(self):
        out_time = time_diff_calculator(4.5, time(6, 38, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0376(self):
        out_time = time_diff_calculator(6.4, time(11, 32, 0))
        self.assertEquals(out_time, time(17, 54, 0))
    
    def test0377(self):
        out_time = time_diff_calculator(4.6, time(7, 51, 0))
        self.assertEquals(out_time, time(12, 30, 0))
    
    def test0378(self):
        out_time = time_diff_calculator(4.4, time(13, 9, 0))
        self.assertEquals(out_time, time(17, 36, 0))
    
    def test0379(self):
        out_time = time_diff_calculator(3.5, time(7, 49, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test0380(self):
        out_time = time_diff_calculator(4.9, time(11, 51, 0))
        self.assertEquals(out_time, time(16, 48, 0))
    
    def test0381(self):
        out_time = time_diff_calculator(3.4, time(7, 49, 0))
        self.assertEquals(out_time, time(11, 12, 0))
    
    def test0382(self):
        out_time = time_diff_calculator(5, time(11, 48, 0))
        self.assertEquals(out_time, time(16, 48, 0))
    
    def test0383(self):
        out_time = time_diff_calculator(3.9, time(7, 56, 0))
        self.assertEquals(out_time, time(11, 48, 0))
    
    def test0384(self):
        out_time = time_diff_calculator(4.2, time(12, 17, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test0385(self):
        out_time = time_diff_calculator(3.9, time(7, 45, 0))
        self.assertEquals(out_time, time(11, 42, 0))
    
    def test0386(self):
        out_time = time_diff_calculator(5.2, time(12, 15, 0))
        self.assertEquals(out_time, time(17, 30, 0))
    
    def test0387(self):
        out_time = time_diff_calculator(3.7, time(7, 50, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test0388(self):
        out_time = time_diff_calculator(4.7, time(12, 15, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test0389(self):
        out_time = time_diff_calculator(3.4, time(7, 54, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test0390(self):
        out_time = time_diff_calculator(4.7, time(11, 48, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test0391(self):
        out_time = time_diff_calculator(4.3, time(7, 44, 0))
        self.assertEquals(out_time, time(12, 0, 0))
    
    def test0392(self):
        out_time = time_diff_calculator(3, time(13, 0, 0))
        self.assertEquals(out_time, time(16, 0, 0))
    
    def test0393(self):
        out_time = time_diff_calculator(4, time(7, 49, 0))
        self.assertEquals(out_time, time(11, 48, 0))
    
    def test0394(self):
        out_time = time_diff_calculator(4.3, time(12, 19, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test0395(self):
        out_time = time_diff_calculator(3.5, time(7, 50, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test0396(self):
        out_time = time_diff_calculator(5.3, time(11, 50, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test0397(self):
        out_time = time_diff_calculator(5.6, time(7, 51, 0))
        self.assertEquals(out_time, time(13, 30, 0))
    
    def test0398(self):
        out_time = time_diff_calculator(3.1, time(14, 1, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test0399(self):
        out_time = time_diff_calculator(3.2, time(7, 46, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0400(self):
        out_time = time_diff_calculator(5.1, time(11, 32, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test0401(self):
        out_time = time_diff_calculator(3.8, time(7, 58, 0))
        self.assertEquals(out_time, time(11, 48, 0))
    
    def test0402(self):
        out_time = time_diff_calculator(4.3, time(12, 18, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test0403(self):
        out_time = time_diff_calculator(4.1, time(7, 51, 0))
        self.assertEquals(out_time, time(12, 0, 0))
    
    def test0404(self):
        out_time = time_diff_calculator(4.1, time(12, 30, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test0405(self):
        out_time = time_diff_calculator(3.6, time(7, 54, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test0406(self):
        out_time = time_diff_calculator(5.2, time(11, 58, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test0407(self):
        out_time = time_diff_calculator(3.8, time(7, 53, 0))
        self.assertEquals(out_time, time(11, 42, 0))
    
    def test0408(self):
        out_time = time_diff_calculator(4.9, time(12, 12, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test0409(self):
        out_time = time_diff_calculator(4.7, time(7, 7, 0))
        self.assertEquals(out_time, time(11, 48, 0))
    
    def test0410(self):
        out_time = time_diff_calculator(4.4, time(12, 18, 0))
        self.assertEquals(out_time, time(16, 42, 0))
    
    def test0411(self):
        out_time = time_diff_calculator(4.3, time(7, 8, 0))
        self.assertEquals(out_time, time(11, 24, 0))
    
    def test0412(self):
        out_time = time_diff_calculator(4.5, time(11, 59, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test0413(self):
        out_time = time_diff_calculator(4.3, time(7, 9, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test0414(self):
        out_time = time_diff_calculator(6, time(12, 3, 0))
        self.assertEquals(out_time, time(18, 6, 0))
    
    def test0415(self):
        out_time = time_diff_calculator(4.4, time(7, 7, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test0416(self):
        out_time = time_diff_calculator(5.1, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test0417(self):
        out_time = time_diff_calculator(3.9, time(7, 13, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0418(self):
        out_time = time_diff_calculator(5.5, time(11, 39, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test0419(self):
        out_time = time_diff_calculator(4.4, time(7, 11, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test0420(self):
        out_time = time_diff_calculator(4.6, time(12, 5, 0))
        self.assertEquals(out_time, time(16, 42, 0))
    
    def test0421(self):
        out_time = time_diff_calculator(3.7, time(7, 23, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0422(self):
        out_time = time_diff_calculator(5, time(11, 42, 0))
        self.assertEquals(out_time, time(16, 42, 0))
    
    def test0423(self):
        out_time = time_diff_calculator(4, time(7, 0, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0424(self):
        out_time = time_diff_calculator(6, time(11, 28, 0))
        self.assertEquals(out_time, time(17, 30, 0))
    
    def test0425(self):
        out_time = time_diff_calculator(1.2, time(7, 14, 0))
        self.assertEquals(out_time, time(8, 24, 0))
    
    def test0426(self):
        out_time = time_diff_calculator(5.7, time(11, 33, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test0427(self):
        out_time = time_diff_calculator(3.6, time(8, 15, 0))
        self.assertEquals(out_time, time(11, 54, 0))
    
    def test0428(self):
        out_time = time_diff_calculator(4.5, time(6, 58, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test0429(self):
        out_time = time_diff_calculator(5.5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 30, 0))
    
    def test0430(self):
        out_time = time_diff_calculator(7.5, time(6, 59, 0))
        self.assertEquals(out_time, time(14, 30, 0))
    
    def test0431(self):
        out_time = time_diff_calculator(2.3, time(14, 58, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test0432(self):
        out_time = time_diff_calculator(6, time(6, 57, 0))
        self.assertEquals(out_time, time(13, 0, 0))
    
    def test0433(self):
        out_time = time_diff_calculator(4, time(13, 33, 0))
        self.assertEquals(out_time, time(17, 36, 0))
    
    def test0434(self):
        out_time = time_diff_calculator(5.5, time(7, 1, 0))
        self.assertEquals(out_time, time(12, 30, 0))
    
    def test0435(self):
        out_time = time_diff_calculator(4.6, time(13, 0, 0))
        self.assertEquals(out_time, time(17, 36, 0))
    
    def test0436(self):
        out_time = time_diff_calculator(4, time(8, 1, 0))
        self.assertEquals(out_time, time(12, 0, 0))
    
    def test0437(self):
        out_time = time_diff_calculator(6.1, time(12, 27, 0))
        self.assertEquals(out_time, time(18, 36, 0))
    
    def test0438(self):
        out_time = time_diff_calculator(4.3, time(8, 4, 0))
        self.assertEquals(out_time, time(12, 24, 0))
    
    def test0439(self):
        out_time = time_diff_calculator(5.2, time(14, 1, 0))
        self.assertEquals(out_time, time(19, 12, 0))
    
    def test0440(self):
        out_time = time_diff_calculator(4.5, time(7, 19, 0))
        self.assertEquals(out_time, time(11, 48, 0))
    
    def test0441(self):
        out_time = time_diff_calculator(4.7, time(12, 57, 0))
        self.assertEquals(out_time, time(17, 42, 0))
    
    def test0442(self):
        out_time = time_diff_calculator(4.5, time(6, 57, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test0443(self):
        out_time = time_diff_calculator(5.6, time(11, 56, 0))
        self.assertEquals(out_time, time(17, 30, 0))
    
    def test0444(self):
        out_time = time_diff_calculator(4.7, time(6, 50, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test0445(self):
        out_time = time_diff_calculator(5.6, time(11, 58, 0))
        self.assertEquals(out_time, time(17, 36, 0))
    
    def test0446(self):
        out_time = time_diff_calculator(5.7, time(6, 57, 0))
        self.assertEquals(out_time, time(12, 42, 0))
    
    def test0447(self):
        out_time = time_diff_calculator(4.5, time(13, 16, 0))
        self.assertEquals(out_time, time(17, 48, 0))
    
    def test0448(self):
        out_time = time_diff_calculator(4.5, time(7, 1, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test0449(self):
        out_time = time_diff_calculator(6.5, time(11, 58, 0))
        self.assertEquals(out_time, time(18, 30, 0))
    
    def test0450(self):
        out_time = time_diff_calculator(4.7, time(7, 59, 0))
        self.assertEquals(out_time, time(12, 42, 0))
    
    def test0451(self):
        out_time = time_diff_calculator(4.9, time(12, 59, 0))
        self.assertEquals(out_time, time(17, 54, 0))
    
    def test0452(self):
        out_time = time_diff_calculator(4, time(7, 4, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0453(self):
        out_time = time_diff_calculator(6.6, time(11, 30, 0))
        self.assertEquals(out_time, time(18, 6, 0))
    
    def test0454(self):
        out_time = time_diff_calculator(4.5, time(7, 1, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test0455(self):
        out_time = time_diff_calculator(7.3, time(11, 59, 0))
        self.assertEquals(out_time, time(19, 18, 0))
    
    def test0456(self):
        out_time = time_diff_calculator(4.4, time(6, 54, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test0457(self):
        out_time = time_diff_calculator(7.3, time(11, 45, 0))
        self.assertEquals(out_time, time(19, 6, 0))
    
    def test0458(self):
        out_time = time_diff_calculator(4.1, time(7, 5, 0))
        self.assertEquals(out_time, time(11, 12, 0))
    
    def test0459(self):
        out_time = time_diff_calculator(6.8, time(11, 48, 0))
        self.assertEquals(out_time, time(18, 36, 0))
    
    def test0460(self):
        out_time = time_diff_calculator(4.9, time(7, 11, 0))
        self.assertEquals(out_time, time(12, 6, 0))
    
    def test0461(self):
        out_time = time_diff_calculator(6.1, time(12, 43, 0))
        self.assertEquals(out_time, time(18, 48, 0))
    
    def test0462(self):
        out_time = time_diff_calculator(4, time(7, 8, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0463(self):
        out_time = time_diff_calculator(6.9, time(11, 35, 0))
        self.assertEquals(out_time, time(18, 30, 0))
    
    def test0464(self):
        out_time = time_diff_calculator(4, time(6, 58, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0465(self):
        out_time = time_diff_calculator(7.6, time(11, 29, 0))
        self.assertEquals(out_time, time(19, 6, 0))
    
    def test0466(self):
        out_time = time_diff_calculator(4, time(7, 12, 0))
        self.assertEquals(out_time, time(11, 12, 0))
    
    def test0467(self):
        out_time = time_diff_calculator(6.7, time(11, 42, 0))
        self.assertEquals(out_time, time(18, 24, 0))
    
    def test0468(self):
        out_time = time_diff_calculator(4.2, time(8, 20, 0))
        self.assertEquals(out_time, time(12, 30, 0))
    
    def test0469(self):
        out_time = time_diff_calculator(6.3, time(12, 58, 0))
        self.assertEquals(out_time, time(19, 18, 0))
    
    def test0470(self):
        out_time = time_diff_calculator(3.9, time(6, 58, 0))
        self.assertEquals(out_time, time(10, 54, 0))
    
    def test0471(self):
        out_time = time_diff_calculator(7.3, time(11, 29, 0))
        self.assertEquals(out_time, time(18, 48, 0))
    
    def test0472(self):
        out_time = time_diff_calculator(4, time(7, 7, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0473(self):
        out_time = time_diff_calculator(7.3, time(11, 30, 0))
        self.assertEquals(out_time, time(18, 48, 0))
    
    def test0474(self):
        out_time = time_diff_calculator(4, time(7, 10, 0))
        self.assertEquals(out_time, time(11, 12, 0))
    
    def test0475(self):
        out_time = time_diff_calculator(6, time(11, 42, 0))
        self.assertEquals(out_time, time(17, 42, 0))
    
    def test0476(self):
        out_time = time_diff_calculator(4.5, time(7, 1, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test0477(self):
        out_time = time_diff_calculator(7.1, time(12, 2, 0))
        self.assertEquals(out_time, time(19, 6, 0))
    
    def test0478(self):
        out_time = time_diff_calculator(6.3, time(6, 51, 0))
        self.assertEquals(out_time, time(13, 12, 0))
    
    def test0479(self):
        out_time = time_diff_calculator(4.9, time(14, 28, 0))
        self.assertEquals(out_time, time(19, 24, 0))
    
    def test0480(self):
        out_time = time_diff_calculator(6.7, time(5, 57, 0))
        self.assertEquals(out_time, time(12, 42, 0))
    
    def test0481(self):
        out_time = time_diff_calculator(5.2, time(14, 12, 0))
        self.assertEquals(out_time, time(19, 24, 0))
    
    def test0482(self):
        out_time = time_diff_calculator(3.7, time(7, 19, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0483(self):
        out_time = time_diff_calculator(5.5, time(11, 35, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test0484(self):
        out_time = time_diff_calculator(3.7, time(7, 18, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0485(self):
        out_time = time_diff_calculator(7.2, time(11, 29, 0))
        self.assertEquals(out_time, time(18, 42, 0))
    
    def test0486(self):
        out_time = time_diff_calculator(4, time(7, 3, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0487(self):
        out_time = time_diff_calculator(6.6, time(11, 37, 0))
        self.assertEquals(out_time, time(18, 12, 0))
    
    def test0488(self):
        out_time = time_diff_calculator(4.2, time(7, 48, 0))
        self.assertEquals(out_time, time(12, 0, 0))
    
    def test0489(self):
        out_time = time_diff_calculator(5.9, time(12, 27, 0))
        self.assertEquals(out_time, time(18, 24, 0))
    
    def test0490(self):
        out_time = time_diff_calculator(3.7, time(7, 23, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0491(self):
        out_time = time_diff_calculator(6.8, time(11, 36, 0))
        self.assertEquals(out_time, time(18, 24, 0))
    
    def test0492(self):
        out_time = time_diff_calculator(3.8, time(7, 12, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0493(self):
        out_time = time_diff_calculator(7.1, time(11, 31, 0))
        self.assertEquals(out_time, time(18, 36, 0))
    
    def test0494(self):
        out_time = time_diff_calculator(5.5, time(7, 6, 0))
        self.assertEquals(out_time, time(12, 36, 0))
    
    def test0495(self):
        out_time = time_diff_calculator(5.4, time(13, 0, 0))
        self.assertEquals(out_time, time(18, 24, 0))
    
    def test0496(self):
        out_time = time_diff_calculator(2.3, time(11, 31, 0))
        self.assertEquals(out_time, time(13, 48, 0))
    
    def test0497(self):
        out_time = time_diff_calculator(2.7, time(15, 0, 0))
        self.assertEquals(out_time, time(17, 42, 0))
    
    def test0498(self):
        out_time = time_diff_calculator(4.2, time(7, 4, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test0499(self):
        out_time = time_diff_calculator(8.1, time(11, 43, 0))
        self.assertEquals(out_time, time(19, 48, 0))
    
    def test0500(self):
        out_time = time_diff_calculator(4, time(7, 0, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0501(self):
        out_time = time_diff_calculator(7.3, time(11, 33, 0))
        self.assertEquals(out_time, time(18, 54, 0))
    
    def test0502(self):
        out_time = time_diff_calculator(4.1, time(7, 7, 0))
        self.assertEquals(out_time, time(11, 12, 0))
    
    def test0503(self):
        out_time = time_diff_calculator(7.1, time(11, 40, 0))
        self.assertEquals(out_time, time(18, 48, 0))
    
    def test0504(self):
        out_time = time_diff_calculator(6.9, time(11, 0, 0))
        self.assertEquals(out_time, time(17, 54, 0))
    
    def test0505(self):
        out_time = time_diff_calculator(4.1, time(7, 5, 0))
        self.assertEquals(out_time, time(11, 12, 0))
    
    def test0506(self):
        out_time = time_diff_calculator(6.8, time(11, 41, 0))
        self.assertEquals(out_time, time(18, 30, 0))
    
    def test0507(self):
        out_time = time_diff_calculator(3.9, time(7, 4, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0508(self):
        out_time = time_diff_calculator(7.3, time(11, 36, 0))
        self.assertEquals(out_time, time(18, 54, 0))
    
    def test0509(self):
        out_time = time_diff_calculator(3.9, time(7, 3, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0510(self):
        out_time = time_diff_calculator(6.9, time(11, 33, 0))
        self.assertEquals(out_time, time(18, 30, 0))
    
    def test0511(self):
        out_time = time_diff_calculator(4.7, time(7, 7, 0))
        self.assertEquals(out_time, time(11, 48, 0))
    
    def test0512(self):
        out_time = time_diff_calculator(6.3, time(12, 18, 0))
        self.assertEquals(out_time, time(18, 36, 0))
    
    def test0513(self):
        out_time = time_diff_calculator(3.6, time(7, 25, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0514(self):
        out_time = time_diff_calculator(7, time(11, 33, 0))
        self.assertEquals(out_time, time(18, 36, 0))
    
    def test0515(self):
        out_time = time_diff_calculator(8.6, time(9, 0, 0))
        self.assertEquals(out_time, time(17, 36, 0))
    
    def test0516(self):
        out_time = time_diff_calculator(5.5, time(7, 27, 0))
        self.assertEquals(out_time, time(13, 0, 0))
    
    def test0517(self):
        out_time = time_diff_calculator(3.2, time(13, 33, 0))
        self.assertEquals(out_time, time(16, 48, 0))
    
    def test0518(self):
        out_time = time_diff_calculator(3.6, time(7, 25, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0519(self):
        out_time = time_diff_calculator(7.2, time(11, 31, 0))
        self.assertEquals(out_time, time(18, 42, 0))
    
    def test0520(self):
        out_time = time_diff_calculator(3.8, time(7, 22, 0))
        self.assertEquals(out_time, time(11, 12, 0))
    
    def test0521(self):
        out_time = time_diff_calculator(5.7, time(11, 25, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test0522(self):
        out_time = time_diff_calculator(4.1, time(7, 28, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test0523(self):
        out_time = time_diff_calculator(6.2, time(12, 7, 0))
        self.assertEquals(out_time, time(18, 18, 0))
    
    def test0524(self):
        out_time = time_diff_calculator(4, time(7, 20, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test0525(self):
        out_time = time_diff_calculator(6.1, time(11, 46, 0))
        self.assertEquals(out_time, time(17, 54, 0))
    
    def test0526(self):
        out_time = time_diff_calculator(3.7, time(7, 24, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0527(self):
        out_time = time_diff_calculator(6.7, time(11, 37, 0))
        self.assertEquals(out_time, time(18, 18, 0))
    
    def test0528(self):
        out_time = time_diff_calculator(3.5, time(7, 27, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0529(self):
        out_time = time_diff_calculator(7.1, time(11, 30, 0))
        self.assertEquals(out_time, time(18, 36, 0))
    
    def test0530(self):
        out_time = time_diff_calculator(3.5, time(7, 28, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0531(self):
        out_time = time_diff_calculator(5.2, time(11, 33, 0))
        self.assertEquals(out_time, time(16, 48, 0))
    
    def test0532(self):
        out_time = time_diff_calculator(3.9, time(7, 8, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0533(self):
        out_time = time_diff_calculator(6.1, time(11, 32, 0))
        self.assertEquals(out_time, time(17, 36, 0))
    
    def test0534(self):
        out_time = time_diff_calculator(3.7, time(7, 16, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0535(self):
        out_time = time_diff_calculator(6.2, time(11, 30, 0))
        self.assertEquals(out_time, time(17, 42, 0))
    
    def test0536(self):
        out_time = time_diff_calculator(6.8, time(12, 21, 0))
        self.assertEquals(out_time, time(19, 12, 0))
    
    def test0537(self):
        out_time = time_diff_calculator(3.5, time(7, 28, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0538(self):
        out_time = time_diff_calculator(7.9, time(11, 31, 0))
        self.assertEquals(out_time, time(19, 24, 0))
    
    def test0539(self):
        out_time = time_diff_calculator(4.1, time(6, 55, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0540(self):
        out_time = time_diff_calculator(6.4, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 54, 0))
    
    def test0541(self):
        out_time = time_diff_calculator(3.9, time(7, 5, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0542(self):
        out_time = time_diff_calculator(6.9, time(11, 34, 0))
        self.assertEquals(out_time, time(18, 30, 0))
    
    def test0543(self):
        out_time = time_diff_calculator(6.7, time(7, 25, 0))
        self.assertEquals(out_time, time(14, 6, 0))
    
    def test0544(self):
        out_time = time_diff_calculator(3.9, time(14, 29, 0))
        self.assertEquals(out_time, time(18, 24, 0))
    
    def test0545(self):
        out_time = time_diff_calculator(3, time(7, 31, 0))
        self.assertEquals(out_time, time(10, 30, 0))
    
    def test0546(self):
        out_time = time_diff_calculator(6.8, time(10, 58, 0))
        self.assertEquals(out_time, time(17, 48, 0))
    
    def test0547(self):
        out_time = time_diff_calculator(3.6, time(7, 24, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0548(self):
        out_time = time_diff_calculator(5.8, time(11, 29, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test0549(self):
        out_time = time_diff_calculator(3.7, time(7, 21, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0550(self):
        out_time = time_diff_calculator(6.7, time(11, 37, 0))
        self.assertEquals(out_time, time(18, 18, 0))
    
    def test0551(self):
        out_time = time_diff_calculator(3.5, time(7, 27, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0552(self):
        out_time = time_diff_calculator(6.2, time(11, 34, 0))
        self.assertEquals(out_time, time(17, 48, 0))
    
    def test0553(self):
        out_time = time_diff_calculator(3.6, time(7, 29, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0554(self):
        out_time = time_diff_calculator(5.8, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test0555(self):
        out_time = time_diff_calculator(3.7, time(7, 26, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0556(self):
        out_time = time_diff_calculator(6.7, time(11, 36, 0))
        self.assertEquals(out_time, time(18, 18, 0))
    
    def test0557(self):
        out_time = time_diff_calculator(5.2, time(10, 22, 0))
        self.assertEquals(out_time, time(15, 36, 0))
    
    def test0558(self):
        out_time = time_diff_calculator(4, time(7, 1, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0559(self):
        out_time = time_diff_calculator(6.7, time(11, 34, 0))
        self.assertEquals(out_time, time(18, 18, 0))
    
    def test0560(self):
        out_time = time_diff_calculator(6.4, time(7, 9, 0))
        self.assertEquals(out_time, time(13, 36, 0))
    
    def test0561(self):
        out_time = time_diff_calculator(3.7, time(14, 6, 0))
        self.assertEquals(out_time, time(17, 48, 0))
    
    def test0562(self):
        out_time = time_diff_calculator(3.8, time(7, 20, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0563(self):
        out_time = time_diff_calculator(6.3, time(11, 28, 0))
        self.assertEquals(out_time, time(17, 48, 0))
    
    def test0564(self):
        out_time = time_diff_calculator(7.7, time(7, 4, 0))
        self.assertEquals(out_time, time(14, 48, 0))
    
    def test0565(self):
        out_time = time_diff_calculator(1.6, time(15, 25, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test0566(self):
        out_time = time_diff_calculator(6.1, time(7, 44, 0))
        self.assertEquals(out_time, time(13, 48, 0))
    
    def test0567(self):
        out_time = time_diff_calculator(4.2, time(14, 18, 0))
        self.assertEquals(out_time, time(18, 30, 0))
    
    def test0568(self):
        out_time = time_diff_calculator(8.3, time(9, 2, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test0569(self):
        out_time = time_diff_calculator(0.4, time(17, 34, 0))
        self.assertEquals(out_time, time(18, 0, 0))
    
    def test0570(self):
        out_time = time_diff_calculator(3.5, time(7, 48, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test0571(self):
        out_time = time_diff_calculator(5.9, time(11, 49, 0))
        self.assertEquals(out_time, time(17, 42, 0))
    
    def test0572(self):
        out_time = time_diff_calculator(3.3, time(7, 46, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0573(self):
        out_time = time_diff_calculator(5.5, time(11, 36, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test0574(self):
        out_time = time_diff_calculator(3.3, time(7, 46, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0575(self):
        out_time = time_diff_calculator(5, time(11, 32, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test0576(self):
        out_time = time_diff_calculator(3.5, time(7, 27, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0577(self):
        out_time = time_diff_calculator(4.9, time(11, 40, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test0578(self):
        out_time = time_diff_calculator(3.5, time(7, 50, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test0579(self):
        out_time = time_diff_calculator(6, time(11, 48, 0))
        self.assertEquals(out_time, time(17, 48, 0))
    
    def test0580(self):
        out_time = time_diff_calculator(3.9, time(7, 38, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test0581(self):
        out_time = time_diff_calculator(5.8, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 48, 0))
    
    def test0582(self):
        out_time = time_diff_calculator(3.4, time(7, 56, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test0583(self):
        out_time = time_diff_calculator(6.2, time(11, 47, 0))
        self.assertEquals(out_time, time(18, 0, 0))
    
    def test0584(self):
        out_time = time_diff_calculator(3.8, time(7, 40, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test0585(self):
        out_time = time_diff_calculator(5.8, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 48, 0))
    
    def test0586(self):
        out_time = time_diff_calculator(3.9, time(7, 35, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test0587(self):
        out_time = time_diff_calculator(6.5, time(12, 0, 0))
        self.assertEquals(out_time, time(18, 30, 0))
    
    def test0588(self):
        out_time = time_diff_calculator(4.2, time(7, 26, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test0589(self):
        out_time = time_diff_calculator(5, time(12, 7, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test0590(self):
        out_time = time_diff_calculator(5.2, time(7, 45, 0))
        self.assertEquals(out_time, time(13, 0, 0))
    
    def test0591(self):
        out_time = time_diff_calculator(3.6, time(13, 32, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test0592(self):
        out_time = time_diff_calculator(3.2, time(7, 46, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0593(self):
        out_time = time_diff_calculator(6.9, time(11, 35, 0))
        self.assertEquals(out_time, time(18, 30, 0))
    
    def test0594(self):
        out_time = time_diff_calculator(3.3, time(7, 41, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0595(self):
        out_time = time_diff_calculator(6.6, time(11, 33, 0))
        self.assertEquals(out_time, time(18, 12, 0))
    
    def test0596(self):
        out_time = time_diff_calculator(3.4, time(7, 33, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0597(self):
        out_time = time_diff_calculator(7.9, time(11, 31, 0))
        self.assertEquals(out_time, time(19, 24, 0))
    
    def test0598(self):
        out_time = time_diff_calculator(1.1, time(8, 44, 0))
        self.assertEquals(out_time, time(9, 48, 0))
    
    def test0599(self):
        out_time = time_diff_calculator(3.3, time(7, 44, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0600(self):
        out_time = time_diff_calculator(5.8, time(11, 32, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test0601(self):
        out_time = time_diff_calculator(3.1, time(7, 52, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0602(self):
        out_time = time_diff_calculator(5.8, time(11, 32, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test0603(self):
        out_time = time_diff_calculator(3.5, time(7, 29, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0604(self):
        out_time = time_diff_calculator(5.4, time(11, 32, 0))
        self.assertEquals(out_time, time(16, 54, 0))
    
    def test0605(self):
        out_time = time_diff_calculator(3, time(7, 57, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0606(self):
        out_time = time_diff_calculator(5.6, time(11, 30, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test0607(self):
        out_time = time_diff_calculator(3.6, time(7, 22, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0608(self):
        out_time = time_diff_calculator(5.2, time(11, 32, 0))
        self.assertEquals(out_time, time(16, 42, 0))
    
    def test0609(self):
        out_time = time_diff_calculator(3.2, time(7, 46, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0610(self):
        out_time = time_diff_calculator(5.6, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test0611(self):
        out_time = time_diff_calculator(3, time(7, 58, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0612(self):
        out_time = time_diff_calculator(5.5, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test0613(self):
        out_time = time_diff_calculator(3.4, time(7, 33, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0614(self):
        out_time = time_diff_calculator(4.3, time(11, 33, 0))
        self.assertEquals(out_time, time(15, 54, 0))
    
    def test0615(self):
        out_time = time_diff_calculator(3.3, time(7, 41, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0616(self):
        out_time = time_diff_calculator(5.2, time(11, 37, 0))
        self.assertEquals(out_time, time(16, 48, 0))
    
    def test0617(self):
        out_time = time_diff_calculator(3.4, time(7, 38, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0618(self):
        out_time = time_diff_calculator(6.4, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 54, 0))
    
    def test0619(self):
        out_time = time_diff_calculator(3.2, time(8, 4, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test0620(self):
        out_time = time_diff_calculator(3.3, time(7, 42, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0621(self):
        out_time = time_diff_calculator(5.6, time(11, 30, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test0622(self):
        out_time = time_diff_calculator(3.4, time(7, 40, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0623(self):
        out_time = time_diff_calculator(4.9, time(11, 33, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test0624(self):
        out_time = time_diff_calculator(3.2, time(7, 45, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0625(self):
        out_time = time_diff_calculator(5.3, time(11, 32, 0))
        self.assertEquals(out_time, time(16, 48, 0))
    
    def test0626(self):
        out_time = time_diff_calculator(4.5, time(7, 28, 0))
        self.assertEquals(out_time, time(12, 0, 0))
    
    def test0627(self):
        out_time = time_diff_calculator(3.5, time(13, 0, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test0628(self):
        out_time = time_diff_calculator(3.9, time(7, 51, 0))
        self.assertEquals(out_time, time(11, 48, 0))
    
    def test0629(self):
        out_time = time_diff_calculator(4.6, time(12, 6, 0))
        self.assertEquals(out_time, time(16, 42, 0))
    
    def test0630(self):
        out_time = time_diff_calculator(3.8, time(7, 18, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0631(self):
        out_time = time_diff_calculator(5.5, time(11, 36, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test0632(self):
        out_time = time_diff_calculator(3, time(8, 2, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0633(self):
        out_time = time_diff_calculator(5, time(11, 32, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test0634(self):
        out_time = time_diff_calculator(3.4, time(7, 40, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0635(self):
        out_time = time_diff_calculator(5.5, time(11, 35, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test0636(self):
        out_time = time_diff_calculator(3.2, time(7, 40, 0))
        self.assertEquals(out_time, time(10, 54, 0))
    
    def test0637(self):
        out_time = time_diff_calculator(5.6, time(11, 32, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test0638(self):
        out_time = time_diff_calculator(3.5, time(7, 38, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0639(self):
        out_time = time_diff_calculator(3.7, time(7, 36, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test0640(self):
        out_time = time_diff_calculator(5.5, time(11, 49, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test0641(self):
        out_time = time_diff_calculator(3.3, time(7, 44, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0642(self):
        out_time = time_diff_calculator(5, time(11, 30, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test0643(self):
        out_time = time_diff_calculator(3.3, time(7, 40, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0644(self):
        out_time = time_diff_calculator(5.4, time(11, 32, 0))
        self.assertEquals(out_time, time(16, 54, 0))
    
    def test0645(self):
        out_time = time_diff_calculator(3.3, time(7, 41, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0646(self):
        out_time = time_diff_calculator(5.6, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test0647(self):
        out_time = time_diff_calculator(3.2, time(7, 47, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0648(self):
        out_time = time_diff_calculator(3.2, time(7, 47, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0649(self):
        out_time = time_diff_calculator(5.5, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test0650(self):
        out_time = time_diff_calculator(3.2, time(7, 47, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0651(self):
        out_time = time_diff_calculator(5.5, time(11, 30, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test0652(self):
        out_time = time_diff_calculator(3.2, time(7, 46, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0653(self):
        out_time = time_diff_calculator(5.4, time(11, 32, 0))
        self.assertEquals(out_time, time(16, 54, 0))
    
    def test0654(self):
        out_time = time_diff_calculator(3.2, time(7, 49, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0655(self):
        out_time = time_diff_calculator(4.9, time(11, 36, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test0656(self):
        out_time = time_diff_calculator(3.7, time(7, 15, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0657(self):
        out_time = time_diff_calculator(3.2, time(7, 49, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0658(self):
        out_time = time_diff_calculator(5.7, time(11, 40, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test0659(self):
        out_time = time_diff_calculator(3.2, time(7, 45, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0660(self):
        out_time = time_diff_calculator(7.2, time(11, 31, 0))
        self.assertEquals(out_time, time(18, 42, 0))
    
    def test0661(self):
        out_time = time_diff_calculator(3.2, time(7, 49, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0662(self):
        out_time = time_diff_calculator(5.2, time(11, 32, 0))
        self.assertEquals(out_time, time(16, 42, 0))
    
    def test0663(self):
        out_time = time_diff_calculator(3.5, time(7, 30, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0664(self):
        out_time = time_diff_calculator(5, time(11, 32, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test0665(self):
        out_time = time_diff_calculator(3.3, time(7, 47, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0666(self):
        out_time = time_diff_calculator(3.2, time(7, 48, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0667(self):
        out_time = time_diff_calculator(5.3, time(11, 34, 0))
        self.assertEquals(out_time, time(16, 54, 0))
    
    def test0668(self):
        out_time = time_diff_calculator(3.7, time(7, 36, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test0669(self):
        out_time = time_diff_calculator(5.1, time(11, 48, 0))
        self.assertEquals(out_time, time(16, 54, 0))
    
    def test0670(self):
        out_time = time_diff_calculator(3.2, time(7, 46, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0671(self):
        out_time = time_diff_calculator(5, time(11, 31, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test0672(self):
        out_time = time_diff_calculator(3.3, time(7, 39, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0673(self):
        out_time = time_diff_calculator(5.1, time(11, 32, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test0674(self):
        out_time = time_diff_calculator(3.4, time(7, 36, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0675(self):
        out_time = time_diff_calculator(5.6, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test0676(self):
        out_time = time_diff_calculator(3.4, time(7, 38, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0677(self):
        out_time = time_diff_calculator(5.5, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test0678(self):
        out_time = time_diff_calculator(3.3, time(7, 40, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0679(self):
        out_time = time_diff_calculator(5.5, time(11, 32, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test0680(self):
        out_time = time_diff_calculator(3.2, time(7, 47, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0681(self):
        out_time = time_diff_calculator(6.2, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 42, 0))
    
    def test0682(self):
        out_time = time_diff_calculator(3.4, time(7, 33, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0683(self):
        out_time = time_diff_calculator(5.7, time(11, 32, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test0684(self):
        out_time = time_diff_calculator(3.3, time(7, 39, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0685(self):
        out_time = time_diff_calculator(5.5, time(11, 32, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test0686(self):
        out_time = time_diff_calculator(3.4, time(7, 34, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0687(self):
        out_time = time_diff_calculator(5.5, time(11, 32, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test0688(self):
        out_time = time_diff_calculator(3.6, time(7, 31, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0689(self):
        out_time = time_diff_calculator(5.4, time(11, 37, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test0690(self):
        out_time = time_diff_calculator(3.4, time(7, 34, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0691(self):
        out_time = time_diff_calculator(5.5, time(11, 32, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test0692(self):
        out_time = time_diff_calculator(4.1, time(6, 51, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0693(self):
        out_time = time_diff_calculator(4.7, time(11, 29, 0))
        self.assertEquals(out_time, time(16, 12, 0))
    
    def test0694(self):
        out_time = time_diff_calculator(3.7, time(7, 18, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0695(self):
        out_time = time_diff_calculator(5.9, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test0696(self):
        out_time = time_diff_calculator(2.4, time(16, 44, 0))
        self.assertEquals(out_time, time(19, 6, 0))
    
    def test0697(self):
        out_time = time_diff_calculator(3.8, time(7, 12, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0698(self):
        out_time = time_diff_calculator(6.3, time(11, 32, 0))
        self.assertEquals(out_time, time(17, 48, 0))
    
    def test0699(self):
        out_time = time_diff_calculator(3.9, time(7, 6, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0700(self):
        out_time = time_diff_calculator(5.7, time(11, 30, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test0701(self):
        out_time = time_diff_calculator(3.5, time(7, 30, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0702(self):
        out_time = time_diff_calculator(6.1, time(11, 33, 0))
        self.assertEquals(out_time, time(17, 42, 0))
    
    def test0703(self):
        out_time = time_diff_calculator(3.8, time(7, 13, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0704(self):
        out_time = time_diff_calculator(5.7, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test0705(self):
        out_time = time_diff_calculator(4, time(14, 8, 0))
        self.assertEquals(out_time, time(18, 6, 0))
    
    def test0706(self):
        out_time = time_diff_calculator(3.8, time(7, 18, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0707(self):
        out_time = time_diff_calculator(5.6, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test0708(self):
        out_time = time_diff_calculator(5.2, time(7, 17, 0))
        self.assertEquals(out_time, time(12, 30, 0))
    
    def test0709(self):
        out_time = time_diff_calculator(4, time(13, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test0710(self):
        out_time = time_diff_calculator(3, time(12, 57, 0))
        self.assertEquals(out_time, time(16, 0, 0))
    
    def test0711(self):
        out_time = time_diff_calculator(3.7, time(7, 17, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0712(self):
        out_time = time_diff_calculator(6.1, time(11, 32, 0))
        self.assertEquals(out_time, time(17, 36, 0))
    
    def test0713(self):
        out_time = time_diff_calculator(3.8, time(7, 12, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0714(self):
        out_time = time_diff_calculator(5.9, time(11, 30, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test0715(self):
        out_time = time_diff_calculator(3.8, time(7, 13, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0716(self):
        out_time = time_diff_calculator(5.2, time(11, 31, 0))
        self.assertEquals(out_time, time(16, 42, 0))
    
    def test0717(self):
        out_time = time_diff_calculator(4, time(7, 15, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test0718(self):
        out_time = time_diff_calculator(5.2, time(11, 49, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test0719(self):
        out_time = time_diff_calculator(3.8, time(7, 14, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0720(self):
        out_time = time_diff_calculator(5.9, time(11, 32, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test0721(self):
        out_time = time_diff_calculator(4, time(7, 20, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test0722(self):
        out_time = time_diff_calculator(5.3, time(12, 15, 0))
        self.assertEquals(out_time, time(17, 36, 0))
    
    def test0723(self):
        out_time = time_diff_calculator(4.1, time(7, 6, 0))
        self.assertEquals(out_time, time(11, 12, 0))
    
    def test0724(self):
        out_time = time_diff_calculator(5.7, time(11, 43, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test0725(self):
        out_time = time_diff_calculator(3.7, time(7, 18, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0726(self):
        out_time = time_diff_calculator(5.8, time(11, 30, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test0727(self):
        out_time = time_diff_calculator(3.7, time(7, 20, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0728(self):
        out_time = time_diff_calculator(6.1, time(11, 32, 0))
        self.assertEquals(out_time, time(17, 36, 0))
    
    def test0729(self):
        out_time = time_diff_calculator(2.7, time(12, 10, 0))
        self.assertEquals(out_time, time(14, 54, 0))
    
    def test0730(self):
        out_time = time_diff_calculator(3.6, time(7, 21, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0731(self):
        out_time = time_diff_calculator(5.7, time(11, 32, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test0732(self):
        out_time = time_diff_calculator(3.8, time(7, 14, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0733(self):
        out_time = time_diff_calculator(6.9, time(11, 32, 0))
        self.assertEquals(out_time, time(18, 24, 0))
    
    def test0734(self):
        out_time = time_diff_calculator(3.5, time(7, 36, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0735(self):
        out_time = time_diff_calculator(5.7, time(11, 37, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test0736(self):
        out_time = time_diff_calculator(3.6, time(7, 24, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0737(self):
        out_time = time_diff_calculator(3.7, time(11, 32, 0))
        self.assertEquals(out_time, time(15, 12, 0))
    
    def test0738(self):
        out_time = time_diff_calculator(3.8, time(7, 13, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0739(self):
        out_time = time_diff_calculator(5.2, time(11, 32, 0))
        self.assertEquals(out_time, time(16, 42, 0))
    
    def test0740(self):
        out_time = time_diff_calculator(10.1, time(7, 10, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test0741(self):
        out_time = time_diff_calculator(3.8, time(7, 17, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0742(self):
        out_time = time_diff_calculator(5.5, time(11, 34, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test0743(self):
        out_time = time_diff_calculator(3.5, time(7, 27, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0744(self):
        out_time = time_diff_calculator(5.9, time(11, 33, 0))
        self.assertEquals(out_time, time(17, 30, 0))
    
    def test0745(self):
        out_time = time_diff_calculator(3.7, time(7, 27, 0))
        self.assertEquals(out_time, time(11, 12, 0))
    
    def test0746(self):
        out_time = time_diff_calculator(6, time(11, 44, 0))
        self.assertEquals(out_time, time(17, 42, 0))
    
    def test0747(self):
        out_time = time_diff_calculator(1.8, time(10, 15, 0))
        self.assertEquals(out_time, time(12, 6, 0))
    
    def test0748(self):
        out_time = time_diff_calculator(5.1, time(12, 13, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test0749(self):
        out_time = time_diff_calculator(3.7, time(7, 18, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0750(self):
        out_time = time_diff_calculator(6.1, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 36, 0))
    
    def test0751(self):
        out_time = time_diff_calculator(2.4, time(18, 1, 0))
        self.assertEquals(out_time, time(20, 24, 0))
    
    def test0752(self):
        out_time = time_diff_calculator(3.7, time(7, 16, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0753(self):
        out_time = time_diff_calculator(6.4, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 54, 0))
    
    def test0754(self):
        out_time = time_diff_calculator(3.7, time(7, 20, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0755(self):
        out_time = time_diff_calculator(6.6, time(11, 30, 0))
        self.assertEquals(out_time, time(18, 6, 0))
    
    def test0756(self):
        out_time = time_diff_calculator(3.7, time(7, 20, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0757(self):
        out_time = time_diff_calculator(8.1, time(11, 32, 0))
        self.assertEquals(out_time, time(19, 36, 0))
    
    def test0758(self):
        out_time = time_diff_calculator(4.4, time(7, 16, 0))
        self.assertEquals(out_time, time(11, 42, 0))
    
    def test0759(self):
        out_time = time_diff_calculator(6.8, time(12, 14, 0))
        self.assertEquals(out_time, time(19, 0, 0))
    
    def test0760(self):
        out_time = time_diff_calculator(9.6, time(8, 17, 0))
        self.assertEquals(out_time, time(17, 54, 0))
    
    def test0761(self):
        out_time = time_diff_calculator(3.2, time(7, 45, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0762(self):
        out_time = time_diff_calculator(5.9, time(11, 32, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test0763(self):
        out_time = time_diff_calculator(3.2, time(7, 48, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0764(self):
        out_time = time_diff_calculator(5.3, time(11, 32, 0))
        self.assertEquals(out_time, time(16, 48, 0))
    
    def test0765(self):
        out_time = time_diff_calculator(3.4, time(7, 36, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0766(self):
        out_time = time_diff_calculator(6.4, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 54, 0))
    
    def test0767(self):
        out_time = time_diff_calculator(3.2, time(7, 54, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0768(self):
        out_time = time_diff_calculator(5.8, time(11, 32, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test0769(self):
        out_time = time_diff_calculator(3.5, time(7, 46, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test0770(self):
        out_time = time_diff_calculator(5.7, time(11, 51, 0))
        self.assertEquals(out_time, time(17, 36, 0))
    
    def test0771(self):
        out_time = time_diff_calculator(4, time(8, 9, 0))
        self.assertEquals(out_time, time(12, 12, 0))
    
    def test0772(self):
        out_time = time_diff_calculator(4.8, time(7, 14, 0))
        self.assertEquals(out_time, time(12, 0, 0))
    
    def test0773(self):
        out_time = time_diff_calculator(5, time(12, 32, 0))
        self.assertEquals(out_time, time(17, 30, 0))
    
    def test0774(self):
        out_time = time_diff_calculator(3.8, time(7, 19, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0775(self):
        out_time = time_diff_calculator(6.3, time(11, 30, 0))
        self.assertEquals(out_time, time(17, 48, 0))
    
    def test0776(self):
        out_time = time_diff_calculator(4.4, time(7, 26, 0))
        self.assertEquals(out_time, time(11, 48, 0))
    
    def test0777(self):
        out_time = time_diff_calculator(4.7, time(12, 21, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test0778(self):
        out_time = time_diff_calculator(3.7, time(7, 32, 0))
        self.assertEquals(out_time, time(11, 12, 0))
    
    def test0779(self):
        out_time = time_diff_calculator(6, time(11, 43, 0))
        self.assertEquals(out_time, time(17, 42, 0))
    
    def test0780(self):
        out_time = time_diff_calculator(3.6, time(7, 24, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0781(self):
        out_time = time_diff_calculator(6.5, time(11, 32, 0))
        self.assertEquals(out_time, time(18, 0, 0))
    
    def test0782(self):
        out_time = time_diff_calculator(3.2, time(9, 5, 0))
        self.assertEquals(out_time, time(12, 18, 0))
    
    def test0783(self):
        out_time = time_diff_calculator(3.4, time(7, 33, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0784(self):
        out_time = time_diff_calculator(6.4, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 54, 0))
    
    def test0785(self):
        out_time = time_diff_calculator(4.1, time(6, 56, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0786(self):
        out_time = time_diff_calculator(5.8, time(11, 30, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test0787(self):
        out_time = time_diff_calculator(4.1, time(6, 53, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0788(self):
        out_time = time_diff_calculator(5.8, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test0789(self):
        out_time = time_diff_calculator(3.8, time(7, 9, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0790(self):
        out_time = time_diff_calculator(6.1, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 36, 0))
    
    def test0791(self):
        out_time = time_diff_calculator(3.7, time(7, 20, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0792(self):
        out_time = time_diff_calculator(5.6, time(11, 28, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test0793(self):
        out_time = time_diff_calculator(3.9, time(7, 12, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0794(self):
        out_time = time_diff_calculator(6, time(11, 37, 0))
        self.assertEquals(out_time, time(17, 36, 0))
    
    def test0795(self):
        out_time = time_diff_calculator(3.5, time(7, 27, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0796(self):
        out_time = time_diff_calculator(6.1, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 36, 0))
    
    def test0797(self):
        out_time = time_diff_calculator(3.7, time(7, 20, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0798(self):
        out_time = time_diff_calculator(6, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 30, 0))
    
    def test0799(self):
        out_time = time_diff_calculator(6.5, time(8, 18, 0))
        self.assertEquals(out_time, time(14, 48, 0))
    
    def test0800(self):
        out_time = time_diff_calculator(8.9, time(7, 25, 0))
        self.assertEquals(out_time, time(16, 18, 0))
    
    def test0801(self):
        out_time = time_diff_calculator(2.1, time(16, 40, 0))
        self.assertEquals(out_time, time(18, 48, 0))
    
    def test0802(self):
        out_time = time_diff_calculator(3.8, time(7, 16, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0803(self):
        out_time = time_diff_calculator(6, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 30, 0))
    
    def test0804(self):
        out_time = time_diff_calculator(3.8, time(7, 20, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0805(self):
        out_time = time_diff_calculator(6.4, time(11, 37, 0))
        self.assertEquals(out_time, time(18, 0, 0))
    
    def test0806(self):
        out_time = time_diff_calculator(4.4, time(6, 42, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0807(self):
        out_time = time_diff_calculator(6, time(11, 39, 0))
        self.assertEquals(out_time, time(17, 42, 0))
    
    def test0808(self):
        out_time = time_diff_calculator(3.7, time(7, 16, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0809(self):
        out_time = time_diff_calculator(5.3, time(11, 32, 0))
        self.assertEquals(out_time, time(16, 48, 0))
    
    def test0810(self):
        out_time = time_diff_calculator(4.1, time(6, 53, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0811(self):
        out_time = time_diff_calculator(5.8, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test0812(self):
        out_time = time_diff_calculator(3.7, time(7, 36, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test0813(self):
        out_time = time_diff_calculator(5.4, time(11, 49, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test0814(self):
        out_time = time_diff_calculator(3.2, time(7, 50, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0815(self):
        out_time = time_diff_calculator(6.2, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 42, 0))
    
    def test0816(self):
        out_time = time_diff_calculator(4, time(7, 6, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0817(self):
        out_time = time_diff_calculator(5.8, time(11, 38, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test0818(self):
        out_time = time_diff_calculator(3.9, time(7, 23, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test0819(self):
        out_time = time_diff_calculator(5.6, time(11, 44, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test0820(self):
        out_time = time_diff_calculator(3.7, time(7, 19, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0821(self):
        out_time = time_diff_calculator(5.9, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test0822(self):
        out_time = time_diff_calculator(3.4, time(7, 35, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0823(self):
        out_time = time_diff_calculator(6.2, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 42, 0))
    
    def test0824(self):
        out_time = time_diff_calculator(3.5, time(7, 27, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0825(self):
        out_time = time_diff_calculator(5.9, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test0826(self):
        out_time = time_diff_calculator(3.7, time(7, 20, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0827(self):
        out_time = time_diff_calculator(6.3, time(11, 32, 0))
        self.assertEquals(out_time, time(17, 48, 0))
    
    def test0828(self):
        out_time = time_diff_calculator(3.5, time(7, 37, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0829(self):
        out_time = time_diff_calculator(3.7, time(7, 16, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0830(self):
        out_time = time_diff_calculator(6.7, time(11, 31, 0))
        self.assertEquals(out_time, time(18, 12, 0))
    
    def test0831(self):
        out_time = time_diff_calculator(3.6, time(7, 24, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0832(self):
        out_time = time_diff_calculator(5.8, time(11, 32, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test0833(self):
        out_time = time_diff_calculator(3.4, time(7, 33, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0834(self):
        out_time = time_diff_calculator(6.6, time(11, 31, 0))
        self.assertEquals(out_time, time(18, 6, 0))
    
    def test0835(self):
        out_time = time_diff_calculator(4, time(7, 17, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test0836(self):
        out_time = time_diff_calculator(5.5, time(11, 44, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test0837(self):
        out_time = time_diff_calculator(3.7, time(7, 24, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0838(self):
        out_time = time_diff_calculator(6.3, time(11, 32, 0))
        self.assertEquals(out_time, time(17, 48, 0))
    
    def test0839(self):
        out_time = time_diff_calculator(5.9, time(8, 49, 0))
        self.assertEquals(out_time, time(14, 42, 0))
    
    def test0840(self):
        out_time = time_diff_calculator(3.9, time(7, 34, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test0841(self):
        out_time = time_diff_calculator(5.6, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 36, 0))
    
    def test0842(self):
        out_time = time_diff_calculator(3.7, time(7, 36, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test0843(self):
        out_time = time_diff_calculator(6.2, time(11, 48, 0))
        self.assertEquals(out_time, time(18, 0, 0))
    
    def test0844(self):
        out_time = time_diff_calculator(3.9, time(7, 27, 0))
        self.assertEquals(out_time, time(11, 24, 0))
    
    def test0845(self):
        out_time = time_diff_calculator(6.1, time(11, 55, 0))
        self.assertEquals(out_time, time(18, 0, 0))
    
    def test0846(self):
        out_time = time_diff_calculator(3.8, time(7, 24, 0))
        self.assertEquals(out_time, time(11, 12, 0))
    
    def test0847(self):
        out_time = time_diff_calculator(6.5, time(11, 42, 0))
        self.assertEquals(out_time, time(18, 12, 0))
    
    def test0848(self):
        out_time = time_diff_calculator(3.4, time(7, 39, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0849(self):
        out_time = time_diff_calculator(5.9, time(11, 35, 0))
        self.assertEquals(out_time, time(17, 30, 0))
    
    def test0850(self):
        out_time = time_diff_calculator(3.3, time(8, 11, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test0851(self):
        out_time = time_diff_calculator(4.3, time(11, 49, 0))
        self.assertEquals(out_time, time(16, 6, 0))
    
    def test0852(self):
        out_time = time_diff_calculator(3.6, time(7, 26, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0853(self):
        out_time = time_diff_calculator(7.3, time(11, 32, 0))
        self.assertEquals(out_time, time(18, 48, 0))
    
    def test0854(self):
        out_time = time_diff_calculator(6.9, time(7, 26, 0))
        self.assertEquals(out_time, time(14, 18, 0))
    
    def test0855(self):
        out_time = time_diff_calculator(3.3, time(14, 49, 0))
        self.assertEquals(out_time, time(18, 6, 0))
    
    def test0856(self):
        out_time = time_diff_calculator(7.7, time(7, 39, 0))
        self.assertEquals(out_time, time(15, 24, 0))
    
    def test0857(self):
        out_time = time_diff_calculator(3.4, time(15, 54, 0))
        self.assertEquals(out_time, time(19, 18, 0))
    
    def test0858(self):
        out_time = time_diff_calculator(8.2, time(7, 25, 0))
        self.assertEquals(out_time, time(15, 36, 0))
    
    def test0859(self):
        out_time = time_diff_calculator(3.1, time(16, 1, 0))
        self.assertEquals(out_time, time(19, 6, 0))
    
    def test0860(self):
        out_time = time_diff_calculator(3.4, time(7, 33, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0861(self):
        out_time = time_diff_calculator(7.6, time(11, 31, 0))
        self.assertEquals(out_time, time(19, 6, 0))
    
    def test0862(self):
        out_time = time_diff_calculator(6.6, time(7, 5, 0))
        self.assertEquals(out_time, time(13, 42, 0))
    
    def test0863(self):
        out_time = time_diff_calculator(2.6, time(13, 59, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test0864(self):
        out_time = time_diff_calculator(5.7, time(7, 16, 0))
        self.assertEquals(out_time, time(13, 0, 0))
    
    def test0865(self):
        out_time = time_diff_calculator(4.5, time(13, 30, 0))
        self.assertEquals(out_time, time(18, 0, 0))
    
    def test0866(self):
        out_time = time_diff_calculator(8.2, time(7, 19, 0))
        self.assertEquals(out_time, time(15, 30, 0))
    
    def test0867(self):
        out_time = time_diff_calculator(2.6, time(16, 0, 0))
        self.assertEquals(out_time, time(18, 36, 0))
    
    def test0868(self):
        out_time = time_diff_calculator(3.5, time(7, 27, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0869(self):
        out_time = time_diff_calculator(6.5, time(11, 31, 0))
        self.assertEquals(out_time, time(18, 0, 0))
    
    def test0870(self):
        out_time = time_diff_calculator(3.9, time(7, 22, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test0871(self):
        out_time = time_diff_calculator(7, time(11, 51, 0))
        self.assertEquals(out_time, time(18, 54, 0))
    
    def test0872(self):
        out_time = time_diff_calculator(6.7, time(7, 19, 0))
        self.assertEquals(out_time, time(14, 0, 0))
    
    def test0873(self):
        out_time = time_diff_calculator(3.4, time(14, 22, 0))
        self.assertEquals(out_time, time(17, 48, 0))
    
    def test0874(self):
        out_time = time_diff_calculator(4.9, time(9, 43, 0))
        self.assertEquals(out_time, time(14, 36, 0))
    
    def test0875(self):
        out_time = time_diff_calculator(3.8, time(7, 14, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0876(self):
        out_time = time_diff_calculator(7.1, time(11, 31, 0))
        self.assertEquals(out_time, time(18, 36, 0))
    
    def test0877(self):
        out_time = time_diff_calculator(5.2, time(7, 23, 0))
        self.assertEquals(out_time, time(12, 36, 0))
    
    def test0878(self):
        out_time = time_diff_calculator(5.3, time(12, 59, 0))
        self.assertEquals(out_time, time(18, 18, 0))
    
    def test0879(self):
        out_time = time_diff_calculator(3.8, time(7, 14, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0880(self):
        out_time = time_diff_calculator(5.8, time(11, 30, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test0881(self):
        out_time = time_diff_calculator(3.5, time(7, 27, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0882(self):
        out_time = time_diff_calculator(7.5, time(11, 29, 0))
        self.assertEquals(out_time, time(19, 0, 0))
    
    def test0883(self):
        out_time = time_diff_calculator(4.4, time(8, 33, 0))
        self.assertEquals(out_time, time(13, 0, 0))
    
    def test0884(self):
        out_time = time_diff_calculator(3.7, time(7, 19, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0885(self):
        out_time = time_diff_calculator(7.5, time(11, 32, 0))
        self.assertEquals(out_time, time(19, 0, 0))
    
    def test0886(self):
        out_time = time_diff_calculator(3.7, time(7, 23, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0887(self):
        out_time = time_diff_calculator(7.1, time(11, 37, 0))
        self.assertEquals(out_time, time(18, 42, 0))
    
    def test0888(self):
        out_time = time_diff_calculator(3.8, time(7, 14, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0889(self):
        out_time = time_diff_calculator(6.7, time(11, 32, 0))
        self.assertEquals(out_time, time(18, 12, 0))
    
    def test0890(self):
        out_time = time_diff_calculator(3.6, time(7, 21, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0891(self):
        out_time = time_diff_calculator(7.3, time(11, 31, 0))
        self.assertEquals(out_time, time(18, 48, 0))
    
    def test0892(self):
        out_time = time_diff_calculator(3.8, time(7, 19, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0893(self):
        out_time = time_diff_calculator(7.3, time(11, 31, 0))
        self.assertEquals(out_time, time(18, 48, 0))
    
    def test0894(self):
        out_time = time_diff_calculator(3.7, time(7, 49, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test0895(self):
        out_time = time_diff_calculator(4.4, time(11, 52, 0))
        self.assertEquals(out_time, time(16, 18, 0))
    
    def test0896(self):
        out_time = time_diff_calculator(3.7, time(7, 26, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0897(self):
        out_time = time_diff_calculator(7.2, time(11, 32, 0))
        self.assertEquals(out_time, time(18, 42, 0))
    
    def test0898(self):
        out_time = time_diff_calculator(3.5, time(7, 33, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0899(self):
        out_time = time_diff_calculator(5.8, time(11, 32, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test0900(self):
        out_time = time_diff_calculator(3.9, time(7, 26, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test0901(self):
        out_time = time_diff_calculator(6.4, time(11, 46, 0))
        self.assertEquals(out_time, time(18, 12, 0))
    
    def test0902(self):
        out_time = time_diff_calculator(3.5, time(7, 27, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0903(self):
        out_time = time_diff_calculator(6.5, time(11, 31, 0))
        self.assertEquals(out_time, time(18, 0, 0))
    
    def test0904(self):
        out_time = time_diff_calculator(3.6, time(7, 30, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0905(self):
        out_time = time_diff_calculator(6.8, time(11, 38, 0))
        self.assertEquals(out_time, time(18, 24, 0))
    
    def test0906(self):
        out_time = time_diff_calculator(4.5, time(12, 30, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test0907(self):
        out_time = time_diff_calculator(3.5, time(7, 29, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0908(self):
        out_time = time_diff_calculator(6.3, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 48, 0))
    
    def test0909(self):
        out_time = time_diff_calculator(3.8, time(7, 14, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0910(self):
        out_time = time_diff_calculator(6.7, time(11, 32, 0))
        self.assertEquals(out_time, time(18, 12, 0))
    
    def test0911(self):
        out_time = time_diff_calculator(3.6, time(7, 25, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0912(self):
        out_time = time_diff_calculator(6.8, time(11, 31, 0))
        self.assertEquals(out_time, time(18, 18, 0))
    
    def test0913(self):
        out_time = time_diff_calculator(4, time(7, 23, 0))
        self.assertEquals(out_time, time(11, 24, 0))
    
    def test0914(self):
        out_time = time_diff_calculator(5.3, time(11, 56, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test0915(self):
        out_time = time_diff_calculator(3.5, time(7, 28, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0916(self):
        out_time = time_diff_calculator(5.8, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test0917(self):
        out_time = time_diff_calculator(3.6, time(7, 27, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0918(self):
        out_time = time_diff_calculator(6.4, time(11, 35, 0))
        self.assertEquals(out_time, time(18, 0, 0))
    
    def test0919(self):
        out_time = time_diff_calculator(14.3, time(6, 56, 0))
        self.assertEquals(out_time, time(21, 12, 0))
    
    def test0920(self):
        out_time = time_diff_calculator(3.9, time(7, 25, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test0921(self):
        out_time = time_diff_calculator(6.3, time(11, 46, 0))
        self.assertEquals(out_time, time(18, 6, 0))
    
    def test0922(self):
        out_time = time_diff_calculator(3.7, time(7, 15, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0923(self):
        out_time = time_diff_calculator(6.4, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 54, 0))
    
    def test0924(self):
        out_time = time_diff_calculator(3.6, time(7, 24, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0925(self):
        out_time = time_diff_calculator(6.4, time(11, 32, 0))
        self.assertEquals(out_time, time(17, 54, 0))
    
    def test0926(self):
        out_time = time_diff_calculator(7.7, time(8, 17, 0))
        self.assertEquals(out_time, time(16, 0, 0))
    
    def test0927(self):
        out_time = time_diff_calculator(3.7, time(7, 25, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0928(self):
        out_time = time_diff_calculator(5.9, time(11, 37, 0))
        self.assertEquals(out_time, time(17, 30, 0))
    
    def test0929(self):
        out_time = time_diff_calculator(3.5, time(7, 27, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0930(self):
        out_time = time_diff_calculator(6.7, time(11, 31, 0))
        self.assertEquals(out_time, time(18, 12, 0))
    
    def test0931(self):
        out_time = time_diff_calculator(3.7, time(7, 21, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0932(self):
        out_time = time_diff_calculator(7.3, time(11, 34, 0))
        self.assertEquals(out_time, time(18, 54, 0))
    
    def test0933(self):
        out_time = time_diff_calculator(3.7, time(7, 18, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0934(self):
        out_time = time_diff_calculator(6.8, time(11, 31, 0))
        self.assertEquals(out_time, time(18, 18, 0))
    
    def test0935(self):
        out_time = time_diff_calculator(7, time(9, 7, 0))
        self.assertEquals(out_time, time(16, 6, 0))
    
    def test0936(self):
        out_time = time_diff_calculator(3.5, time(7, 27, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0937(self):
        out_time = time_diff_calculator(6.6, time(11, 29, 0))
        self.assertEquals(out_time, time(18, 6, 0))
    
    def test0938(self):
        out_time = time_diff_calculator(3.9, time(7, 6, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0939(self):
        out_time = time_diff_calculator(5.5, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test0940(self):
        out_time = time_diff_calculator(3.6, time(7, 22, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0941(self):
        out_time = time_diff_calculator(6, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 30, 0))
    
    def test0942(self):
        out_time = time_diff_calculator(3.6, time(7, 22, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0943(self):
        out_time = time_diff_calculator(5, time(11, 31, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test0944(self):
        out_time = time_diff_calculator(3.5, time(7, 29, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0945(self):
        out_time = time_diff_calculator(6.3, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 48, 0))
    
    def test0946(self):
        out_time = time_diff_calculator(3.6, time(7, 21, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0947(self):
        out_time = time_diff_calculator(6.5, time(11, 30, 0))
        self.assertEquals(out_time, time(18, 0, 0))
    
    def test0948(self):
        out_time = time_diff_calculator(3.8, time(7, 25, 0))
        self.assertEquals(out_time, time(11, 12, 0))
    
    def test0949(self):
        out_time = time_diff_calculator(6.1, time(11, 45, 0))
        self.assertEquals(out_time, time(17, 54, 0))
    
    def test0950(self):
        out_time = time_diff_calculator(4, time(7, 22, 0))
        self.assertEquals(out_time, time(11, 24, 0))
    
    def test0951(self):
        out_time = time_diff_calculator(6.1, time(11, 54, 0))
        self.assertEquals(out_time, time(18, 0, 0))
    
    def test0952(self):
        out_time = time_diff_calculator(3.6, time(7, 24, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0953(self):
        out_time = time_diff_calculator(6.5, time(11, 31, 0))
        self.assertEquals(out_time, time(18, 0, 0))
    
    def test0954(self):
        out_time = time_diff_calculator(3.6, time(7, 21, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0955(self):
        out_time = time_diff_calculator(5.8, time(11, 32, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test0956(self):
        out_time = time_diff_calculator(3.6, time(7, 21, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0957(self):
        out_time = time_diff_calculator(6.6, time(11, 31, 0))
        self.assertEquals(out_time, time(18, 6, 0))
    
    def test0958(self):
        out_time = time_diff_calculator(3.6, time(7, 25, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0959(self):
        out_time = time_diff_calculator(6.2, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 42, 0))
    
    def test0960(self):
        out_time = time_diff_calculator(3.8, time(7, 2, 0))
        self.assertEquals(out_time, time(10, 48, 0))
    
    def test0961(self):
        out_time = time_diff_calculator(2.4, time(13, 23, 0))
        self.assertEquals(out_time, time(15, 48, 0))
    
    def test0962(self):
        out_time = time_diff_calculator(3.7, time(7, 16, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0963(self):
        out_time = time_diff_calculator(6, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 30, 0))
    
    def test0964(self):
        out_time = time_diff_calculator(3.8, time(7, 40, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test0965(self):
        out_time = time_diff_calculator(5.7, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 42, 0))
    
    def test0966(self):
        out_time = time_diff_calculator(3.5, time(7, 27, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0967(self):
        out_time = time_diff_calculator(6.3, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 48, 0))
    
    def test0968(self):
        out_time = time_diff_calculator(3.3, time(7, 42, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0969(self):
        out_time = time_diff_calculator(5.9, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test0970(self):
        out_time = time_diff_calculator(3.6, time(7, 29, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0971(self):
        out_time = time_diff_calculator(5.9, time(11, 36, 0))
        self.assertEquals(out_time, time(17, 30, 0))
    
    def test0972(self):
        out_time = time_diff_calculator(6.7, time(10, 25, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test0973(self):
        out_time = time_diff_calculator(3.5, time(7, 30, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0974(self):
        out_time = time_diff_calculator(6.3, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 48, 0))
    
    def test0975(self):
        out_time = time_diff_calculator(3.4, time(7, 35, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0976(self):
        out_time = time_diff_calculator(6.7, time(11, 31, 0))
        self.assertEquals(out_time, time(18, 12, 0))
    
    def test0977(self):
        out_time = time_diff_calculator(6, time(7, 46, 0))
        self.assertEquals(out_time, time(13, 48, 0))
    
    def test0978(self):
        out_time = time_diff_calculator(3.7, time(14, 35, 0))
        self.assertEquals(out_time, time(18, 18, 0))
    
    def test0979(self):
        out_time = time_diff_calculator(3.4, time(7, 38, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0980(self):
        out_time = time_diff_calculator(6.4, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 54, 0))
    
    def test0981(self):
        out_time = time_diff_calculator(3.9, time(7, 31, 0))
        self.assertEquals(out_time, time(11, 24, 0))
    
    def test0982(self):
        out_time = time_diff_calculator(5, time(11, 55, 0))
        self.assertEquals(out_time, time(16, 54, 0))
    
    def test0983(self):
        out_time = time_diff_calculator(7, time(9, 7, 0))
        self.assertEquals(out_time, time(16, 6, 0))
    
    def test0984(self):
        out_time = time_diff_calculator(3.6, time(7, 30, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0985(self):
        out_time = time_diff_calculator(5.7, time(11, 38, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test0986(self):
        out_time = time_diff_calculator(3.7, time(7, 50, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test0987(self):
        out_time = time_diff_calculator(5.6, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 36, 0))
    
    def test0988(self):
        out_time = time_diff_calculator(3.2, time(7, 51, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test0989(self):
        out_time = time_diff_calculator(6.3, time(11, 39, 0))
        self.assertEquals(out_time, time(18, 0, 0))
    
    def test0990(self):
        out_time = time_diff_calculator(6.2, time(10, 47, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test0991(self):
        out_time = time_diff_calculator(3.5, time(7, 32, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0992(self):
        out_time = time_diff_calculator(6.4, time(11, 33, 0))
        self.assertEquals(out_time, time(18, 0, 0))
    
    def test0993(self):
        out_time = time_diff_calculator(3.1, time(7, 51, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0994(self):
        out_time = time_diff_calculator(6.3, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 48, 0))
    
    def test0995(self):
        out_time = time_diff_calculator(3.6, time(7, 22, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0996(self):
        out_time = time_diff_calculator(6.5, time(11, 31, 0))
        self.assertEquals(out_time, time(18, 0, 0))
    
    def test0997(self):
        out_time = time_diff_calculator(6.8, time(7, 51, 0))
        self.assertEquals(out_time, time(14, 42, 0))
    
    def test0998(self):
        out_time = time_diff_calculator(3.8, time(7, 14, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test0999(self):
        out_time = time_diff_calculator(7.2, time(11, 31, 0))
        self.assertEquals(out_time, time(18, 42, 0))
    
    def test1000(self):
        out_time = time_diff_calculator(3, time(7, 59, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test1001(self):
        out_time = time_diff_calculator(6.3, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 48, 0))
    
    def test1002(self):
        out_time = time_diff_calculator(3.5, time(7, 29, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test1003(self):
        out_time = time_diff_calculator(8, time(11, 31, 0))
        self.assertEquals(out_time, time(19, 30, 0))
    
    def test1004(self):
        out_time = time_diff_calculator(3.2, time(7, 48, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test1005(self):
        out_time = time_diff_calculator(6.9, time(11, 31, 0))
        self.assertEquals(out_time, time(18, 24, 0))
    
    def test1006(self):
        out_time = time_diff_calculator(3.6, time(7, 23, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test1007(self):
        out_time = time_diff_calculator(6.6, time(11, 32, 0))
        self.assertEquals(out_time, time(18, 6, 0))
    
    def test1008(self):
        out_time = time_diff_calculator(5.2, time(7, 37, 0))
        self.assertEquals(out_time, time(12, 48, 0))
    
    def test1009(self):
        out_time = time_diff_calculator(3.8, time(7, 14, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test1010(self):
        out_time = time_diff_calculator(7.1, time(11, 32, 0))
        self.assertEquals(out_time, time(18, 36, 0))
    
    def test1011(self):
        out_time = time_diff_calculator(3.3, time(7, 41, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test1012(self):
        out_time = time_diff_calculator(7.3, time(11, 31, 0))
        self.assertEquals(out_time, time(18, 48, 0))
    
    def test1013(self):
        out_time = time_diff_calculator(3.7, time(7, 21, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test1014(self):
        out_time = time_diff_calculator(6.5, time(11, 37, 0))
        self.assertEquals(out_time, time(18, 6, 0))
    
    def test1015(self):
        out_time = time_diff_calculator(3, time(7, 57, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test1016(self):
        out_time = time_diff_calculator(6.3, time(11, 32, 0))
        self.assertEquals(out_time, time(17, 48, 0))
    
    def test1017(self):
        out_time = time_diff_calculator(3.8, time(7, 32, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test1018(self):
        out_time = time_diff_calculator(5.9, time(11, 43, 0))
        self.assertEquals(out_time, time(17, 36, 0))
    
    def test1019(self):
        out_time = time_diff_calculator(4.8, time(8, 17, 0))
        self.assertEquals(out_time, time(13, 6, 0))
    
    def test1020(self):
        out_time = time_diff_calculator(3.5, time(7, 29, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test1021(self):
        out_time = time_diff_calculator(6.3, time(11, 32, 0))
        self.assertEquals(out_time, time(17, 48, 0))
    
    def test1022(self):
        out_time = time_diff_calculator(3.2, time(7, 51, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test1023(self):
        out_time = time_diff_calculator(5.5, time(11, 39, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test1024(self):
        out_time = time_diff_calculator(3.6, time(7, 23, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test1025(self):
        out_time = time_diff_calculator(5.8, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test1026(self):
        out_time = time_diff_calculator(3, time(7, 57, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test1027(self):
        out_time = time_diff_calculator(7.3, time(11, 32, 0))
        self.assertEquals(out_time, time(18, 48, 0))
    
    def test1028(self):
        out_time = time_diff_calculator(4, time(7, 32, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1029(self):
        out_time = time_diff_calculator(6.6, time(12, 0, 0))
        self.assertEquals(out_time, time(18, 36, 0))
    
    def test1030(self):
        out_time = time_diff_calculator(4.2, time(8, 59, 0))
        self.assertEquals(out_time, time(13, 12, 0))
    
    def test1031(self):
        out_time = time_diff_calculator(3.6, time(7, 38, 0))
        self.assertEquals(out_time, time(11, 12, 0))
    
    def test1032(self):
        out_time = time_diff_calculator(6.5, time(11, 44, 0))
        self.assertEquals(out_time, time(18, 12, 0))
    
    def test1033(self):
        out_time = time_diff_calculator(3.4, time(7, 36, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test1034(self):
        out_time = time_diff_calculator(6.6, time(11, 31, 0))
        self.assertEquals(out_time, time(18, 6, 0))
    
    def test1035(self):
        out_time = time_diff_calculator(3.5, time(7, 30, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test1036(self):
        out_time = time_diff_calculator(6.6, time(11, 32, 0))
        self.assertEquals(out_time, time(18, 6, 0))
    
    def test1037(self):
        out_time = time_diff_calculator(3.9, time(7, 22, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test1038(self):
        out_time = time_diff_calculator(8.2, time(11, 49, 0))
        self.assertEquals(out_time, time(20, 0, 0))
    
    def test1039(self):
        out_time = time_diff_calculator(3.8, time(7, 32, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test1040(self):
        out_time = time_diff_calculator(5.5, time(11, 49, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test1041(self):
        out_time = time_diff_calculator(5.1, time(8, 24, 0))
        self.assertEquals(out_time, time(13, 30, 0))
    
    def test1042(self):
        out_time = time_diff_calculator(3.7, time(7, 39, 0))
        self.assertEquals(out_time, time(11, 24, 0))
    
    def test1043(self):
        out_time = time_diff_calculator(6, time(11, 57, 0))
        self.assertEquals(out_time, time(18, 0, 0))
    
    def test1044(self):
        out_time = time_diff_calculator(3.5, time(7, 35, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test1045(self):
        out_time = time_diff_calculator(6.6, time(11, 37, 0))
        self.assertEquals(out_time, time(18, 12, 0))
    
    def test1046(self):
        out_time = time_diff_calculator(3.5, time(7, 28, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test1047(self):
        out_time = time_diff_calculator(6.7, time(11, 31, 0))
        self.assertEquals(out_time, time(18, 12, 0))
    
    def test1048(self):
        out_time = time_diff_calculator(3.2, time(7, 48, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test1049(self):
        out_time = time_diff_calculator(7.1, time(11, 32, 0))
        self.assertEquals(out_time, time(18, 36, 0))
    
    def test1050(self):
        out_time = time_diff_calculator(3.6, time(7, 22, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test1051(self):
        out_time = time_diff_calculator(7.5, time(11, 31, 0))
        self.assertEquals(out_time, time(19, 0, 0))
    
    def test1052(self):
        out_time = time_diff_calculator(4.5, time(9, 10, 0))
        self.assertEquals(out_time, time(13, 42, 0))
    
    def test1053(self):
        out_time = time_diff_calculator(3.7, time(7, 20, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test1054(self):
        out_time = time_diff_calculator(6.9, time(11, 31, 0))
        self.assertEquals(out_time, time(18, 24, 0))
    
    def test1055(self):
        out_time = time_diff_calculator(3.5, time(7, 30, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test1056(self):
        out_time = time_diff_calculator(7, time(11, 31, 0))
        self.assertEquals(out_time, time(18, 30, 0))
    
    def test1057(self):
        out_time = time_diff_calculator(3.1, time(7, 53, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test1058(self):
        out_time = time_diff_calculator(5.7, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test1059(self):
        out_time = time_diff_calculator(3.4, time(7, 42, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test1060(self):
        out_time = time_diff_calculator(6.4, time(11, 33, 0))
        self.assertEquals(out_time, time(18, 0, 0))
    
    def test1061(self):
        out_time = time_diff_calculator(3.4, time(7, 37, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test1062(self):
        out_time = time_diff_calculator(7.2, time(11, 31, 0))
        self.assertEquals(out_time, time(18, 42, 0))
    
    def test1063(self):
        out_time = time_diff_calculator(6.3, time(11, 44, 0))
        self.assertEquals(out_time, time(18, 0, 0))
    
    def test1064(self):
        out_time = time_diff_calculator(3.1, time(7, 52, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test1065(self):
        out_time = time_diff_calculator(6.8, time(11, 31, 0))
        self.assertEquals(out_time, time(18, 18, 0))
    
    def test1066(self):
        out_time = time_diff_calculator(3.7, time(7, 27, 0))
        self.assertEquals(out_time, time(11, 12, 0))
    
    def test1067(self):
        out_time = time_diff_calculator(6.3, time(11, 40, 0))
        self.assertEquals(out_time, time(18, 0, 0))
    
    def test1068(self):
        out_time = time_diff_calculator(3.2, time(7, 47, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test1069(self):
        out_time = time_diff_calculator(7.3, time(11, 31, 0))
        self.assertEquals(out_time, time(18, 48, 0))
    
    def test1070(self):
        out_time = time_diff_calculator(3.4, time(7, 33, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test1071(self):
        out_time = time_diff_calculator(7, time(11, 31, 0))
        self.assertEquals(out_time, time(18, 30, 0))
    
    def test1072(self):
        out_time = time_diff_calculator(5.1, time(12, 53, 0))
        self.assertEquals(out_time, time(18, 0, 0))
    
    def test1073(self):
        out_time = time_diff_calculator(3.6, time(7, 30, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test1074(self):
        out_time = time_diff_calculator(6.2, time(11, 34, 0))
        self.assertEquals(out_time, time(17, 48, 0))
    
    def test1075(self):
        out_time = time_diff_calculator(3.4, time(7, 52, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test1076(self):
        out_time = time_diff_calculator(7.3, time(11, 49, 0))
        self.assertEquals(out_time, time(19, 6, 0))
    
    def test1077(self):
        out_time = time_diff_calculator(3.4, time(7, 36, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test1078(self):
        out_time = time_diff_calculator(6.8, time(11, 31, 0))
        self.assertEquals(out_time, time(18, 18, 0))
    
    def test1079(self):
        out_time = time_diff_calculator(4.3, time(7, 13, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1080(self):
        out_time = time_diff_calculator(6.4, time(12, 1, 0))
        self.assertEquals(out_time, time(18, 24, 0))
    
    def test1081(self):
        out_time = time_diff_calculator(7.5, time(10, 22, 0))
        self.assertEquals(out_time, time(17, 54, 0))
    
    def test1082(self):
        out_time = time_diff_calculator(3.5, time(7, 30, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test1083(self):
        out_time = time_diff_calculator(5.4, time(11, 30, 0))
        self.assertEquals(out_time, time(16, 54, 0))
    
    def test1084(self):
        out_time = time_diff_calculator(3.4, time(7, 55, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test1085(self):
        out_time = time_diff_calculator(7.1, time(11, 49, 0))
        self.assertEquals(out_time, time(18, 54, 0))
    
    def test1086(self):
        out_time = time_diff_calculator(4.9, time(7, 40, 0))
        self.assertEquals(out_time, time(12, 36, 0))
    
    def test1087(self):
        out_time = time_diff_calculator(5.1, time(13, 6, 0))
        self.assertEquals(out_time, time(18, 12, 0))
    
    def test1088(self):
        out_time = time_diff_calculator(3.3, time(7, 39, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test1089(self):
        out_time = time_diff_calculator(6.9, time(11, 35, 0))
        self.assertEquals(out_time, time(18, 30, 0))
    
    def test1090(self):
        out_time = time_diff_calculator(3.4, time(7, 33, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test1091(self):
        out_time = time_diff_calculator(6.9, time(11, 31, 0))
        self.assertEquals(out_time, time(18, 24, 0))
    
    def test1092(self):
        out_time = time_diff_calculator(3.6, time(8, 34, 0))
        self.assertEquals(out_time, time(12, 12, 0))
    
    def test1093(self):
        out_time = time_diff_calculator(3.9, time(12, 31, 0))
        self.assertEquals(out_time, time(16, 24, 0))
    
    def test1094(self):
        out_time = time_diff_calculator(3.1, time(7, 55, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test1095(self):
        out_time = time_diff_calculator(6.5, time(11, 31, 0))
        self.assertEquals(out_time, time(18, 0, 0))
    
    def test1096(self):
        out_time = time_diff_calculator(3.3, time(7, 42, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test1097(self):
        out_time = time_diff_calculator(7.1, time(11, 32, 0))
        self.assertEquals(out_time, time(18, 36, 0))
    
    def test1098(self):
        out_time = time_diff_calculator(4.4, time(7, 48, 0))
        self.assertEquals(out_time, time(12, 12, 0))
    
    def test1099(self):
        out_time = time_diff_calculator(5.4, time(12, 42, 0))
        self.assertEquals(out_time, time(18, 6, 0))
    
    def test1100(self):
        out_time = time_diff_calculator(4.2, time(7, 34, 0))
        self.assertEquals(out_time, time(11, 48, 0))
    
    def test1101(self):
        out_time = time_diff_calculator(6.3, time(12, 20, 0))
        self.assertEquals(out_time, time(18, 36, 0))
    
    def test1102(self):
        out_time = time_diff_calculator(3.3, time(7, 46, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test1103(self):
        out_time = time_diff_calculator(6.4, time(11, 34, 0))
        self.assertEquals(out_time, time(18, 0, 0))
    
    def test1104(self):
        out_time = time_diff_calculator(4.3, time(9, 37, 0))
        self.assertEquals(out_time, time(13, 54, 0))
    
    def test1105(self):
        out_time = time_diff_calculator(3.4, time(7, 35, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test1106(self):
        out_time = time_diff_calculator(6.3, time(11, 32, 0))
        self.assertEquals(out_time, time(17, 48, 0))
    
    def test1107(self):
        out_time = time_diff_calculator(3.1, time(7, 55, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test1108(self):
        out_time = time_diff_calculator(6.7, time(11, 31, 0))
        self.assertEquals(out_time, time(18, 12, 0))
    
    def test1109(self):
        out_time = time_diff_calculator(3.3, time(7, 52, 0))
        self.assertEquals(out_time, time(11, 12, 0))
    
    def test1110(self):
        out_time = time_diff_calculator(5.5, time(11, 44, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test1111(self):
        out_time = time_diff_calculator(3.4, time(7, 37, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test1112(self):
        out_time = time_diff_calculator(6.5, time(11, 32, 0))
        self.assertEquals(out_time, time(18, 0, 0))
    
    def test1113(self):
        out_time = time_diff_calculator(3.4, time(7, 37, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test1114(self):
        out_time = time_diff_calculator(6.5, time(11, 32, 0))
        self.assertEquals(out_time, time(18, 0, 0))
    
    def test1115(self):
        out_time = time_diff_calculator(4.6, time(12, 21, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1116(self):
        out_time = time_diff_calculator(3.5, time(7, 43, 0))
        self.assertEquals(out_time, time(11, 12, 0))
    
    def test1117(self):
        out_time = time_diff_calculator(6, time(11, 42, 0))
        self.assertEquals(out_time, time(17, 42, 0))
    
    def test1118(self):
        out_time = time_diff_calculator(4.7, time(7, 48, 0))
        self.assertEquals(out_time, time(12, 30, 0))
    
    def test1119(self):
        out_time = time_diff_calculator(4.5, time(13, 3, 0))
        self.assertEquals(out_time, time(17, 36, 0))
    
    def test1120(self):
        out_time = time_diff_calculator(3.4, time(7, 42, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test1121(self):
        out_time = time_diff_calculator(6.8, time(11, 37, 0))
        self.assertEquals(out_time, time(18, 24, 0))
    
    def test1122(self):
        out_time = time_diff_calculator(3.4, time(7, 41, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test1123(self):
        out_time = time_diff_calculator(6, time(11, 37, 0))
        self.assertEquals(out_time, time(17, 36, 0))
    
    def test1124(self):
        out_time = time_diff_calculator(3.2, time(7, 40, 0))
        self.assertEquals(out_time, time(10, 54, 0))
    
    def test1125(self):
        out_time = time_diff_calculator(6.8, time(11, 20, 0))
        self.assertEquals(out_time, time(18, 6, 0))
    
    def test1126(self):
        out_time = time_diff_calculator(3.9, time(13, 38, 0))
        self.assertEquals(out_time, time(17, 30, 0))
    
    def test1127(self):
        out_time = time_diff_calculator(3.4, time(7, 49, 0))
        self.assertEquals(out_time, time(11, 12, 0))
    
    def test1128(self):
        out_time = time_diff_calculator(6.2, time(11, 44, 0))
        self.assertEquals(out_time, time(17, 54, 0))
    
    def test1129(self):
        out_time = time_diff_calculator(3.5, time(7, 34, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test1130(self):
        out_time = time_diff_calculator(6.1, time(11, 37, 0))
        self.assertEquals(out_time, time(17, 42, 0))
    
    def test1131(self):
        out_time = time_diff_calculator(4, time(7, 11, 0))
        self.assertEquals(out_time, time(11, 12, 0))
    
    def test1132(self):
        out_time = time_diff_calculator(6.3, time(11, 42, 0))
        self.assertEquals(out_time, time(18, 0, 0))
    
    def test1133(self):
        out_time = time_diff_calculator(3.8, time(7, 20, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test1134(self):
        out_time = time_diff_calculator(7.6, time(11, 37, 0))
        self.assertEquals(out_time, time(19, 12, 0))
    
    def test1135(self):
        out_time = time_diff_calculator(4.2, time(7, 24, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1136(self):
        out_time = time_diff_calculator(6.4, time(12, 1, 0))
        self.assertEquals(out_time, time(18, 24, 0))
    
    def test1137(self):
        out_time = time_diff_calculator(3.9, time(8, 34, 0))
        self.assertEquals(out_time, time(12, 30, 0))
    
    def test1138(self):
        out_time = time_diff_calculator(2.8, time(12, 49, 0))
        self.assertEquals(out_time, time(15, 36, 0))
    
    def test1139(self):
        out_time = time_diff_calculator(3.4, time(7, 49, 0))
        self.assertEquals(out_time, time(11, 12, 0))
    
    def test1140(self):
        out_time = time_diff_calculator(7.3, time(11, 43, 0))
        self.assertEquals(out_time, time(19, 0, 0))
    
    def test1141(self):
        out_time = time_diff_calculator(3.8, time(7, 50, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1142(self):
        out_time = time_diff_calculator(7.1, time(12, 1, 0))
        self.assertEquals(out_time, time(19, 6, 0))
    
    def test1143(self):
        out_time = time_diff_calculator(3.8, time(7, 42, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1144(self):
        out_time = time_diff_calculator(5.9, time(12, 2, 0))
        self.assertEquals(out_time, time(17, 54, 0))
    
    def test1145(self):
        out_time = time_diff_calculator(4.1, time(7, 42, 0))
        self.assertEquals(out_time, time(11, 48, 0))
    
    def test1146(self):
        out_time = time_diff_calculator(7.1, time(12, 14, 0))
        self.assertEquals(out_time, time(19, 18, 0))
    
    def test1147(self):
        out_time = time_diff_calculator(4.8, time(7, 35, 0))
        self.assertEquals(out_time, time(12, 24, 0))
    
    def test1148(self):
        out_time = time_diff_calculator(4.7, time(12, 55, 0))
        self.assertEquals(out_time, time(17, 36, 0))
    
    def test1149(self):
        out_time = time_diff_calculator(4.6, time(7, 12, 0))
        self.assertEquals(out_time, time(11, 48, 0))
    
    def test1150(self):
        out_time = time_diff_calculator(6.4, time(12, 20, 0))
        self.assertEquals(out_time, time(18, 42, 0))
    
    def test1151(self):
        out_time = time_diff_calculator(2.1, time(7, 30, 0))
        self.assertEquals(out_time, time(9, 36, 0))
    
    def test1152(self):
        out_time = time_diff_calculator(6.3, time(11, 46, 0))
        self.assertEquals(out_time, time(18, 6, 0))
    
    def test1153(self):
        out_time = time_diff_calculator(4.3, time(7, 20, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1154(self):
        out_time = time_diff_calculator(6.4, time(12, 7, 0))
        self.assertEquals(out_time, time(18, 30, 0))
    
    def test1155(self):
        out_time = time_diff_calculator(4.4, time(7, 21, 0))
        self.assertEquals(out_time, time(11, 48, 0))
    
    def test1156(self):
        out_time = time_diff_calculator(5.4, time(12, 17, 0))
        self.assertEquals(out_time, time(17, 42, 0))
    
    def test1157(self):
        out_time = time_diff_calculator(4.2, time(7, 23, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1158(self):
        out_time = time_diff_calculator(6, time(12, 8, 0))
        self.assertEquals(out_time, time(18, 6, 0))
    
    def test1159(self):
        out_time = time_diff_calculator(6.1, time(10, 3, 0))
        self.assertEquals(out_time, time(16, 12, 0))
    
    def test1160(self):
        out_time = time_diff_calculator(10.6, time(7, 24, 0))
        self.assertEquals(out_time, time(18, 0, 0))
    
    def test1161(self):
        out_time = time_diff_calculator(4.7, time(7, 19, 0))
        self.assertEquals(out_time, time(12, 0, 0))
    
    def test1162(self):
        out_time = time_diff_calculator(6, time(12, 35, 0))
        self.assertEquals(out_time, time(18, 36, 0))
    
    def test1163(self):
        out_time = time_diff_calculator(4.2, time(7, 25, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1164(self):
        out_time = time_diff_calculator(6.2, time(12, 7, 0))
        self.assertEquals(out_time, time(18, 18, 0))
    
    def test1165(self):
        out_time = time_diff_calculator(4.7, time(7, 24, 0))
        self.assertEquals(out_time, time(12, 6, 0))
    
    def test1166(self):
        out_time = time_diff_calculator(5.4, time(12, 38, 0))
        self.assertEquals(out_time, time(18, 0, 0))
    
    def test1167(self):
        out_time = time_diff_calculator(4.5, time(7, 34, 0))
        self.assertEquals(out_time, time(12, 6, 0))
    
    def test1168(self):
        out_time = time_diff_calculator(4.8, time(12, 37, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test1169(self):
        out_time = time_diff_calculator(6.5, time(8, 17, 0))
        self.assertEquals(out_time, time(14, 48, 0))
    
    def test1170(self):
        out_time = time_diff_calculator(4.2, time(7, 25, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1171(self):
        out_time = time_diff_calculator(5.1, time(12, 6, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test1172(self):
        out_time = time_diff_calculator(4.3, time(7, 20, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1173(self):
        out_time = time_diff_calculator(5.6, time(12, 7, 0))
        self.assertEquals(out_time, time(17, 42, 0))
    
    def test1174(self):
        out_time = time_diff_calculator(10.1, time(7, 30, 0))
        self.assertEquals(out_time, time(17, 36, 0))
    
    def test1175(self):
        out_time = time_diff_calculator(10.6, time(7, 3, 0))
        self.assertEquals(out_time, time(17, 42, 0))
    
    def test1176(self):
        out_time = time_diff_calculator(4.3, time(7, 27, 0))
        self.assertEquals(out_time, time(11, 48, 0))
    
    def test1177(self):
        out_time = time_diff_calculator(6.5, time(12, 19, 0))
        self.assertEquals(out_time, time(18, 48, 0))
    
    def test1178(self):
        out_time = time_diff_calculator(3.6, time(8, 6, 0))
        self.assertEquals(out_time, time(11, 42, 0))
    
    def test1179(self):
        out_time = time_diff_calculator(6.6, time(12, 1, 0))
        self.assertEquals(out_time, time(18, 36, 0))
    
    def test1180(self):
        out_time = time_diff_calculator(4.1, time(7, 28, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1181(self):
        out_time = time_diff_calculator(5.5, time(12, 6, 0))
        self.assertEquals(out_time, time(17, 36, 0))
    
    def test1182(self):
        out_time = time_diff_calculator(4.2, time(7, 31, 0))
        self.assertEquals(out_time, time(11, 42, 0))
    
    def test1183(self):
        out_time = time_diff_calculator(4.8, time(12, 38, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test1184(self):
        out_time = time_diff_calculator(4.4, time(7, 39, 0))
        self.assertEquals(out_time, time(12, 6, 0))
    
    def test1185(self):
        out_time = time_diff_calculator(4.7, time(12, 38, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test1186(self):
        out_time = time_diff_calculator(3.6, time(7, 28, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test1187(self):
        out_time = time_diff_calculator(4.7, time(12, 16, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1188(self):
        out_time = time_diff_calculator(3.5, time(7, 58, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1189(self):
        out_time = time_diff_calculator(4.5, time(12, 28, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1190(self):
        out_time = time_diff_calculator(3.5, time(7, 57, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1191(self):
        out_time = time_diff_calculator(4.5, time(12, 31, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1192(self):
        out_time = time_diff_calculator(5, time(7, 58, 0))
        self.assertEquals(out_time, time(13, 0, 0))
    
    def test1193(self):
        out_time = time_diff_calculator(3, time(14, 2, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1194(self):
        out_time = time_diff_calculator(3.6, time(7, 58, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1195(self):
        out_time = time_diff_calculator(4.4, time(12, 36, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1196(self):
        out_time = time_diff_calculator(3.6, time(7, 57, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1197(self):
        out_time = time_diff_calculator(4.4, time(12, 36, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1198(self):
        out_time = time_diff_calculator(4, time(7, 58, 0))
        self.assertEquals(out_time, time(12, 0, 0))
    
    def test1199(self):
        out_time = time_diff_calculator(4, time(13, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1200(self):
        out_time = time_diff_calculator(4.2, time(7, 59, 0))
        self.assertEquals(out_time, time(12, 12, 0))
    
    def test1201(self):
        out_time = time_diff_calculator(3.8, time(13, 12, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1202(self):
        out_time = time_diff_calculator(3.6, time(7, 58, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1203(self):
        out_time = time_diff_calculator(4.4, time(12, 37, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1204(self):
        out_time = time_diff_calculator(3.6, time(7, 58, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1205(self):
        out_time = time_diff_calculator(4.4, time(12, 6, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test1206(self):
        out_time = time_diff_calculator(3.5, time(7, 58, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1207(self):
        out_time = time_diff_calculator(4.5, time(12, 31, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1208(self):
        out_time = time_diff_calculator(3.6, time(7, 58, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1209(self):
        out_time = time_diff_calculator(4.4, time(12, 36, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1210(self):
        out_time = time_diff_calculator(3.6, time(8, 0, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1211(self):
        out_time = time_diff_calculator(4.3, time(12, 39, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1212(self):
        out_time = time_diff_calculator(0.1, time(17, 1, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test1213(self):
        out_time = time_diff_calculator(0.7, time(7, 36, 0))
        self.assertEquals(out_time, time(8, 18, 0))
    
    def test1214(self):
        out_time = time_diff_calculator(1.9, time(9, 6, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test1215(self):
        out_time = time_diff_calculator(5.4, time(11, 36, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1216(self):
        out_time = time_diff_calculator(3.8, time(8, 2, 0))
        self.assertEquals(out_time, time(11, 48, 0))
    
    def test1217(self):
        out_time = time_diff_calculator(4.2, time(12, 47, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1218(self):
        out_time = time_diff_calculator(3.5, time(7, 58, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1219(self):
        out_time = time_diff_calculator(4.5, time(12, 31, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1220(self):
        out_time = time_diff_calculator(3.5, time(7, 58, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1221(self):
        out_time = time_diff_calculator(4.5, time(12, 31, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1222(self):
        out_time = time_diff_calculator(3.6, time(7, 58, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1223(self):
        out_time = time_diff_calculator(4.5, time(12, 38, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test1224(self):
        out_time = time_diff_calculator(3.5, time(7, 58, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1225(self):
        out_time = time_diff_calculator(4.4, time(12, 37, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1226(self):
        out_time = time_diff_calculator(3.5, time(7, 58, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1227(self):
        out_time = time_diff_calculator(4.5, time(12, 32, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1228(self):
        out_time = time_diff_calculator(3.8, time(7, 58, 0))
        self.assertEquals(out_time, time(11, 48, 0))
    
    def test1229(self):
        out_time = time_diff_calculator(4.2, time(12, 50, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1230(self):
        out_time = time_diff_calculator(3.7, time(7, 46, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1231(self):
        out_time = time_diff_calculator(4.5, time(12, 32, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1232(self):
        out_time = time_diff_calculator(5, time(7, 45, 0))
        self.assertEquals(out_time, time(12, 48, 0))
    
    def test1233(self):
        out_time = time_diff_calculator(3, time(14, 8, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test1234(self):
        out_time = time_diff_calculator(4, time(7, 54, 0))
        self.assertEquals(out_time, time(11, 54, 0))
    
    def test1235(self):
        out_time = time_diff_calculator(3.8, time(13, 11, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1236(self):
        out_time = time_diff_calculator(3.5, time(7, 58, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1237(self):
        out_time = time_diff_calculator(4.5, time(12, 32, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1238(self):
        out_time = time_diff_calculator(3.6, time(7, 59, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1239(self):
        out_time = time_diff_calculator(4.4, time(12, 37, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1240(self):
        out_time = time_diff_calculator(3.5, time(8, 12, 0))
        self.assertEquals(out_time, time(11, 42, 0))
    
    def test1241(self):
        out_time = time_diff_calculator(4.5, time(12, 32, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1242(self):
        out_time = time_diff_calculator(4.1, time(7, 29, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1243(self):
        out_time = time_diff_calculator(3.9, time(12, 34, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test1244(self):
        out_time = time_diff_calculator(3.6, time(8, 2, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1245(self):
        out_time = time_diff_calculator(4.4, time(12, 37, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1246(self):
        out_time = time_diff_calculator(3.5, time(8, 0, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1247(self):
        out_time = time_diff_calculator(7, time(12, 0, 0))
        self.assertEquals(out_time, time(19, 0, 0))
    
    def test1248(self):
        out_time = time_diff_calculator(4.3, time(7, 41, 0))
        self.assertEquals(out_time, time(12, 0, 0))
    
    def test1249(self):
        out_time = time_diff_calculator(5, time(12, 31, 0))
        self.assertEquals(out_time, time(17, 30, 0))
    
    def test1250(self):
        out_time = time_diff_calculator(3.6, time(7, 51, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1251(self):
        out_time = time_diff_calculator(5.2, time(12, 2, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test1252(self):
        out_time = time_diff_calculator(4.2, time(7, 47, 0))
        self.assertEquals(out_time, time(12, 0, 0))
    
    def test1253(self):
        out_time = time_diff_calculator(5.5, time(12, 31, 0))
        self.assertEquals(out_time, time(18, 0, 0))
    
    def test1254(self):
        out_time = time_diff_calculator(4, time(7, 37, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1255(self):
        out_time = time_diff_calculator(5.6, time(12, 8, 0))
        self.assertEquals(out_time, time(17, 42, 0))
    
    def test1256(self):
        out_time = time_diff_calculator(6.4, time(7, 55, 0))
        self.assertEquals(out_time, time(14, 18, 0))
    
    def test1257(self):
        out_time = time_diff_calculator(4.2, time(14, 54, 0))
        self.assertEquals(out_time, time(19, 6, 0))
    
    def test1258(self):
        out_time = time_diff_calculator(3.5, time(6, 35, 0))
        self.assertEquals(out_time, time(10, 6, 0))
    
    def test1259(self):
        out_time = time_diff_calculator(4.3, time(7, 41, 0))
        self.assertEquals(out_time, time(12, 0, 0))
    
    def test1260(self):
        out_time = time_diff_calculator(5.3, time(12, 31, 0))
        self.assertEquals(out_time, time(17, 48, 0))
    
    def test1261(self):
        out_time = time_diff_calculator(3.8, time(7, 41, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1262(self):
        out_time = time_diff_calculator(5, time(12, 2, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1263(self):
        out_time = time_diff_calculator(3.7, time(7, 46, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1264(self):
        out_time = time_diff_calculator(5.9, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 54, 0))
    
    def test1265(self):
        out_time = time_diff_calculator(3.9, time(7, 46, 0))
        self.assertEquals(out_time, time(11, 42, 0))
    
    def test1266(self):
        out_time = time_diff_calculator(5, time(12, 11, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test1267(self):
        out_time = time_diff_calculator(4.6, time(7, 6, 0))
        self.assertEquals(out_time, time(11, 42, 0))
    
    def test1268(self):
        out_time = time_diff_calculator(5.3, time(12, 13, 0))
        self.assertEquals(out_time, time(17, 30, 0))
    
    def test1269(self):
        out_time = time_diff_calculator(4.2, time(7, 23, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1270(self):
        out_time = time_diff_calculator(5.5, time(12, 8, 0))
        self.assertEquals(out_time, time(17, 36, 0))
    
    def test1271(self):
        out_time = time_diff_calculator(4, time(7, 28, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1272(self):
        out_time = time_diff_calculator(4.7, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 42, 0))
    
    def test1273(self):
        out_time = time_diff_calculator(4.4, time(7, 11, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1274(self):
        out_time = time_diff_calculator(5.7, time(12, 8, 0))
        self.assertEquals(out_time, time(17, 48, 0))
    
    def test1275(self):
        out_time = time_diff_calculator(4, time(7, 42, 0))
        self.assertEquals(out_time, time(11, 42, 0))
    
    def test1276(self):
        out_time = time_diff_calculator(6.4, time(12, 14, 0))
        self.assertEquals(out_time, time(18, 36, 0))
    
    def test1277(self):
        out_time = time_diff_calculator(4, time(8, 39, 0))
        self.assertEquals(out_time, time(12, 42, 0))
    
    def test1278(self):
        out_time = time_diff_calculator(3.9, time(7, 41, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1279(self):
        out_time = time_diff_calculator(5, time(12, 8, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test1280(self):
        out_time = time_diff_calculator(3.8, time(7, 45, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1281(self):
        out_time = time_diff_calculator(5.7, time(12, 8, 0))
        self.assertEquals(out_time, time(17, 48, 0))
    
    def test1282(self):
        out_time = time_diff_calculator(4.2, time(7, 45, 0))
        self.assertEquals(out_time, time(12, 0, 0))
    
    def test1283(self):
        out_time = time_diff_calculator(5.2, time(12, 34, 0))
        self.assertEquals(out_time, time(17, 48, 0))
    
    def test1284(self):
        out_time = time_diff_calculator(3.7, time(7, 47, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1285(self):
        out_time = time_diff_calculator(5.3, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test1286(self):
        out_time = time_diff_calculator(3.6, time(6, 35, 0))
        self.assertEquals(out_time, time(10, 12, 0))
    
    def test1287(self):
        out_time = time_diff_calculator(1.7, time(7, 41, 0))
        self.assertEquals(out_time, time(9, 24, 0))
    
    def test1288(self):
        out_time = time_diff_calculator(3.8, time(7, 43, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1289(self):
        out_time = time_diff_calculator(4.3, time(12, 2, 0))
        self.assertEquals(out_time, time(16, 18, 0))
    
    def test1290(self):
        out_time = time_diff_calculator(3.5, time(8, 0, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1291(self):
        out_time = time_diff_calculator(7, time(12, 1, 0))
        self.assertEquals(out_time, time(19, 0, 0))
    
    def test1292(self):
        out_time = time_diff_calculator(3.9, time(7, 42, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1293(self):
        out_time = time_diff_calculator(4.7, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 42, 0))
    
    def test1294(self):
        out_time = time_diff_calculator(3.4, time(8, 3, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1295(self):
        out_time = time_diff_calculator(6.7, time(12, 1, 0))
        self.assertEquals(out_time, time(18, 42, 0))
    
    def test1296(self):
        out_time = time_diff_calculator(3.7, time(5, 31, 0))
        self.assertEquals(out_time, time(9, 12, 0))
    
    def test1297(self):
        out_time = time_diff_calculator(3.9, time(7, 34, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1298(self):
        out_time = time_diff_calculator(6.7, time(12, 1, 0))
        self.assertEquals(out_time, time(18, 42, 0))
    
    def test1299(self):
        out_time = time_diff_calculator(3.9, time(7, 35, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1300(self):
        out_time = time_diff_calculator(4.7, time(12, 2, 0))
        self.assertEquals(out_time, time(16, 42, 0))
    
    def test1301(self):
        out_time = time_diff_calculator(3.7, time(7, 49, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1302(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1303(self):
        out_time = time_diff_calculator(4.1, time(7, 25, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1304(self):
        out_time = time_diff_calculator(4.9, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 54, 0))
    
    def test1305(self):
        out_time = time_diff_calculator(3.7, time(7, 47, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1306(self):
        out_time = time_diff_calculator(5.1, time(12, 2, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test1307(self):
        out_time = time_diff_calculator(5, time(7, 13, 0))
        self.assertEquals(out_time, time(12, 12, 0))
    
    def test1308(self):
        out_time = time_diff_calculator(4.4, time(7, 54, 0))
        self.assertEquals(out_time, time(12, 18, 0))
    
    def test1309(self):
        out_time = time_diff_calculator(3.4, time(13, 20, 0))
        self.assertEquals(out_time, time(16, 42, 0))
    
    def test1310(self):
        out_time = time_diff_calculator(4.1, time(17, 56, 0))
        self.assertEquals(out_time, time(22, 0, 0))
    
    def test1311(self):
        out_time = time_diff_calculator(3.9, time(7, 46, 0))
        self.assertEquals(out_time, time(11, 42, 0))
    
    def test1312(self):
        out_time = time_diff_calculator(6, time(12, 14, 0))
        self.assertEquals(out_time, time(18, 12, 0))
    
    def test1313(self):
        out_time = time_diff_calculator(3.7, time(7, 54, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1314(self):
        out_time = time_diff_calculator(5.9, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 54, 0))
    
    def test1315(self):
        out_time = time_diff_calculator(3.9, time(7, 48, 0))
        self.assertEquals(out_time, time(11, 42, 0))
    
    def test1316(self):
        out_time = time_diff_calculator(4.7, time(12, 15, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1317(self):
        out_time = time_diff_calculator(4.5, time(7, 52, 0))
        self.assertEquals(out_time, time(12, 24, 0))
    
    def test1318(self):
        out_time = time_diff_calculator(6.4, time(13, 8, 0))
        self.assertEquals(out_time, time(19, 30, 0))
    
    def test1319(self):
        out_time = time_diff_calculator(5.9, time(8, 12, 0))
        self.assertEquals(out_time, time(14, 6, 0))
    
    def test1320(self):
        out_time = time_diff_calculator(3.1, time(14, 33, 0))
        self.assertEquals(out_time, time(17, 42, 0))
    
    def test1321(self):
        out_time = time_diff_calculator(5.2, time(9, 41, 0))
        self.assertEquals(out_time, time(14, 54, 0))
    
    def test1322(self):
        out_time = time_diff_calculator(3.8, time(7, 54, 0))
        self.assertEquals(out_time, time(11, 42, 0))
    
    def test1323(self):
        out_time = time_diff_calculator(5.5, time(12, 14, 0))
        self.assertEquals(out_time, time(17, 42, 0))
    
    def test1324(self):
        out_time = time_diff_calculator(3.6, time(7, 54, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1325(self):
        out_time = time_diff_calculator(5.3, time(12, 2, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test1326(self):
        out_time = time_diff_calculator(3.5, time(7, 58, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1327(self):
        out_time = time_diff_calculator(5.1, time(12, 2, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test1328(self):
        out_time = time_diff_calculator(4.1, time(7, 56, 0))
        self.assertEquals(out_time, time(12, 0, 0))
    
    def test1329(self):
        out_time = time_diff_calculator(4.5, time(12, 36, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test1330(self):
        out_time = time_diff_calculator(3.7, time(7, 47, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1331(self):
        out_time = time_diff_calculator(5.7, time(12, 2, 0))
        self.assertEquals(out_time, time(17, 42, 0))
    
    def test1332(self):
        out_time = time_diff_calculator(3.8, time(7, 44, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1333(self):
        out_time = time_diff_calculator(4.5, time(12, 0, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test1334(self):
        out_time = time_diff_calculator(3.6, time(7, 55, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1335(self):
        out_time = time_diff_calculator(5.3, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test1336(self):
        out_time = time_diff_calculator(3.7, time(8, 0, 0))
        self.assertEquals(out_time, time(11, 42, 0))
    
    def test1337(self):
        out_time = time_diff_calculator(5.4, time(12, 14, 0))
        self.assertEquals(out_time, time(17, 36, 0))
    
    def test1338(self):
        out_time = time_diff_calculator(4.2, time(7, 56, 0))
        self.assertEquals(out_time, time(12, 6, 0))
    
    def test1339(self):
        out_time = time_diff_calculator(4.3, time(12, 57, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test1340(self):
        out_time = time_diff_calculator(3.7, time(7, 56, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1341(self):
        out_time = time_diff_calculator(5.4, time(12, 8, 0))
        self.assertEquals(out_time, time(17, 30, 0))
    
    def test1342(self):
        out_time = time_diff_calculator(3.9, time(7, 50, 0))
        self.assertEquals(out_time, time(11, 42, 0))
    
    def test1343(self):
        out_time = time_diff_calculator(5, time(12, 14, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test1344(self):
        out_time = time_diff_calculator(3.7, time(7, 46, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1345(self):
        out_time = time_diff_calculator(5.4, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test1346(self):
        out_time = time_diff_calculator(3.7, time(7, 53, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1347(self):
        out_time = time_diff_calculator(5.1, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test1348(self):
        out_time = time_diff_calculator(3.6, time(7, 53, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1349(self):
        out_time = time_diff_calculator(5.2, time(12, 11, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test1350(self):
        out_time = time_diff_calculator(4.2, time(7, 25, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1351(self):
        out_time = time_diff_calculator(5.2, time(12, 8, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test1352(self):
        out_time = time_diff_calculator(3.7, time(7, 49, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1353(self):
        out_time = time_diff_calculator(6.3, time(12, 0, 0))
        self.assertEquals(out_time, time(18, 18, 0))
    
    def test1354(self):
        out_time = time_diff_calculator(3.9, time(7, 33, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1355(self):
        out_time = time_diff_calculator(5.9, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 54, 0))
    
    def test1356(self):
        out_time = time_diff_calculator(3.7, time(7, 56, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1357(self):
        out_time = time_diff_calculator(5.8, time(12, 9, 0))
        self.assertEquals(out_time, time(18, 0, 0))
    
    def test1358(self):
        out_time = time_diff_calculator(4, time(7, 39, 0))
        self.assertEquals(out_time, time(11, 42, 0))
    
    def test1359(self):
        out_time = time_diff_calculator(5.3, time(12, 14, 0))
        self.assertEquals(out_time, time(17, 30, 0))
    
    def test1360(self):
        out_time = time_diff_calculator(4.5, time(7, 34, 0))
        self.assertEquals(out_time, time(12, 6, 0))
    
    def test1361(self):
        out_time = time_diff_calculator(5.9, time(12, 36, 0))
        self.assertEquals(out_time, time(18, 30, 0))
    
    def test1362(self):
        out_time = time_diff_calculator(2, time(9, 28, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1363(self):
        out_time = time_diff_calculator(3.6, time(7, 38, 0))
        self.assertEquals(out_time, time(11, 12, 0))
    
    def test1364(self):
        out_time = time_diff_calculator(6.9, time(12, 5, 0))
        self.assertEquals(out_time, time(19, 0, 0))
    
    def test1365(self):
        out_time = time_diff_calculator(3.7, time(7, 45, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1366(self):
        out_time = time_diff_calculator(6.2, time(12, 2, 0))
        self.assertEquals(out_time, time(18, 12, 0))
    
    def test1367(self):
        out_time = time_diff_calculator(3.6, time(7, 51, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1368(self):
        out_time = time_diff_calculator(4.6, time(12, 2, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test1369(self):
        out_time = time_diff_calculator(4, time(7, 53, 0))
        self.assertEquals(out_time, time(11, 54, 0))
    
    def test1370(self):
        out_time = time_diff_calculator(6.7, time(12, 25, 0))
        self.assertEquals(out_time, time(19, 6, 0))
    
    def test1371(self):
        out_time = time_diff_calculator(3, time(6, 7, 0))
        self.assertEquals(out_time, time(9, 6, 0))
    
    def test1372(self):
        out_time = time_diff_calculator(3.7, time(7, 52, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1373(self):
        out_time = time_diff_calculator(7, time(12, 8, 0))
        self.assertEquals(out_time, time(19, 6, 0))
    
    def test1374(self):
        out_time = time_diff_calculator(3.5, time(8, 5, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1375(self):
        out_time = time_diff_calculator(5.6, time(12, 8, 0))
        self.assertEquals(out_time, time(17, 42, 0))
    
    def test1376(self):
        out_time = time_diff_calculator(3.3, time(7, 50, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test1377(self):
        out_time = time_diff_calculator(6.8, time(11, 37, 0))
        self.assertEquals(out_time, time(18, 24, 0))
    
    def test1378(self):
        out_time = time_diff_calculator(3.5, time(8, 0, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1379(self):
        out_time = time_diff_calculator(6.1, time(12, 1, 0))
        self.assertEquals(out_time, time(18, 6, 0))
    
    def test1380(self):
        out_time = time_diff_calculator(3.7, time(7, 45, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1381(self):
        out_time = time_diff_calculator(7.2, time(12, 1, 0))
        self.assertEquals(out_time, time(19, 12, 0))
    
    def test1382(self):
        out_time = time_diff_calculator(5.9, time(7, 50, 0))
        self.assertEquals(out_time, time(13, 42, 0))
    
    def test1383(self):
        out_time = time_diff_calculator(4.1, time(7, 24, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1384(self):
        out_time = time_diff_calculator(5, time(12, 3, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test1385(self):
        out_time = time_diff_calculator(4.1, time(7, 25, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1386(self):
        out_time = time_diff_calculator(6.6, time(12, 2, 0))
        self.assertEquals(out_time, time(18, 36, 0))
    
    def test1387(self):
        out_time = time_diff_calculator(3.4, time(7, 57, 0))
        self.assertEquals(out_time, time(11, 24, 0))
    
    def test1388(self):
        out_time = time_diff_calculator(6.3, time(11, 55, 0))
        self.assertEquals(out_time, time(18, 12, 0))
    
    def test1389(self):
        out_time = time_diff_calculator(3.7, time(7, 46, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1390(self):
        out_time = time_diff_calculator(7.4, time(12, 0, 0))
        self.assertEquals(out_time, time(19, 24, 0))
    
    def test1391(self):
        out_time = time_diff_calculator(3.6, time(7, 52, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1392(self):
        out_time = time_diff_calculator(7.3, time(12, 1, 0))
        self.assertEquals(out_time, time(19, 18, 0))
    
    def test1393(self):
        out_time = time_diff_calculator(7.4, time(7, 26, 0))
        self.assertEquals(out_time, time(14, 48, 0))
    
    def test1394(self):
        out_time = time_diff_calculator(3.9, time(7, 33, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1395(self):
        out_time = time_diff_calculator(5.5, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 30, 0))
    
    def test1396(self):
        out_time = time_diff_calculator(3.8, time(7, 39, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1397(self):
        out_time = time_diff_calculator(6.1, time(12, 1, 0))
        self.assertEquals(out_time, time(18, 6, 0))
    
    def test1398(self):
        out_time = time_diff_calculator(0.5, time(7, 56, 0))
        self.assertEquals(out_time, time(8, 24, 0))
    
    def test1399(self):
        out_time = time_diff_calculator(1.5, time(10, 28, 0))
        self.assertEquals(out_time, time(12, 0, 0))
    
    def test1400(self):
        out_time = time_diff_calculator(5, time(12, 54, 0))
        self.assertEquals(out_time, time(17, 54, 0))
    
    def test1401(self):
        out_time = time_diff_calculator(4.1, time(7, 22, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1402(self):
        out_time = time_diff_calculator(7.4, time(12, 1, 0))
        self.assertEquals(out_time, time(19, 24, 0))
    
    def test1403(self):
        out_time = time_diff_calculator(3.9, time(7, 38, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1404(self):
        out_time = time_diff_calculator(5.5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 30, 0))
    
    def test1405(self):
        out_time = time_diff_calculator(5.1, time(9, 4, 0))
        self.assertEquals(out_time, time(14, 12, 0))
    
    def test1406(self):
        out_time = time_diff_calculator(3.9, time(7, 56, 0))
        self.assertEquals(out_time, time(11, 48, 0))
    
    def test1407(self):
        out_time = time_diff_calculator(4.8, time(12, 18, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test1408(self):
        out_time = time_diff_calculator(4, time(7, 38, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1409(self):
        out_time = time_diff_calculator(6.9, time(12, 7, 0))
        self.assertEquals(out_time, time(19, 0, 0))
    
    def test1410(self):
        out_time = time_diff_calculator(3.7, time(7, 52, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1411(self):
        out_time = time_diff_calculator(5.9, time(12, 8, 0))
        self.assertEquals(out_time, time(18, 0, 0))
    
    def test1412(self):
        out_time = time_diff_calculator(3.7, time(7, 45, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1413(self):
        out_time = time_diff_calculator(6.5, time(12, 1, 0))
        self.assertEquals(out_time, time(18, 30, 0))
    
    def test1414(self):
        out_time = time_diff_calculator(3.7, time(7, 50, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1415(self):
        out_time = time_diff_calculator(5.2, time(12, 2, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test1416(self):
        out_time = time_diff_calculator(4.8, time(7, 39, 0))
        self.assertEquals(out_time, time(12, 30, 0))
    
    def test1417(self):
        out_time = time_diff_calculator(4.1, time(7, 22, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1418(self):
        out_time = time_diff_calculator(5.2, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test1419(self):
        out_time = time_diff_calculator(4.5, time(7, 33, 0))
        self.assertEquals(out_time, time(12, 6, 0))
    
    def test1420(self):
        out_time = time_diff_calculator(6.2, time(12, 31, 0))
        self.assertEquals(out_time, time(18, 42, 0))
    
    def test1421(self):
        out_time = time_diff_calculator(4, time(7, 31, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1422(self):
        out_time = time_diff_calculator(6.1, time(12, 2, 0))
        self.assertEquals(out_time, time(18, 6, 0))
    
    def test1423(self):
        out_time = time_diff_calculator(4.2, time(7, 28, 0))
        self.assertEquals(out_time, time(11, 42, 0))
    
    def test1424(self):
        out_time = time_diff_calculator(6.4, time(12, 14, 0))
        self.assertEquals(out_time, time(18, 36, 0))
    
    def test1425(self):
        out_time = time_diff_calculator(3.9, time(7, 36, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1426(self):
        out_time = time_diff_calculator(6.6, time(12, 1, 0))
        self.assertEquals(out_time, time(18, 36, 0))
    
    def test1427(self):
        out_time = time_diff_calculator(6.7, time(7, 47, 0))
        self.assertEquals(out_time, time(14, 30, 0))
    
    def test1428(self):
        out_time = time_diff_calculator(2.2, time(14, 51, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test1429(self):
        out_time = time_diff_calculator(3.9, time(7, 36, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1430(self):
        out_time = time_diff_calculator(5.2, time(12, 2, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test1431(self):
        out_time = time_diff_calculator(3.9, time(7, 44, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1432(self):
        out_time = time_diff_calculator(5.8, time(12, 8, 0))
        self.assertEquals(out_time, time(17, 54, 0))
    
    def test1433(self):
        out_time = time_diff_calculator(3.3, time(8, 3, 0))
        self.assertEquals(out_time, time(11, 24, 0))
    
    def test1434(self):
        out_time = time_diff_calculator(5, time(13, 0, 0))
        self.assertEquals(out_time, time(18, 0, 0))
    
    def test1435(self):
        out_time = time_diff_calculator(3.9, time(7, 45, 0))
        self.assertEquals(out_time, time(11, 42, 0))
    
    def test1436(self):
        out_time = time_diff_calculator(6.8, time(12, 14, 0))
        self.assertEquals(out_time, time(19, 0, 0))
    
    def test1437(self):
        out_time = time_diff_calculator(3.7, time(7, 53, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1438(self):
        out_time = time_diff_calculator(7.1, time(12, 8, 0))
        self.assertEquals(out_time, time(19, 12, 0))
    
    def test1439(self):
        out_time = time_diff_calculator(5.7, time(8, 55, 0))
        self.assertEquals(out_time, time(14, 36, 0))
    
    def test1440(self):
        out_time = time_diff_calculator(3.7, time(7, 47, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1441(self):
        out_time = time_diff_calculator(7.3, time(12, 1, 0))
        self.assertEquals(out_time, time(19, 18, 0))
    
    def test1442(self):
        out_time = time_diff_calculator(3.7, time(7, 50, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1443(self):
        out_time = time_diff_calculator(6, time(12, 2, 0))
        self.assertEquals(out_time, time(18, 0, 0))
    
    def test1444(self):
        out_time = time_diff_calculator(3.7, time(7, 50, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1445(self):
        out_time = time_diff_calculator(6.4, time(12, 1, 0))
        self.assertEquals(out_time, time(18, 24, 0))
    
    def test1446(self):
        out_time = time_diff_calculator(3.7, time(7, 51, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1447(self):
        out_time = time_diff_calculator(7, time(12, 1, 0))
        self.assertEquals(out_time, time(19, 0, 0))
    
    def test1448(self):
        out_time = time_diff_calculator(3.9, time(7, 46, 0))
        self.assertEquals(out_time, time(11, 42, 0))
    
    def test1449(self):
        out_time = time_diff_calculator(7, time(12, 16, 0))
        self.assertEquals(out_time, time(19, 18, 0))
    
    def test1450(self):
        out_time = time_diff_calculator(6.7, time(7, 43, 0))
        self.assertEquals(out_time, time(14, 24, 0))
    
    def test1451(self):
        out_time = time_diff_calculator(4.4, time(7, 40, 0))
        self.assertEquals(out_time, time(12, 6, 0))
    
    def test1452(self):
        out_time = time_diff_calculator(6.1, time(12, 37, 0))
        self.assertEquals(out_time, time(18, 42, 0))
    
    def test1453(self):
        out_time = time_diff_calculator(4, time(7, 31, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1454(self):
        out_time = time_diff_calculator(6.5, time(12, 2, 0))
        self.assertEquals(out_time, time(18, 30, 0))
    
    def test1455(self):
        out_time = time_diff_calculator(4.1, time(7, 43, 0))
        self.assertEquals(out_time, time(11, 48, 0))
    
    def test1456(self):
        out_time = time_diff_calculator(5.7, time(12, 16, 0))
        self.assertEquals(out_time, time(18, 0, 0))
    
    def test1457(self):
        out_time = time_diff_calculator(4.2, time(7, 16, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1458(self):
        out_time = time_diff_calculator(7, time(12, 1, 0))
        self.assertEquals(out_time, time(19, 0, 0))
    
    def test1459(self):
        out_time = time_diff_calculator(1.8, time(7, 49, 0))
        self.assertEquals(out_time, time(9, 36, 0))
    
    def test1460(self):
        out_time = time_diff_calculator(5, time(13, 17, 0))
        self.assertEquals(out_time, time(18, 18, 0))
    
    def test1461(self):
        out_time = time_diff_calculator(6.6, time(8, 48, 0))
        self.assertEquals(out_time, time(15, 24, 0))
    
    def test1462(self):
        out_time = time_diff_calculator(3.8, time(7, 44, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1463(self):
        out_time = time_diff_calculator(5.4, time(12, 9, 0))
        self.assertEquals(out_time, time(17, 36, 0))
    
    def test1464(self):
        out_time = time_diff_calculator(3.7, time(7, 48, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1465(self):
        out_time = time_diff_calculator(7, time(12, 1, 0))
        self.assertEquals(out_time, time(19, 0, 0))
    
    def test1466(self):
        out_time = time_diff_calculator(3.8, time(7, 42, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1467(self):
        out_time = time_diff_calculator(6.6, time(12, 2, 0))
        self.assertEquals(out_time, time(18, 36, 0))
    
    def test1468(self):
        out_time = time_diff_calculator(3.7, time(7, 50, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1469(self):
        out_time = time_diff_calculator(7.1, time(12, 1, 0))
        self.assertEquals(out_time, time(19, 6, 0))
    
    def test1470(self):
        out_time = time_diff_calculator(3.3, time(8, 19, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1471(self):
        out_time = time_diff_calculator(3.7, time(7, 53, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1472(self):
        out_time = time_diff_calculator(7.2, time(12, 7, 0))
        self.assertEquals(out_time, time(19, 18, 0))
    
    def test1473(self):
        out_time = time_diff_calculator(3.5, time(7, 59, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1474(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1475(self):
        out_time = time_diff_calculator(3.6, time(7, 55, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1476(self):
        out_time = time_diff_calculator(5.4, time(12, 43, 0))
        self.assertEquals(out_time, time(18, 6, 0))
    
    def test1477(self):
        out_time = time_diff_calculator(4.1, time(7, 27, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1478(self):
        out_time = time_diff_calculator(6.8, time(12, 0, 0))
        self.assertEquals(out_time, time(18, 48, 0))
    
    def test1479(self):
        out_time = time_diff_calculator(3.6, time(7, 52, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1480(self):
        out_time = time_diff_calculator(5.4, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test1481(self):
        out_time = time_diff_calculator(4.1, time(12, 34, 0))
        self.assertEquals(out_time, time(16, 42, 0))
    
    def test1482(self):
        out_time = time_diff_calculator(3.9, time(7, 50, 0))
        self.assertEquals(out_time, time(11, 42, 0))
    
    def test1483(self):
        out_time = time_diff_calculator(6.9, time(12, 37, 0))
        self.assertEquals(out_time, time(19, 30, 0))
    
    def test1484(self):
        out_time = time_diff_calculator(3.7, time(7, 44, 0))
        self.assertEquals(out_time, time(11, 24, 0))
    
    def test1485(self):
        out_time = time_diff_calculator(6, time(11, 55, 0))
        self.assertEquals(out_time, time(17, 54, 0))
    
    def test1486(self):
        out_time = time_diff_calculator(3.8, time(7, 44, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1487(self):
        out_time = time_diff_calculator(6.9, time(12, 1, 0))
        self.assertEquals(out_time, time(18, 54, 0))
    
    def test1488(self):
        out_time = time_diff_calculator(3.7, time(7, 51, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1489(self):
        out_time = time_diff_calculator(4.9, time(12, 6, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1490(self):
        out_time = time_diff_calculator(4.2, time(7, 25, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1491(self):
        out_time = time_diff_calculator(5.3, time(12, 7, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test1492(self):
        out_time = time_diff_calculator(3.9, time(7, 37, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1493(self):
        out_time = time_diff_calculator(6, time(12, 1, 0))
        self.assertEquals(out_time, time(18, 0, 0))
    
    def test1494(self):
        out_time = time_diff_calculator(3.8, time(7, 42, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1495(self):
        out_time = time_diff_calculator(7.3, time(12, 2, 0))
        self.assertEquals(out_time, time(19, 18, 0))
    
    def test1496(self):
        out_time = time_diff_calculator(3.6, time(7, 56, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1497(self):
        out_time = time_diff_calculator(7.9, time(12, 2, 0))
        self.assertEquals(out_time, time(19, 54, 0))
    
    def test1498(self):
        out_time = time_diff_calculator(3.6, time(7, 54, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1499(self):
        out_time = time_diff_calculator(5.3, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test1500(self):
        out_time = time_diff_calculator(3.7, time(7, 48, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1501(self):
        out_time = time_diff_calculator(6.6, time(12, 1, 0))
        self.assertEquals(out_time, time(18, 36, 0))
    
    def test1502(self):
        out_time = time_diff_calculator(3.8, time(7, 39, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1503(self):
        out_time = time_diff_calculator(4.6, time(12, 3, 0))
        self.assertEquals(out_time, time(16, 42, 0))
    
    def test1504(self):
        out_time = time_diff_calculator(5, time(11, 53, 0))
        self.assertEquals(out_time, time(16, 54, 0))
    
    def test1505(self):
        out_time = time_diff_calculator(4.3, time(7, 18, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1506(self):
        out_time = time_diff_calculator(7.1, time(12, 7, 0))
        self.assertEquals(out_time, time(19, 12, 0))
    
    def test1507(self):
        out_time = time_diff_calculator(4.2, time(7, 23, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1508(self):
        out_time = time_diff_calculator(5.9, time(12, 8, 0))
        self.assertEquals(out_time, time(18, 0, 0))
    
    def test1509(self):
        out_time = time_diff_calculator(4, time(7, 44, 0))
        self.assertEquals(out_time, time(11, 42, 0))
    
    def test1510(self):
        out_time = time_diff_calculator(5.6, time(12, 14, 0))
        self.assertEquals(out_time, time(17, 48, 0))
    
    def test1511(self):
        out_time = time_diff_calculator(4.2, time(7, 19, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1512(self):
        out_time = time_diff_calculator(6.2, time(12, 1, 0))
        self.assertEquals(out_time, time(18, 12, 0))
    
    def test1513(self):
        out_time = time_diff_calculator(3.8, time(7, 39, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1514(self):
        out_time = time_diff_calculator(4.8, time(12, 2, 0))
        self.assertEquals(out_time, time(16, 48, 0))
    
    def test1515(self):
        out_time = time_diff_calculator(3.6, time(7, 55, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1516(self):
        out_time = time_diff_calculator(5.2, time(12, 2, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test1517(self):
        out_time = time_diff_calculator(3.3, time(12, 29, 0))
        self.assertEquals(out_time, time(15, 48, 0))
    
    def test1518(self):
        out_time = time_diff_calculator(9.1, time(9, 2, 0))
        self.assertEquals(out_time, time(18, 6, 0))
    
    def test1519(self):
        out_time = time_diff_calculator(8.1, time(10, 3, 0))
        self.assertEquals(out_time, time(18, 12, 0))
    
    def test1520(self):
        out_time = time_diff_calculator(4.1, time(7, 24, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1521(self):
        out_time = time_diff_calculator(7.5, time(12, 2, 0))
        self.assertEquals(out_time, time(19, 30, 0))
    
    def test1522(self):
        out_time = time_diff_calculator(4.2, time(7, 26, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1523(self):
        out_time = time_diff_calculator(5.5, time(12, 6, 0))
        self.assertEquals(out_time, time(17, 36, 0))
    
    def test1524(self):
        out_time = time_diff_calculator(3.9, time(7, 39, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1525(self):
        out_time = time_diff_calculator(6.1, time(12, 9, 0))
        self.assertEquals(out_time, time(18, 18, 0))
    
    def test1526(self):
        out_time = time_diff_calculator(4.2, time(7, 18, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1527(self):
        out_time = time_diff_calculator(5.3, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test1528(self):
        out_time = time_diff_calculator(4.5, time(7, 30, 0))
        self.assertEquals(out_time, time(12, 0, 0))
    
    def test1529(self):
        out_time = time_diff_calculator(5.6, time(12, 32, 0))
        self.assertEquals(out_time, time(18, 6, 0))
    
    def test1530(self):
        out_time = time_diff_calculator(5.4, time(9, 14, 0))
        self.assertEquals(out_time, time(14, 36, 0))
    
    def test1531(self):
        out_time = time_diff_calculator(4.4, time(7, 13, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1532(self):
        out_time = time_diff_calculator(9, time(12, 5, 0))
        self.assertEquals(out_time, time(21, 6, 0))
    
    def test1533(self):
        out_time = time_diff_calculator(4.4, time(7, 13, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1534(self):
        out_time = time_diff_calculator(7.2, time(12, 7, 0))
        self.assertEquals(out_time, time(19, 18, 0))
    
    def test1535(self):
        out_time = time_diff_calculator(4.3, time(7, 21, 0))
        self.assertEquals(out_time, time(11, 42, 0))
    
    def test1536(self):
        out_time = time_diff_calculator(5.3, time(12, 3, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test1537(self):
        out_time = time_diff_calculator(4.2, time(7, 25, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1538(self):
        out_time = time_diff_calculator(4.8, time(12, 8, 0))
        self.assertEquals(out_time, time(16, 54, 0))
    
    def test1539(self):
        out_time = time_diff_calculator(3.5, time(7, 59, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1540(self):
        out_time = time_diff_calculator(6.5, time(12, 2, 0))
        self.assertEquals(out_time, time(18, 30, 0))
    
    def test1541(self):
        out_time = time_diff_calculator(4.2, time(8, 51, 0))
        self.assertEquals(out_time, time(13, 6, 0))
    
    def test1542(self):
        out_time = time_diff_calculator(4.2, time(7, 27, 0))
        self.assertEquals(out_time, time(11, 42, 0))
    
    def test1543(self):
        out_time = time_diff_calculator(6.6, time(12, 14, 0))
        self.assertEquals(out_time, time(18, 48, 0))
    
    def test1544(self):
        out_time = time_diff_calculator(4.3, time(7, 17, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1545(self):
        out_time = time_diff_calculator(6.1, time(12, 2, 0))
        self.assertEquals(out_time, time(18, 6, 0))
    
    def test1546(self):
        out_time = time_diff_calculator(3.9, time(7, 37, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1547(self):
        out_time = time_diff_calculator(6, time(12, 2, 0))
        self.assertEquals(out_time, time(18, 0, 0))
    
    def test1548(self):
        out_time = time_diff_calculator(4.3, time(7, 12, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1549(self):
        out_time = time_diff_calculator(6.5, time(12, 2, 0))
        self.assertEquals(out_time, time(18, 30, 0))
    
    def test1550(self):
        out_time = time_diff_calculator(4, time(7, 27, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1551(self):
        out_time = time_diff_calculator(5.4, time(12, 2, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test1552(self):
        out_time = time_diff_calculator(4, time(7, 29, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1553(self):
        out_time = time_diff_calculator(5.4, time(12, 2, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test1554(self):
        out_time = time_diff_calculator(4.3, time(7, 12, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1555(self):
        out_time = time_diff_calculator(5, time(12, 6, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test1556(self):
        out_time = time_diff_calculator(4.7, time(7, 32, 0))
        self.assertEquals(out_time, time(12, 12, 0))
    
    def test1557(self):
        out_time = time_diff_calculator(4.3, time(12, 44, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1558(self):
        out_time = time_diff_calculator(3.9, time(7, 35, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1559(self):
        out_time = time_diff_calculator(5.3, time(12, 2, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test1560(self):
        out_time = time_diff_calculator(4.1, time(7, 37, 0))
        self.assertEquals(out_time, time(11, 42, 0))
    
    def test1561(self):
        out_time = time_diff_calculator(4.8, time(12, 14, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1562(self):
        out_time = time_diff_calculator(4.7, time(7, 54, 0))
        self.assertEquals(out_time, time(12, 36, 0))
    
    def test1563(self):
        out_time = time_diff_calculator(4.4, time(13, 5, 0))
        self.assertEquals(out_time, time(17, 30, 0))
    
    def test1564(self):
        out_time = time_diff_calculator(4.1, time(7, 32, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1565(self):
        out_time = time_diff_calculator(5, time(12, 8, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test1566(self):
        out_time = time_diff_calculator(4.4, time(7, 37, 0))
        self.assertEquals(out_time, time(12, 0, 0))
    
    def test1567(self):
        out_time = time_diff_calculator(4.7, time(12, 31, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test1568(self):
        out_time = time_diff_calculator(3.7, time(7, 50, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1569(self):
        out_time = time_diff_calculator(5.2, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test1570(self):
        out_time = time_diff_calculator(3.8, time(7, 39, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1571(self):
        out_time = time_diff_calculator(5.3, time(12, 2, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test1572(self):
        out_time = time_diff_calculator(3.7, time(7, 47, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1573(self):
        out_time = time_diff_calculator(5.4, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test1574(self):
        out_time = time_diff_calculator(3.7, time(7, 48, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1575(self):
        out_time = time_diff_calculator(4.8, time(12, 2, 0))
        self.assertEquals(out_time, time(16, 48, 0))
    
    def test1576(self):
        out_time = time_diff_calculator(3.7, time(7, 48, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1577(self):
        out_time = time_diff_calculator(5.8, time(12, 2, 0))
        self.assertEquals(out_time, time(17, 48, 0))
    
    def test1578(self):
        out_time = time_diff_calculator(3.7, time(7, 50, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1579(self):
        out_time = time_diff_calculator(4.7, time(12, 50, 0))
        self.assertEquals(out_time, time(17, 30, 0))
    
    def test1580(self):
        out_time = time_diff_calculator(3.7, time(7, 49, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1581(self):
        out_time = time_diff_calculator(5.4, time(12, 2, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test1582(self):
        out_time = time_diff_calculator(3.6, time(7, 54, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1583(self):
        out_time = time_diff_calculator(5.1, time(12, 2, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test1584(self):
        out_time = time_diff_calculator(3.8, time(7, 42, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1585(self):
        out_time = time_diff_calculator(5.4, time(12, 2, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test1586(self):
        out_time = time_diff_calculator(3.7, time(7, 47, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1587(self):
        out_time = time_diff_calculator(6.4, time(12, 2, 0))
        self.assertEquals(out_time, time(18, 24, 0))
    
    def test1588(self):
        out_time = time_diff_calculator(3.9, time(7, 37, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1589(self):
        out_time = time_diff_calculator(5.8, time(12, 3, 0))
        self.assertEquals(out_time, time(17, 54, 0))
    
    def test1590(self):
        out_time = time_diff_calculator(4.1, time(7, 44, 0))
        self.assertEquals(out_time, time(11, 48, 0))
    
    def test1591(self):
        out_time = time_diff_calculator(5.1, time(12, 19, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test1592(self):
        out_time = time_diff_calculator(3.8, time(7, 43, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1593(self):
        out_time = time_diff_calculator(5.3, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test1594(self):
        out_time = time_diff_calculator(3.7, time(7, 48, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1595(self):
        out_time = time_diff_calculator(5.4, time(12, 2, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test1596(self):
        out_time = time_diff_calculator(3.7, time(7, 47, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1597(self):
        out_time = time_diff_calculator(5.7, time(12, 2, 0))
        self.assertEquals(out_time, time(17, 42, 0))
    
    def test1598(self):
        out_time = time_diff_calculator(3.8, time(7, 40, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1599(self):
        out_time = time_diff_calculator(5, time(12, 3, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test1600(self):
        out_time = time_diff_calculator(3.8, time(7, 44, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1601(self):
        out_time = time_diff_calculator(5.2, time(12, 2, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test1602(self):
        out_time = time_diff_calculator(3.6, time(7, 55, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1603(self):
        out_time = time_diff_calculator(5, time(12, 2, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1604(self):
        out_time = time_diff_calculator(3.9, time(7, 35, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1605(self):
        out_time = time_diff_calculator(5.3, time(12, 2, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test1606(self):
        out_time = time_diff_calculator(4, time(7, 35, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1607(self):
        out_time = time_diff_calculator(5.3, time(12, 8, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test1608(self):
        out_time = time_diff_calculator(3.8, time(7, 43, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1609(self):
        out_time = time_diff_calculator(5.2, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test1610(self):
        out_time = time_diff_calculator(3.6, time(7, 51, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1611(self):
        out_time = time_diff_calculator(5.1, time(12, 2, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test1612(self):
        out_time = time_diff_calculator(3.8, time(7, 44, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1613(self):
        out_time = time_diff_calculator(5.3, time(12, 2, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test1614(self):
        out_time = time_diff_calculator(3.9, time(7, 38, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1615(self):
        out_time = time_diff_calculator(5.1, time(12, 2, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test1616(self):
        out_time = time_diff_calculator(3.6, time(7, 53, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1617(self):
        out_time = time_diff_calculator(5.4, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test1618(self):
        out_time = time_diff_calculator(4, time(7, 37, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1619(self):
        out_time = time_diff_calculator(5.1, time(12, 8, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test1620(self):
        out_time = time_diff_calculator(3.7, time(7, 45, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1621(self):
        out_time = time_diff_calculator(5, time(12, 2, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1622(self):
        out_time = time_diff_calculator(3.5, time(7, 46, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test1623(self):
        out_time = time_diff_calculator(5.1, time(11, 51, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1624(self):
        out_time = time_diff_calculator(3.6, time(7, 54, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1625(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1626(self):
        out_time = time_diff_calculator(3.8, time(7, 40, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1627(self):
        out_time = time_diff_calculator(3.5, time(12, 1, 0))
        self.assertEquals(out_time, time(15, 30, 0))
    
    def test1628(self):
        out_time = time_diff_calculator(3.7, time(7, 48, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1629(self):
        out_time = time_diff_calculator(4.5, time(12, 3, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test1630(self):
        out_time = time_diff_calculator(3.7, time(7, 45, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1631(self):
        out_time = time_diff_calculator(5.1, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test1632(self):
        out_time = time_diff_calculator(3.8, time(7, 40, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1633(self):
        out_time = time_diff_calculator(4.6, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test1634(self):
        out_time = time_diff_calculator(3.7, time(7, 50, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1635(self):
        out_time = time_diff_calculator(5.1, time(12, 2, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test1636(self):
        out_time = time_diff_calculator(3.7, time(7, 45, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1637(self):
        out_time = time_diff_calculator(5.1, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test1638(self):
        out_time = time_diff_calculator(3.6, time(7, 51, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1639(self):
        out_time = time_diff_calculator(4.8, time(12, 2, 0))
        self.assertEquals(out_time, time(16, 48, 0))
    
    def test1640(self):
        out_time = time_diff_calculator(3.6, time(7, 53, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1641(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1642(self):
        out_time = time_diff_calculator(3.9, time(7, 38, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1643(self):
        out_time = time_diff_calculator(4.6, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test1644(self):
        out_time = time_diff_calculator(3.7, time(7, 49, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1645(self):
        out_time = time_diff_calculator(5.3, time(11, 59, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test1646(self):
        out_time = time_diff_calculator(3.8, time(7, 47, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1647(self):
        out_time = time_diff_calculator(5, time(12, 2, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1648(self):
        out_time = time_diff_calculator(3.7, time(7, 49, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1649(self):
        out_time = time_diff_calculator(5.2, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test1650(self):
        out_time = time_diff_calculator(3.7, time(7, 49, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1651(self):
        out_time = time_diff_calculator(5.1, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test1652(self):
        out_time = time_diff_calculator(4.3, time(7, 50, 0))
        self.assertEquals(out_time, time(12, 6, 0))
    
    def test1653(self):
        out_time = time_diff_calculator(4.2, time(12, 36, 0))
        self.assertEquals(out_time, time(16, 48, 0))
    
    def test1654(self):
        out_time = time_diff_calculator(4.2, time(7, 16, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1655(self):
        out_time = time_diff_calculator(5.1, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test1656(self):
        out_time = time_diff_calculator(4, time(7, 28, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1657(self):
        out_time = time_diff_calculator(4.7, time(11, 59, 0))
        self.assertEquals(out_time, time(16, 42, 0))
    
    def test1658(self):
        out_time = time_diff_calculator(3.8, time(7, 41, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1659(self):
        out_time = time_diff_calculator(4.9, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 54, 0))
    
    def test1660(self):
        out_time = time_diff_calculator(3.8, time(7, 48, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1661(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1662(self):
        out_time = time_diff_calculator(3.8, time(7, 44, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1663(self):
        out_time = time_diff_calculator(4.6, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test1664(self):
        out_time = time_diff_calculator(3.9, time(7, 24, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test1665(self):
        out_time = time_diff_calculator(5.6, time(11, 49, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test1666(self):
        out_time = time_diff_calculator(3.9, time(7, 36, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1667(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1668(self):
        out_time = time_diff_calculator(3.7, time(7, 46, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1669(self):
        out_time = time_diff_calculator(5.1, time(11, 59, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test1670(self):
        out_time = time_diff_calculator(4.1, time(7, 56, 0))
        self.assertEquals(out_time, time(12, 0, 0))
    
    def test1671(self):
        out_time = time_diff_calculator(4.6, time(12, 30, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test1672(self):
        out_time = time_diff_calculator(3.9, time(7, 38, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1673(self):
        out_time = time_diff_calculator(4.6, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test1674(self):
        out_time = time_diff_calculator(3.9, time(7, 36, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1675(self):
        out_time = time_diff_calculator(5.3, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test1676(self):
        out_time = time_diff_calculator(3.7, time(7, 45, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1677(self):
        out_time = time_diff_calculator(4.6, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test1678(self):
        out_time = time_diff_calculator(3.8, time(7, 44, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1679(self):
        out_time = time_diff_calculator(6.1, time(12, 1, 0))
        self.assertEquals(out_time, time(18, 6, 0))
    
    def test1680(self):
        out_time = time_diff_calculator(3.9, time(7, 37, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1681(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1682(self):
        out_time = time_diff_calculator(3.8, time(7, 40, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1683(self):
        out_time = time_diff_calculator(4.6, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test1684(self):
        out_time = time_diff_calculator(3.9, time(7, 38, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1685(self):
        out_time = time_diff_calculator(5.2, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test1686(self):
        out_time = time_diff_calculator(3.8, time(7, 41, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1687(self):
        out_time = time_diff_calculator(5.2, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test1688(self):
        out_time = time_diff_calculator(4.2, time(7, 48, 0))
        self.assertEquals(out_time, time(12, 0, 0))
    
    def test1689(self):
        out_time = time_diff_calculator(5, time(12, 31, 0))
        self.assertEquals(out_time, time(17, 30, 0))
    
    def test1690(self):
        out_time = time_diff_calculator(4.7, time(9, 15, 0))
        self.assertEquals(out_time, time(14, 0, 0))
    
    def test1691(self):
        out_time = time_diff_calculator(3.9, time(7, 34, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1692(self):
        out_time = time_diff_calculator(7.4, time(12, 2, 0))
        self.assertEquals(out_time, time(19, 24, 0))
    
    def test1693(self):
        out_time = time_diff_calculator(3.7, time(7, 45, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1694(self):
        out_time = time_diff_calculator(4.6, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test1695(self):
        out_time = time_diff_calculator(3.7, time(7, 48, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1696(self):
        out_time = time_diff_calculator(5.1, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test1697(self):
        out_time = time_diff_calculator(3.7, time(7, 55, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1698(self):
        out_time = time_diff_calculator(5.1, time(12, 8, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test1699(self):
        out_time = time_diff_calculator(3.5, time(8, 4, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1700(self):
        out_time = time_diff_calculator(5.1, time(11, 55, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1701(self):
        out_time = time_diff_calculator(3.9, time(7, 38, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1702(self):
        out_time = time_diff_calculator(5.1, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test1703(self):
        out_time = time_diff_calculator(3.7, time(7, 50, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1704(self):
        out_time = time_diff_calculator(4.4, time(12, 2, 0))
        self.assertEquals(out_time, time(16, 24, 0))
    
    def test1705(self):
        out_time = time_diff_calculator(3.7, time(7, 54, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1706(self):
        out_time = time_diff_calculator(6, time(12, 8, 0))
        self.assertEquals(out_time, time(18, 6, 0))
    
    def test1707(self):
        out_time = time_diff_calculator(4.3, time(7, 21, 0))
        self.assertEquals(out_time, time(11, 42, 0))
    
    def test1708(self):
        out_time = time_diff_calculator(5.2, time(12, 11, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test1709(self):
        out_time = time_diff_calculator(3.7, time(7, 47, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1710(self):
        out_time = time_diff_calculator(5.1, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test1711(self):
        out_time = time_diff_calculator(3.2, time(8, 27, 0))
        self.assertEquals(out_time, time(11, 42, 0))
    
    def test1712(self):
        out_time = time_diff_calculator(4.2, time(7, 30, 0))
        self.assertEquals(out_time, time(11, 42, 0))
    
    def test1713(self):
        out_time = time_diff_calculator(4.7, time(12, 13, 0))
        self.assertEquals(out_time, time(16, 54, 0))
    
    def test1714(self):
        out_time = time_diff_calculator(3.9, time(7, 43, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1715(self):
        out_time = time_diff_calculator(4.5, time(12, 7, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test1716(self):
        out_time = time_diff_calculator(3.9, time(7, 44, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1717(self):
        out_time = time_diff_calculator(5.4, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test1718(self):
        out_time = time_diff_calculator(4, time(7, 28, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1719(self):
        out_time = time_diff_calculator(4.9, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 54, 0))
    
    def test1720(self):
        out_time = time_diff_calculator(3.7, time(7, 48, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1721(self):
        out_time = time_diff_calculator(5.2, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test1722(self):
        out_time = time_diff_calculator(4.1, time(7, 25, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1723(self):
        out_time = time_diff_calculator(4.7, time(12, 2, 0))
        self.assertEquals(out_time, time(16, 42, 0))
    
    def test1724(self):
        out_time = time_diff_calculator(3.6, time(7, 51, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1725(self):
        out_time = time_diff_calculator(5.1, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test1726(self):
        out_time = time_diff_calculator(3.9, time(7, 36, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1727(self):
        out_time = time_diff_calculator(5.3, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test1728(self):
        out_time = time_diff_calculator(3.6, time(7, 51, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1729(self):
        out_time = time_diff_calculator(4.8, time(12, 2, 0))
        self.assertEquals(out_time, time(16, 48, 0))
    
    def test1730(self):
        out_time = time_diff_calculator(3.9, time(7, 43, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1731(self):
        out_time = time_diff_calculator(5, time(12, 8, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test1732(self):
        out_time = time_diff_calculator(3.8, time(7, 43, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1733(self):
        out_time = time_diff_calculator(4.6, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test1734(self):
        out_time = time_diff_calculator(3.9, time(7, 37, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1735(self):
        out_time = time_diff_calculator(5.3, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test1736(self):
        out_time = time_diff_calculator(3.5, time(7, 30, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test1737(self):
        out_time = time_diff_calculator(5.4, time(11, 59, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test1738(self):
        out_time = time_diff_calculator(3.8, time(7, 48, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1739(self):
        out_time = time_diff_calculator(5.1, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test1740(self):
        out_time = time_diff_calculator(3.8, time(11, 24, 0))
        self.assertEquals(out_time, time(15, 12, 0))
    
    def test1741(self):
        out_time = time_diff_calculator(3.9, time(7, 38, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1742(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1743(self):
        out_time = time_diff_calculator(4, time(7, 29, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1744(self):
        out_time = time_diff_calculator(4.5, time(12, 2, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test1745(self):
        out_time = time_diff_calculator(3.8, time(7, 42, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1746(self):
        out_time = time_diff_calculator(5.6, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 36, 0))
    
    def test1747(self):
        out_time = time_diff_calculator(3.9, time(7, 33, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1748(self):
        out_time = time_diff_calculator(5.3, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test1749(self):
        out_time = time_diff_calculator(3.8, time(7, 43, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1750(self):
        out_time = time_diff_calculator(4.7, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 42, 0))
    
    def test1751(self):
        out_time = time_diff_calculator(3, time(10, 41, 0))
        self.assertEquals(out_time, time(13, 42, 0))
    
    def test1752(self):
        out_time = time_diff_calculator(3.6, time(7, 56, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1753(self):
        out_time = time_diff_calculator(5.1, time(12, 2, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test1754(self):
        out_time = time_diff_calculator(3.7, time(7, 48, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1755(self):
        out_time = time_diff_calculator(4.5, time(12, 2, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test1756(self):
        out_time = time_diff_calculator(3.6, time(7, 51, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1757(self):
        out_time = time_diff_calculator(5.2, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test1758(self):
        out_time = time_diff_calculator(3.8, time(7, 41, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1759(self):
        out_time = time_diff_calculator(5.1, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test1760(self):
        out_time = time_diff_calculator(3.7, time(7, 48, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1761(self):
        out_time = time_diff_calculator(5.1, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test1762(self):
        out_time = time_diff_calculator(2.7, time(8, 46, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1763(self):
        out_time = time_diff_calculator(4.6, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test1764(self):
        out_time = time_diff_calculator(3.9, time(7, 38, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1765(self):
        out_time = time_diff_calculator(5.4, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test1766(self):
        out_time = time_diff_calculator(4, time(7, 28, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1767(self):
        out_time = time_diff_calculator(5.3, time(12, 2, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test1768(self):
        out_time = time_diff_calculator(4.1, time(7, 27, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1769(self):
        out_time = time_diff_calculator(5.2, time(12, 7, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test1770(self):
        out_time = time_diff_calculator(3.2, time(9, 5, 0))
        self.assertEquals(out_time, time(12, 18, 0))
    
    def test1771(self):
        out_time = time_diff_calculator(4.1, time(7, 22, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1772(self):
        out_time = time_diff_calculator(5.4, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test1773(self):
        out_time = time_diff_calculator(3.5, time(8, 41, 0))
        self.assertEquals(out_time, time(12, 12, 0))
    
    def test1774(self):
        out_time = time_diff_calculator(4, time(7, 32, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1775(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1776(self):
        out_time = time_diff_calculator(4, time(7, 32, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1777(self):
        out_time = time_diff_calculator(4.5, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test1778(self):
        out_time = time_diff_calculator(3.9, time(7, 46, 0))
        self.assertEquals(out_time, time(11, 42, 0))
    
    def test1779(self):
        out_time = time_diff_calculator(5.2, time(12, 13, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test1780(self):
        out_time = time_diff_calculator(4.1, time(7, 24, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1781(self):
        out_time = time_diff_calculator(5.2, time(12, 2, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test1782(self):
        out_time = time_diff_calculator(4.3, time(7, 19, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1783(self):
        out_time = time_diff_calculator(5.7, time(12, 36, 0))
        self.assertEquals(out_time, time(18, 18, 0))
    
    def test1784(self):
        out_time = time_diff_calculator(3.9, time(8, 56, 0))
        self.assertEquals(out_time, time(12, 48, 0))
    
    def test1785(self):
        out_time = time_diff_calculator(4.7, time(13, 19, 0))
        self.assertEquals(out_time, time(18, 0, 0))
    
    def test1786(self):
        out_time = time_diff_calculator(6.1, time(11, 49, 0))
        self.assertEquals(out_time, time(17, 54, 0))
    
    def test1787(self):
        out_time = time_diff_calculator(4, time(7, 32, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1788(self):
        out_time = time_diff_calculator(6.2, time(12, 1, 0))
        self.assertEquals(out_time, time(18, 12, 0))
    
    def test1789(self):
        out_time = time_diff_calculator(3.7, time(7, 48, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1790(self):
        out_time = time_diff_calculator(4.7, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 42, 0))
    
    def test1791(self):
        out_time = time_diff_calculator(3.8, time(7, 43, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1792(self):
        out_time = time_diff_calculator(5.7, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 42, 0))
    
    def test1793(self):
        out_time = time_diff_calculator(3.9, time(7, 44, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1794(self):
        out_time = time_diff_calculator(5.2, time(12, 8, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test1795(self):
        out_time = time_diff_calculator(3.9, time(7, 42, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1796(self):
        out_time = time_diff_calculator(5.6, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 36, 0))
    
    def test1797(self):
        out_time = time_diff_calculator(6.6, time(7, 36, 0))
        self.assertEquals(out_time, time(14, 12, 0))
    
    def test1798(self):
        out_time = time_diff_calculator(2.1, time(14, 31, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test1799(self):
        out_time = time_diff_calculator(4.4, time(7, 19, 0))
        self.assertEquals(out_time, time(11, 42, 0))
    
    def test1800(self):
        out_time = time_diff_calculator(4.9, time(12, 13, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test1801(self):
        out_time = time_diff_calculator(4.2, time(7, 27, 0))
        self.assertEquals(out_time, time(11, 42, 0))
    
    def test1802(self):
        out_time = time_diff_calculator(4.9, time(12, 13, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test1803(self):
        out_time = time_diff_calculator(4.2, time(7, 18, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1804(self):
        out_time = time_diff_calculator(7.7, time(12, 1, 0))
        self.assertEquals(out_time, time(19, 42, 0))
    
    def test1805(self):
        out_time = time_diff_calculator(4.6, time(7, 25, 0))
        self.assertEquals(out_time, time(12, 0, 0))
    
    def test1806(self):
        out_time = time_diff_calculator(4.7, time(12, 32, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test1807(self):
        out_time = time_diff_calculator(4.1, time(7, 24, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1808(self):
        out_time = time_diff_calculator(5.4, time(12, 2, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test1809(self):
        out_time = time_diff_calculator(4.2, time(7, 20, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1810(self):
        out_time = time_diff_calculator(5.1, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test1811(self):
        out_time = time_diff_calculator(4.2, time(7, 16, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1812(self):
        out_time = time_diff_calculator(4.5, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test1813(self):
        out_time = time_diff_calculator(4.2, time(7, 18, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1814(self):
        out_time = time_diff_calculator(5.3, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test1815(self):
        out_time = time_diff_calculator(4, time(7, 30, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1816(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1817(self):
        out_time = time_diff_calculator(4, time(7, 31, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1818(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1819(self):
        out_time = time_diff_calculator(3.9, time(7, 33, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1820(self):
        out_time = time_diff_calculator(5.2, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test1821(self):
        out_time = time_diff_calculator(4, time(7, 32, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1822(self):
        out_time = time_diff_calculator(4.5, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test1823(self):
        out_time = time_diff_calculator(4, time(7, 29, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1824(self):
        out_time = time_diff_calculator(5, time(12, 2, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1825(self):
        out_time = time_diff_calculator(3.9, time(7, 34, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1826(self):
        out_time = time_diff_calculator(5.3, time(12, 2, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test1827(self):
        out_time = time_diff_calculator(4, time(7, 30, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1828(self):
        out_time = time_diff_calculator(5.2, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test1829(self):
        out_time = time_diff_calculator(4.5, time(9, 24, 0))
        self.assertEquals(out_time, time(13, 54, 0))
    
    def test1830(self):
        out_time = time_diff_calculator(4.1, time(7, 26, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1831(self):
        out_time = time_diff_calculator(6, time(12, 1, 0))
        self.assertEquals(out_time, time(18, 0, 0))
    
    def test1832(self):
        out_time = time_diff_calculator(4.2, time(7, 20, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1833(self):
        out_time = time_diff_calculator(4.5, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test1834(self):
        out_time = time_diff_calculator(4.2, time(7, 25, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1835(self):
        out_time = time_diff_calculator(5.5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 30, 0))
    
    def test1836(self):
        out_time = time_diff_calculator(4.2, time(7, 13, 0))
        self.assertEquals(out_time, time(11, 24, 0))
    
    def test1837(self):
        out_time = time_diff_calculator(4.8, time(12, 33, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test1838(self):
        out_time = time_diff_calculator(1.7, time(7, 36, 0))
        self.assertEquals(out_time, time(9, 18, 0))
    
    def test1839(self):
        out_time = time_diff_calculator(1.6, time(10, 8, 0))
        self.assertEquals(out_time, time(11, 42, 0))
    
    def test1840(self):
        out_time = time_diff_calculator(5.9, time(12, 14, 0))
        self.assertEquals(out_time, time(18, 6, 0))
    
    def test1841(self):
        out_time = time_diff_calculator(4.5, time(7, 48, 0))
        self.assertEquals(out_time, time(12, 18, 0))
    
    def test1842(self):
        out_time = time_diff_calculator(3.9, time(7, 33, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1843(self):
        out_time = time_diff_calculator(5.1, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test1844(self):
        out_time = time_diff_calculator(3.9, time(7, 36, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1845(self):
        out_time = time_diff_calculator(4.3, time(12, 2, 0))
        self.assertEquals(out_time, time(16, 18, 0))
    
    def test1846(self):
        out_time = time_diff_calculator(3.6, time(7, 58, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1847(self):
        out_time = time_diff_calculator(4.6, time(12, 6, 0))
        self.assertEquals(out_time, time(16, 42, 0))
    
    def test1848(self):
        out_time = time_diff_calculator(5.1, time(7, 19, 0))
        self.assertEquals(out_time, time(12, 24, 0))
    
    def test1849(self):
        out_time = time_diff_calculator(5.8, time(12, 54, 0))
        self.assertEquals(out_time, time(18, 42, 0))
    
    def test1850(self):
        out_time = time_diff_calculator(4, time(7, 29, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1851(self):
        out_time = time_diff_calculator(5.6, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 36, 0))
    
    def test1852(self):
        out_time = time_diff_calculator(3.8, time(7, 40, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1853(self):
        out_time = time_diff_calculator(5.4, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test1854(self):
        out_time = time_diff_calculator(3, time(8, 7, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test1855(self):
        out_time = time_diff_calculator(4, time(7, 31, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1856(self):
        out_time = time_diff_calculator(5.3, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test1857(self):
        out_time = time_diff_calculator(5.2, time(7, 25, 0))
        self.assertEquals(out_time, time(12, 36, 0))
    
    def test1858(self):
        out_time = time_diff_calculator(4.1, time(13, 0, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test1859(self):
        out_time = time_diff_calculator(4.3, time(7, 19, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1860(self):
        out_time = time_diff_calculator(6.7, time(12, 6, 0))
        self.assertEquals(out_time, time(18, 48, 0))
    
    def test1861(self):
        out_time = time_diff_calculator(4, time(7, 32, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1862(self):
        out_time = time_diff_calculator(5.5, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 30, 0))
    
    def test1863(self):
        out_time = time_diff_calculator(4.1, time(7, 26, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1864(self):
        out_time = time_diff_calculator(5.1, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test1865(self):
        out_time = time_diff_calculator(4.6, time(9, 6, 0))
        self.assertEquals(out_time, time(13, 42, 0))
    
    def test1866(self):
        out_time = time_diff_calculator(3.7, time(7, 49, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1867(self):
        out_time = time_diff_calculator(5.2, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test1868(self):
        out_time = time_diff_calculator(4.1, time(7, 24, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1869(self):
        out_time = time_diff_calculator(4.1, time(12, 31, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test1870(self):
        out_time = time_diff_calculator(4, time(7, 30, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1871(self):
        out_time = time_diff_calculator(5.9, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 54, 0))
    
    def test1872(self):
        out_time = time_diff_calculator(3.8, time(7, 43, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1873(self):
        out_time = time_diff_calculator(6.8, time(12, 1, 0))
        self.assertEquals(out_time, time(18, 48, 0))
    
    def test1874(self):
        out_time = time_diff_calculator(5.2, time(6, 18, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1875(self):
        out_time = time_diff_calculator(5.4, time(11, 59, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test1876(self):
        out_time = time_diff_calculator(4.9, time(7, 47, 0))
        self.assertEquals(out_time, time(12, 42, 0))
    
    def test1877(self):
        out_time = time_diff_calculator(3.7, time(7, 45, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1878(self):
        out_time = time_diff_calculator(4.3, time(12, 0, 0))
        self.assertEquals(out_time, time(16, 18, 0))
    
    def test1879(self):
        out_time = time_diff_calculator(4.5, time(8, 0, 0))
        self.assertEquals(out_time, time(12, 30, 0))
    
    def test1880(self):
        out_time = time_diff_calculator(3.5, time(13, 30, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1881(self):
        out_time = time_diff_calculator(4.5, time(7, 0, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1882(self):
        out_time = time_diff_calculator(3.5, time(11, 59, 0))
        self.assertEquals(out_time, time(15, 30, 0))
    
    def test1883(self):
        out_time = time_diff_calculator(4, time(7, 32, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1884(self):
        out_time = time_diff_calculator(4, time(11, 59, 0))
        self.assertEquals(out_time, time(16, 0, 0))
    
    def test1885(self):
        out_time = time_diff_calculator(1.9, time(7, 23, 0))
        self.assertEquals(out_time, time(9, 18, 0))
    
    def test1886(self):
        out_time = time_diff_calculator(2.3, time(10, 21, 0))
        self.assertEquals(out_time, time(12, 42, 0))
    
    def test1887(self):
        out_time = time_diff_calculator(4.2, time(7, 20, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1888(self):
        out_time = time_diff_calculator(5.3, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test1889(self):
        out_time = time_diff_calculator(4, time(7, 30, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1890(self):
        out_time = time_diff_calculator(4.7, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 42, 0))
    
    def test1891(self):
        out_time = time_diff_calculator(1.8, time(7, 25, 0))
        self.assertEquals(out_time, time(9, 12, 0))
    
    def test1892(self):
        out_time = time_diff_calculator(4, time(7, 29, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1893(self):
        out_time = time_diff_calculator(6.4, time(12, 0, 0))
        self.assertEquals(out_time, time(18, 24, 0))
    
    def test1894(self):
        out_time = time_diff_calculator(4, time(8, 45, 0))
        self.assertEquals(out_time, time(12, 48, 0))
    
    def test1895(self):
        out_time = time_diff_calculator(4, time(7, 30, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1896(self):
        out_time = time_diff_calculator(5.3, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test1897(self):
        out_time = time_diff_calculator(5.9, time(7, 23, 0))
        self.assertEquals(out_time, time(13, 18, 0))
    
    def test1898(self):
        out_time = time_diff_calculator(3, time(13, 44, 0))
        self.assertEquals(out_time, time(16, 42, 0))
    
    def test1899(self):
        out_time = time_diff_calculator(3.4, time(7, 34, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test1900(self):
        out_time = time_diff_calculator(5, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1901(self):
        out_time = time_diff_calculator(3, time(7, 59, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test1902(self):
        out_time = time_diff_calculator(5, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1903(self):
        out_time = time_diff_calculator(3, time(7, 59, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test1904(self):
        out_time = time_diff_calculator(5.1, time(11, 59, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test1905(self):
        out_time = time_diff_calculator(3, time(7, 58, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test1906(self):
        out_time = time_diff_calculator(5, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1907(self):
        out_time = time_diff_calculator(3.5, time(7, 59, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1908(self):
        out_time = time_diff_calculator(4.5, time(11, 59, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test1909(self):
        out_time = time_diff_calculator(3, time(7, 58, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test1910(self):
        out_time = time_diff_calculator(5, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1911(self):
        out_time = time_diff_calculator(3.5, time(7, 59, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1912(self):
        out_time = time_diff_calculator(4.5, time(12, 0, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test1913(self):
        out_time = time_diff_calculator(2.9, time(7, 34, 0))
        self.assertEquals(out_time, time(10, 30, 0))
    
    def test1914(self):
        out_time = time_diff_calculator(5.6, time(11, 24, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1915(self):
        out_time = time_diff_calculator(3, time(7, 58, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test1916(self):
        out_time = time_diff_calculator(5, time(11, 59, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1917(self):
        out_time = time_diff_calculator(3.5, time(7, 37, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test1918(self):
        out_time = time_diff_calculator(4.5, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test1919(self):
        out_time = time_diff_calculator(3, time(8, 0, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test1920(self):
        out_time = time_diff_calculator(5.1, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test1921(self):
        out_time = time_diff_calculator(4.5, time(7, 36, 0))
        self.assertEquals(out_time, time(12, 6, 0))
    
    def test1922(self):
        out_time = time_diff_calculator(3.6, time(13, 1, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test1923(self):
        out_time = time_diff_calculator(3, time(8, 0, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test1924(self):
        out_time = time_diff_calculator(5.2, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test1925(self):
        out_time = time_diff_calculator(3, time(7, 58, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test1926(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1927(self):
        out_time = time_diff_calculator(3.6, time(7, 41, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test1928(self):
        out_time = time_diff_calculator(4.6, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test1929(self):
        out_time = time_diff_calculator(3, time(7, 59, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test1930(self):
        out_time = time_diff_calculator(5.7, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 42, 0))
    
    def test1931(self):
        out_time = time_diff_calculator(3.9, time(7, 38, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1932(self):
        out_time = time_diff_calculator(4.7, time(12, 0, 0))
        self.assertEquals(out_time, time(16, 42, 0))
    
    def test1933(self):
        out_time = time_diff_calculator(3.8, time(7, 39, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1934(self):
        out_time = time_diff_calculator(5.7, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 42, 0))
    
    def test1935(self):
        out_time = time_diff_calculator(2.7, time(12, 22, 0))
        self.assertEquals(out_time, time(15, 6, 0))
    
    def test1936(self):
        out_time = time_diff_calculator(4.1, time(7, 27, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1937(self):
        out_time = time_diff_calculator(5.2, time(12, 8, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test1938(self):
        out_time = time_diff_calculator(14.8, time(7, 0, 0))
        self.assertEquals(out_time, time(21, 48, 0))
    
    def test1939(self):
        out_time = time_diff_calculator(3.8, time(7, 44, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1940(self):
        out_time = time_diff_calculator(5.4, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test1941(self):
        out_time = time_diff_calculator(4, time(7, 37, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1942(self):
        out_time = time_diff_calculator(4.6, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test1943(self):
        out_time = time_diff_calculator(3, time(8, 0, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test1944(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1945(self):
        out_time = time_diff_calculator(3.5, time(7, 58, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1946(self):
        out_time = time_diff_calculator(4.5, time(12, 0, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test1947(self):
        out_time = time_diff_calculator(3, time(7, 58, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test1948(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1949(self):
        out_time = time_diff_calculator(3.4, time(7, 57, 0))
        self.assertEquals(out_time, time(11, 24, 0))
    
    def test1950(self):
        out_time = time_diff_calculator(4.5, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test1951(self):
        out_time = time_diff_calculator(3.6, time(7, 30, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test1952(self):
        out_time = time_diff_calculator(5, time(12, 8, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test1953(self):
        out_time = time_diff_calculator(3, time(7, 58, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test1954(self):
        out_time = time_diff_calculator(5, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test1955(self):
        out_time = time_diff_calculator(3.5, time(7, 37, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test1956(self):
        out_time = time_diff_calculator(4.6, time(12, 0, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test1957(self):
        out_time = time_diff_calculator(3.7, time(7, 50, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1958(self):
        out_time = time_diff_calculator(4.8, time(12, 2, 0))
        self.assertEquals(out_time, time(16, 48, 0))
    
    def test1959(self):
        out_time = time_diff_calculator(4.8, time(7, 42, 0))
        self.assertEquals(out_time, time(12, 30, 0))
    
    def test1960(self):
        out_time = time_diff_calculator(4.5, time(13, 0, 0))
        self.assertEquals(out_time, time(17, 30, 0))
    
    def test1961(self):
        out_time = time_diff_calculator(4, time(7, 31, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1962(self):
        out_time = time_diff_calculator(5.6, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 36, 0))
    
    def test1963(self):
        out_time = time_diff_calculator(4, time(7, 35, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1964(self):
        out_time = time_diff_calculator(5.9, time(12, 6, 0))
        self.assertEquals(out_time, time(18, 0, 0))
    
    def test1965(self):
        out_time = time_diff_calculator(3.9, time(7, 34, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1966(self):
        out_time = time_diff_calculator(6.2, time(12, 1, 0))
        self.assertEquals(out_time, time(18, 12, 0))
    
    def test1967(self):
        out_time = time_diff_calculator(3.8, time(7, 44, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1968(self):
        out_time = time_diff_calculator(6.1, time(12, 1, 0))
        self.assertEquals(out_time, time(18, 6, 0))
    
    def test1969(self):
        out_time = time_diff_calculator(4, time(7, 30, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1970(self):
        out_time = time_diff_calculator(5.1, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test1971(self):
        out_time = time_diff_calculator(4, time(7, 32, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1972(self):
        out_time = time_diff_calculator(6, time(12, 1, 0))
        self.assertEquals(out_time, time(18, 0, 0))
    
    def test1973(self):
        out_time = time_diff_calculator(3.8, time(7, 43, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1974(self):
        out_time = time_diff_calculator(6.1, time(12, 1, 0))
        self.assertEquals(out_time, time(18, 6, 0))
    
    def test1975(self):
        out_time = time_diff_calculator(3.8, time(7, 44, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1976(self):
        out_time = time_diff_calculator(5.5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 30, 0))
    
    def test1977(self):
        out_time = time_diff_calculator(6.5, time(7, 48, 0))
        self.assertEquals(out_time, time(14, 18, 0))
    
    def test1978(self):
        out_time = time_diff_calculator(3.7, time(7, 47, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1979(self):
        out_time = time_diff_calculator(5.4, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test1980(self):
        out_time = time_diff_calculator(4.2, time(7, 22, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test1981(self):
        out_time = time_diff_calculator(5.4, time(12, 7, 0))
        self.assertEquals(out_time, time(17, 30, 0))
    
    def test1982(self):
        out_time = time_diff_calculator(4.1, time(7, 24, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1983(self):
        out_time = time_diff_calculator(5.9, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 54, 0))
    
    def test1984(self):
        out_time = time_diff_calculator(4.4, time(7, 41, 0))
        self.assertEquals(out_time, time(12, 6, 0))
    
    def test1985(self):
        out_time = time_diff_calculator(3.6, time(7, 53, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1986(self):
        out_time = time_diff_calculator(4.7, time(12, 30, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test1987(self):
        out_time = time_diff_calculator(3.7, time(8, 41, 0))
        self.assertEquals(out_time, time(12, 24, 0))
    
    def test1988(self):
        out_time = time_diff_calculator(4, time(7, 31, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1989(self):
        out_time = time_diff_calculator(5.6, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 36, 0))
    
    def test1990(self):
        out_time = time_diff_calculator(3.7, time(7, 47, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1991(self):
        out_time = time_diff_calculator(5.6, time(12, 33, 0))
        self.assertEquals(out_time, time(18, 12, 0))
    
    def test1992(self):
        out_time = time_diff_calculator(4.5, time(7, 11, 0))
        self.assertEquals(out_time, time(11, 42, 0))
    
    def test1993(self):
        out_time = time_diff_calculator(6, time(12, 14, 0))
        self.assertEquals(out_time, time(18, 12, 0))
    
    def test1994(self):
        out_time = time_diff_calculator(4, time(7, 28, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1995(self):
        out_time = time_diff_calculator(5.7, time(12, 2, 0))
        self.assertEquals(out_time, time(17, 42, 0))
    
    def test1996(self):
        out_time = time_diff_calculator(4.2, time(7, 18, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1997(self):
        out_time = time_diff_calculator(6.5, time(12, 2, 0))
        self.assertEquals(out_time, time(18, 30, 0))
    
    def test1998(self):
        out_time = time_diff_calculator(4.5, time(6, 59, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test1999(self):
        out_time = time_diff_calculator(7.2, time(12, 2, 0))
        self.assertEquals(out_time, time(19, 12, 0))
    
    def test2000(self):
        out_time = time_diff_calculator(4, time(6, 36, 0))
        self.assertEquals(out_time, time(10, 36, 0))
    
    def test2001(self):
        out_time = time_diff_calculator(7.2, time(10, 51, 0))
        self.assertEquals(out_time, time(18, 6, 0))
    
    def test2002(self):
        out_time = time_diff_calculator(8.1, time(8, 30, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test2003(self):
        out_time = time_diff_calculator(4.1, time(7, 22, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test2004(self):
        out_time = time_diff_calculator(5.4, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test2005(self):
        out_time = time_diff_calculator(4, time(7, 27, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test2006(self):
        out_time = time_diff_calculator(6.6, time(12, 0, 0))
        self.assertEquals(out_time, time(18, 36, 0))
    
    def test2007(self):
        out_time = time_diff_calculator(3.7, time(7, 36, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test2008(self):
        out_time = time_diff_calculator(7.3, time(11, 47, 0))
        self.assertEquals(out_time, time(19, 6, 0))
    
    def test2009(self):
        out_time = time_diff_calculator(4.1, time(7, 23, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test2010(self):
        out_time = time_diff_calculator(7.8, time(12, 1, 0))
        self.assertEquals(out_time, time(19, 48, 0))
    
    def test2011(self):
        out_time = time_diff_calculator(7, time(7, 54, 0))
        self.assertEquals(out_time, time(14, 54, 0))
    
    def test2012(self):
        out_time = time_diff_calculator(5, time(9, 45, 0))
        self.assertEquals(out_time, time(14, 48, 0))
    
    def test2013(self):
        out_time = time_diff_calculator(4, time(7, 32, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test2014(self):
        out_time = time_diff_calculator(5.2, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test2015(self):
        out_time = time_diff_calculator(4, time(7, 31, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test2016(self):
        out_time = time_diff_calculator(5.2, time(11, 58, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test2017(self):
        out_time = time_diff_calculator(1.9, time(7, 39, 0))
        self.assertEquals(out_time, time(9, 36, 0))
    
    def test2018(self):
        out_time = time_diff_calculator(6, time(11, 8, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test2019(self):
        out_time = time_diff_calculator(3.9, time(7, 33, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test2020(self):
        out_time = time_diff_calculator(6, time(12, 30, 0))
        self.assertEquals(out_time, time(18, 30, 0))
    
    def test2021(self):
        out_time = time_diff_calculator(3.8, time(7, 43, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test2022(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2023(self):
        out_time = time_diff_calculator(3.8, time(14, 15, 0))
        self.assertEquals(out_time, time(18, 6, 0))
    
    def test2024(self):
        out_time = time_diff_calculator(4, time(7, 31, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test2025(self):
        out_time = time_diff_calculator(5.3, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test2026(self):
        out_time = time_diff_calculator(3.8, time(7, 41, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test2027(self):
        out_time = time_diff_calculator(5.3, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test2028(self):
        out_time = time_diff_calculator(3.7, time(7, 48, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test2029(self):
        out_time = time_diff_calculator(5.6, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 36, 0))
    
    def test2030(self):
        out_time = time_diff_calculator(3.9, time(7, 38, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test2031(self):
        out_time = time_diff_calculator(5.2, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test2032(self):
        out_time = time_diff_calculator(4.5, time(8, 33, 0))
        self.assertEquals(out_time, time(13, 6, 0))
    
    def test2033(self):
        out_time = time_diff_calculator(3.3, time(7, 42, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2034(self):
        out_time = time_diff_calculator(5.4, time(12, 3, 0))
        self.assertEquals(out_time, time(17, 30, 0))
    
    def test2035(self):
        out_time = time_diff_calculator(4.1, time(7, 20, 0))
        self.assertEquals(out_time, time(11, 24, 0))
    
    def test2036(self):
        out_time = time_diff_calculator(3.9, time(7, 34, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test2037(self):
        out_time = time_diff_calculator(5.1, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test2038(self):
        out_time = time_diff_calculator(4, time(7, 31, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test2039(self):
        out_time = time_diff_calculator(5.5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 30, 0))
    
    def test2040(self):
        out_time = time_diff_calculator(4, time(7, 29, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test2041(self):
        out_time = time_diff_calculator(5.4, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test2042(self):
        out_time = time_diff_calculator(3.6, time(8, 55, 0))
        self.assertEquals(out_time, time(12, 30, 0))
    
    def test2043(self):
        out_time = time_diff_calculator(3.9, time(7, 49, 0))
        self.assertEquals(out_time, time(11, 42, 0))
    
    def test2044(self):
        out_time = time_diff_calculator(5, time(12, 12, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test2045(self):
        out_time = time_diff_calculator(3.3, time(7, 41, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2046(self):
        out_time = time_diff_calculator(5.5, time(11, 32, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2047(self):
        out_time = time_diff_calculator(3.9, time(7, 36, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test2048(self):
        out_time = time_diff_calculator(5.2, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test2049(self):
        out_time = time_diff_calculator(4, time(7, 27, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test2050(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2051(self):
        out_time = time_diff_calculator(3.5, time(8, 0, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test2052(self):
        out_time = time_diff_calculator(4.5, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test2053(self):
        out_time = time_diff_calculator(3.5, time(7, 37, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test2054(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2055(self):
        out_time = time_diff_calculator(3.4, time(7, 34, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2056(self):
        out_time = time_diff_calculator(4.5, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test2057(self):
        out_time = time_diff_calculator(3.1, time(7, 54, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2058(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2059(self):
        out_time = time_diff_calculator(3.2, time(7, 46, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2060(self):
        out_time = time_diff_calculator(5.2, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test2061(self):
        out_time = time_diff_calculator(3.5, time(7, 38, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test2062(self):
        out_time = time_diff_calculator(5, time(12, 7, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test2063(self):
        out_time = time_diff_calculator(3.1, time(7, 53, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2064(self):
        out_time = time_diff_calculator(5.1, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test2065(self):
        out_time = time_diff_calculator(3.6, time(7, 32, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test2066(self):
        out_time = time_diff_calculator(4.6, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test2067(self):
        out_time = time_diff_calculator(3.5, time(7, 31, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2068(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2069(self):
        out_time = time_diff_calculator(3.5, time(7, 36, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test2070(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2071(self):
        out_time = time_diff_calculator(3.4, time(7, 35, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2072(self):
        out_time = time_diff_calculator(5, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2073(self):
        out_time = time_diff_calculator(3.5, time(7, 30, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2074(self):
        out_time = time_diff_calculator(5.1, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test2075(self):
        out_time = time_diff_calculator(3.9, time(7, 30, 0))
        self.assertEquals(out_time, time(11, 24, 0))
    
    def test2076(self):
        out_time = time_diff_calculator(4.2, time(12, 25, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test2077(self):
        out_time = time_diff_calculator(3.3, time(7, 41, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2078(self):
        out_time = time_diff_calculator(5.3, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test2079(self):
        out_time = time_diff_calculator(3, time(7, 59, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2080(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2081(self):
        out_time = time_diff_calculator(3.3, time(7, 59, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test2082(self):
        out_time = time_diff_calculator(4.7, time(12, 18, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2083(self):
        out_time = time_diff_calculator(3.5, time(7, 28, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2084(self):
        out_time = time_diff_calculator(5.1, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test2085(self):
        out_time = time_diff_calculator(3.4, time(7, 36, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2086(self):
        out_time = time_diff_calculator(4.5, time(12, 0, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test2087(self):
        out_time = time_diff_calculator(3, time(7, 59, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2088(self):
        out_time = time_diff_calculator(5, time(11, 59, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2089(self):
        out_time = time_diff_calculator(4.4, time(7, 36, 0))
        self.assertEquals(out_time, time(12, 0, 0))
    
    def test2090(self):
        out_time = time_diff_calculator(3.9, time(13, 0, 0))
        self.assertEquals(out_time, time(16, 54, 0))
    
    def test2091(self):
        out_time = time_diff_calculator(3, time(7, 59, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2092(self):
        out_time = time_diff_calculator(5, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2093(self):
        out_time = time_diff_calculator(3.4, time(7, 33, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2094(self):
        out_time = time_diff_calculator(4.6, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test2095(self):
        out_time = time_diff_calculator(3.2, time(7, 50, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2096(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2097(self):
        out_time = time_diff_calculator(3.1, time(7, 54, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2098(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2099(self):
        out_time = time_diff_calculator(3.4, time(7, 38, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2100(self):
        out_time = time_diff_calculator(5, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2101(self):
        out_time = time_diff_calculator(3.5, time(7, 38, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test2102(self):
        out_time = time_diff_calculator(4.6, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test2103(self):
        out_time = time_diff_calculator(3.2, time(7, 54, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test2104(self):
        out_time = time_diff_calculator(4.9, time(12, 5, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2105(self):
        out_time = time_diff_calculator(3.4, time(7, 36, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2106(self):
        out_time = time_diff_calculator(5, time(12, 2, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2107(self):
        out_time = time_diff_calculator(4.1, time(7, 55, 0))
        self.assertEquals(out_time, time(12, 0, 0))
    
    def test2108(self):
        out_time = time_diff_calculator(4.3, time(13, 1, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test2109(self):
        out_time = time_diff_calculator(3.1, time(7, 53, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2110(self):
        out_time = time_diff_calculator(5, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2111(self):
        out_time = time_diff_calculator(3.4, time(7, 34, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2112(self):
        out_time = time_diff_calculator(4.4, time(11, 59, 0))
        self.assertEquals(out_time, time(16, 24, 0))
    
    def test2113(self):
        out_time = time_diff_calculator(3.1, time(7, 55, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2114(self):
        out_time = time_diff_calculator(5, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2115(self):
        out_time = time_diff_calculator(3, time(8, 0, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2116(self):
        out_time = time_diff_calculator(5, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2117(self):
        out_time = time_diff_calculator(3, time(7, 59, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2118(self):
        out_time = time_diff_calculator(5, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2119(self):
        out_time = time_diff_calculator(3.5, time(7, 34, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test2120(self):
        out_time = time_diff_calculator(4.8, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 48, 0))
    
    def test2121(self):
        out_time = time_diff_calculator(3.3, time(7, 43, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2122(self):
        out_time = time_diff_calculator(4.5, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test2123(self):
        out_time = time_diff_calculator(2.9, time(7, 59, 0))
        self.assertEquals(out_time, time(10, 54, 0))
    
    def test2124(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2125(self):
        out_time = time_diff_calculator(3, time(8, 0, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2126(self):
        out_time = time_diff_calculator(5, time(11, 59, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2127(self):
        out_time = time_diff_calculator(3.2, time(7, 46, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2128(self):
        out_time = time_diff_calculator(4.8, time(12, 2, 0))
        self.assertEquals(out_time, time(16, 48, 0))
    
    def test2129(self):
        out_time = time_diff_calculator(3, time(8, 0, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2130(self):
        out_time = time_diff_calculator(5, time(12, 2, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2131(self):
        out_time = time_diff_calculator(3.6, time(7, 30, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test2132(self):
        out_time = time_diff_calculator(4.4, time(12, 7, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test2133(self):
        out_time = time_diff_calculator(3.5, time(7, 59, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test2134(self):
        out_time = time_diff_calculator(6.5, time(12, 1, 0))
        self.assertEquals(out_time, time(18, 30, 0))
    
    def test2135(self):
        out_time = time_diff_calculator(4.1, time(7, 21, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test2136(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2137(self):
        out_time = time_diff_calculator(4.3, time(7, 13, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test2138(self):
        out_time = time_diff_calculator(5.5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 30, 0))
    
    def test2139(self):
        out_time = time_diff_calculator(4.2, time(7, 20, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test2140(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2141(self):
        out_time = time_diff_calculator(4.2, time(7, 26, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test2142(self):
        out_time = time_diff_calculator(5.5, time(12, 6, 0))
        self.assertEquals(out_time, time(17, 36, 0))
    
    def test2143(self):
        out_time = time_diff_calculator(11.6, time(7, 24, 0))
        self.assertEquals(out_time, time(19, 0, 0))
    
    def test2144(self):
        out_time = time_diff_calculator(3.9, time(7, 34, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test2145(self):
        out_time = time_diff_calculator(6.3, time(12, 1, 0))
        self.assertEquals(out_time, time(18, 18, 0))
    
    def test2146(self):
        out_time = time_diff_calculator(3.6, time(7, 39, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test2147(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2148(self):
        out_time = time_diff_calculator(3.8, time(7, 43, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test2149(self):
        out_time = time_diff_calculator(5.1, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test2150(self):
        out_time = time_diff_calculator(4.2, time(7, 18, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test2151(self):
        out_time = time_diff_calculator(5.3, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test2152(self):
        out_time = time_diff_calculator(3.6, time(7, 26, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2153(self):
        out_time = time_diff_calculator(5.1, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test2154(self):
        out_time = time_diff_calculator(3.4, time(7, 36, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2155(self):
        out_time = time_diff_calculator(5.1, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test2156(self):
        out_time = time_diff_calculator(3.5, time(7, 41, 0))
        self.assertEquals(out_time, time(11, 12, 0))
    
    def test2157(self):
        out_time = time_diff_calculator(4.6, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test2158(self):
        out_time = time_diff_calculator(3.4, time(7, 42, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test2159(self):
        out_time = time_diff_calculator(4.5, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test2160(self):
        out_time = time_diff_calculator(3, time(7, 59, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2161(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2162(self):
        out_time = time_diff_calculator(3, time(8, 0, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2163(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2164(self):
        out_time = time_diff_calculator(3, time(8, 0, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2165(self):
        out_time = time_diff_calculator(5, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2166(self):
        out_time = time_diff_calculator(3.4, time(7, 44, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test2167(self):
        out_time = time_diff_calculator(4.6, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test2168(self):
        out_time = time_diff_calculator(3, time(8, 0, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2169(self):
        out_time = time_diff_calculator(5, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2170(self):
        out_time = time_diff_calculator(3, time(8, 0, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2171(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2172(self):
        out_time = time_diff_calculator(3, time(8, 1, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2173(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2174(self):
        out_time = time_diff_calculator(3, time(7, 55, 0))
        self.assertEquals(out_time, time(10, 54, 0))
    
    def test2175(self):
        out_time = time_diff_calculator(5, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2176(self):
        out_time = time_diff_calculator(3.1, time(8, 0, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test2177(self):
        out_time = time_diff_calculator(4.9, time(12, 6, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2178(self):
        out_time = time_diff_calculator(3, time(8, 0, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2179(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2180(self):
        out_time = time_diff_calculator(3, time(8, 0, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2181(self):
        out_time = time_diff_calculator(5, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2182(self):
        out_time = time_diff_calculator(3.4, time(7, 43, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test2183(self):
        out_time = time_diff_calculator(4.6, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test2184(self):
        out_time = time_diff_calculator(3, time(8, 1, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2185(self):
        out_time = time_diff_calculator(5.1, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test2186(self):
        out_time = time_diff_calculator(3, time(8, 0, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2187(self):
        out_time = time_diff_calculator(4.9, time(12, 6, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2188(self):
        out_time = time_diff_calculator(3.2, time(7, 47, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2189(self):
        out_time = time_diff_calculator(4.8, time(12, 0, 0))
        self.assertEquals(out_time, time(16, 48, 0))
    
    def test2190(self):
        out_time = time_diff_calculator(3, time(8, 0, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2191(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2192(self):
        out_time = time_diff_calculator(3.5, time(7, 35, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test2193(self):
        out_time = time_diff_calculator(4.5, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test2194(self):
        out_time = time_diff_calculator(3, time(8, 0, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2195(self):
        out_time = time_diff_calculator(5, time(12, 2, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2196(self):
        out_time = time_diff_calculator(3.1, time(7, 59, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test2197(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2198(self):
        out_time = time_diff_calculator(3.1, time(8, 0, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test2199(self):
        out_time = time_diff_calculator(4.8, time(12, 12, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2200(self):
        out_time = time_diff_calculator(3, time(7, 59, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2201(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2202(self):
        out_time = time_diff_calculator(3.3, time(8, 1, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test2203(self):
        out_time = time_diff_calculator(5.3, time(11, 50, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test2204(self):
        out_time = time_diff_calculator(3.6, time(7, 59, 0))
        self.assertEquals(out_time, time(11, 36, 0))
    
    def test2205(self):
        out_time = time_diff_calculator(5, time(12, 4, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test2206(self):
        out_time = time_diff_calculator(4, time(7, 30, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test2207(self):
        out_time = time_diff_calculator(5, time(12, 2, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2208(self):
        out_time = time_diff_calculator(3.5, time(7, 34, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test2209(self):
        out_time = time_diff_calculator(4.5, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test2210(self):
        out_time = time_diff_calculator(3.5, time(7, 31, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2211(self):
        out_time = time_diff_calculator(4.5, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test2212(self):
        out_time = time_diff_calculator(3, time(8, 1, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2213(self):
        out_time = time_diff_calculator(5, time(12, 2, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2214(self):
        out_time = time_diff_calculator(3, time(8, 0, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2215(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2216(self):
        out_time = time_diff_calculator(3, time(8, 0, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2217(self):
        out_time = time_diff_calculator(5, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2218(self):
        out_time = time_diff_calculator(3.5, time(7, 30, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2219(self):
        out_time = time_diff_calculator(4.5, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test2220(self):
        out_time = time_diff_calculator(3, time(8, 1, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2221(self):
        out_time = time_diff_calculator(3, time(8, 1, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2222(self):
        out_time = time_diff_calculator(5, time(11, 59, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2223(self):
        out_time = time_diff_calculator(3, time(8, 0, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2224(self):
        out_time = time_diff_calculator(5, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2225(self):
        out_time = time_diff_calculator(3, time(8, 0, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2226(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2227(self):
        out_time = time_diff_calculator(3, time(8, 1, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2228(self):
        out_time = time_diff_calculator(4.5, time(12, 0, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test2229(self):
        out_time = time_diff_calculator(3.6, time(7, 35, 0))
        self.assertEquals(out_time, time(11, 12, 0))
    
    def test2230(self):
        out_time = time_diff_calculator(4.9, time(12, 8, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2231(self):
        out_time = time_diff_calculator(4, time(7, 59, 0))
        self.assertEquals(out_time, time(12, 0, 0))
    
    def test2232(self):
        out_time = time_diff_calculator(3, time(8, 0, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2233(self):
        out_time = time_diff_calculator(5, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2234(self):
        out_time = time_diff_calculator(3, time(8, 0, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2235(self):
        out_time = time_diff_calculator(5, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2236(self):
        out_time = time_diff_calculator(3.5, time(7, 31, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2237(self):
        out_time = time_diff_calculator(4.5, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test2238(self):
        out_time = time_diff_calculator(3, time(8, 0, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2239(self):
        out_time = time_diff_calculator(5, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2240(self):
        out_time = time_diff_calculator(3, time(7, 59, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2241(self):
        out_time = time_diff_calculator(5, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2242(self):
        out_time = time_diff_calculator(3, time(7, 49, 0))
        self.assertEquals(out_time, time(10, 48, 0))
    
    def test2243(self):
        out_time = time_diff_calculator(5, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2244(self):
        out_time = time_diff_calculator(3, time(7, 59, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2245(self):
        out_time = time_diff_calculator(5, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2246(self):
        out_time = time_diff_calculator(3, time(7, 59, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2247(self):
        out_time = time_diff_calculator(5, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2248(self):
        out_time = time_diff_calculator(3, time(8, 0, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2249(self):
        out_time = time_diff_calculator(5, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2250(self):
        out_time = time_diff_calculator(3, time(7, 59, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2251(self):
        out_time = time_diff_calculator(5, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2252(self):
        out_time = time_diff_calculator(3, time(7, 59, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2253(self):
        out_time = time_diff_calculator(5, time(11, 59, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2254(self):
        out_time = time_diff_calculator(3, time(7, 33, 0))
        self.assertEquals(out_time, time(10, 36, 0))
    
    def test2255(self):
        out_time = time_diff_calculator(4.1, time(12, 51, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2256(self):
        out_time = time_diff_calculator(3.5, time(7, 33, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test2257(self):
        out_time = time_diff_calculator(2.4, time(11, 36, 0))
        self.assertEquals(out_time, time(14, 0, 0))
    
    def test2258(self):
        out_time = time_diff_calculator(1.4, time(15, 38, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2259(self):
        out_time = time_diff_calculator(3.4, time(7, 37, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2260(self):
        out_time = time_diff_calculator(5, time(11, 59, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2261(self):
        out_time = time_diff_calculator(3.5, time(7, 27, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2262(self):
        out_time = time_diff_calculator(4.5, time(12, 0, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test2263(self):
        out_time = time_diff_calculator(3, time(8, 0, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2264(self):
        out_time = time_diff_calculator(5, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2265(self):
        out_time = time_diff_calculator(3.5, time(7, 34, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test2266(self):
        out_time = time_diff_calculator(4.5, time(12, 0, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test2267(self):
        out_time = time_diff_calculator(0, time(7, 35, 0))
        self.assertEquals(out_time, time(7, 36, 0))
    
    def test2268(self):
        out_time = time_diff_calculator(3.1, time(7, 59, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test2269(self):
        out_time = time_diff_calculator(4.9, time(12, 7, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2270(self):
        out_time = time_diff_calculator(3, time(7, 59, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2271(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2272(self):
        out_time = time_diff_calculator(3, time(8, 0, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2273(self):
        out_time = time_diff_calculator(5, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2274(self):
        out_time = time_diff_calculator(3, time(7, 59, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2275(self):
        out_time = time_diff_calculator(5, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2276(self):
        out_time = time_diff_calculator(3, time(7, 41, 0))
        self.assertEquals(out_time, time(10, 42, 0))
    
    def test2277(self):
        out_time = time_diff_calculator(5.1, time(12, 55, 0))
        self.assertEquals(out_time, time(18, 0, 0))
    
    def test2278(self):
        out_time = time_diff_calculator(4.1, time(7, 23, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test2279(self):
        out_time = time_diff_calculator(6.1, time(12, 0, 0))
        self.assertEquals(out_time, time(18, 6, 0))
    
    def test2280(self):
        out_time = time_diff_calculator(3.4, time(7, 36, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2281(self):
        out_time = time_diff_calculator(5.7, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test2282(self):
        out_time = time_diff_calculator(5.3, time(7, 39, 0))
        self.assertEquals(out_time, time(13, 0, 0))
    
    def test2283(self):
        out_time = time_diff_calculator(3.9, time(13, 30, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test2284(self):
        out_time = time_diff_calculator(9.1, time(7, 44, 0))
        self.assertEquals(out_time, time(16, 48, 0))
    
    def test2285(self):
        out_time = time_diff_calculator(3.2, time(7, 46, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2286(self):
        out_time = time_diff_calculator(5.6, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test2287(self):
        out_time = time_diff_calculator(3.4, time(7, 36, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2288(self):
        out_time = time_diff_calculator(7.4, time(11, 30, 0))
        self.assertEquals(out_time, time(18, 54, 0))
    
    def test2289(self):
        out_time = time_diff_calculator(5.3, time(7, 47, 0))
        self.assertEquals(out_time, time(13, 6, 0))
    
    def test2290(self):
        out_time = time_diff_calculator(5.2, time(13, 33, 0))
        self.assertEquals(out_time, time(18, 48, 0))
    
    def test2291(self):
        out_time = time_diff_calculator(3.3, time(7, 41, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2292(self):
        out_time = time_diff_calculator(5.7, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test2293(self):
        out_time = time_diff_calculator(3.3, time(7, 40, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2294(self):
        out_time = time_diff_calculator(5.7, time(11, 30, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test2295(self):
        out_time = time_diff_calculator(3.8, time(9, 56, 0))
        self.assertEquals(out_time, time(13, 42, 0))
    
    def test2296(self):
        out_time = time_diff_calculator(3.5, time(7, 31, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2297(self):
        out_time = time_diff_calculator(5.6, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test2298(self):
        out_time = time_diff_calculator(3.4, time(7, 35, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2299(self):
        out_time = time_diff_calculator(5.5, time(11, 32, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2300(self):
        out_time = time_diff_calculator(3.3, time(7, 41, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2301(self):
        out_time = time_diff_calculator(5.7, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test2302(self):
        out_time = time_diff_calculator(3.4, time(7, 34, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2303(self):
        out_time = time_diff_calculator(5.7, time(11, 32, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test2304(self):
        out_time = time_diff_calculator(3.4, time(7, 46, 0))
        self.assertEquals(out_time, time(11, 12, 0))
    
    def test2305(self):
        out_time = time_diff_calculator(5.1, time(11, 59, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test2306(self):
        out_time = time_diff_calculator(3.2, time(7, 48, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2307(self):
        out_time = time_diff_calculator(5, time(11, 59, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2308(self):
        out_time = time_diff_calculator(5.3, time(7, 31, 0))
        self.assertEquals(out_time, time(12, 48, 0))
    
    def test2309(self):
        out_time = time_diff_calculator(3.4, time(13, 19, 0))
        self.assertEquals(out_time, time(16, 42, 0))
    
    def test2310(self):
        out_time = time_diff_calculator(3.5, time(7, 31, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2311(self):
        out_time = time_diff_calculator(5.5, time(11, 30, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2312(self):
        out_time = time_diff_calculator(4.5, time(7, 38, 0))
        self.assertEquals(out_time, time(12, 6, 0))
    
    def test2313(self):
        out_time = time_diff_calculator(3.4, time(7, 35, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2314(self):
        out_time = time_diff_calculator(5.5, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2315(self):
        out_time = time_diff_calculator(3.2, time(7, 50, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2316(self):
        out_time = time_diff_calculator(5.3, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test2317(self):
        out_time = time_diff_calculator(3, time(8, 1, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2318(self):
        out_time = time_diff_calculator(4.6, time(12, 0, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test2319(self):
        out_time = time_diff_calculator(3.3, time(7, 44, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2320(self):
        out_time = time_diff_calculator(2.7, time(11, 31, 0))
        self.assertEquals(out_time, time(14, 12, 0))
    
    def test2321(self):
        out_time = time_diff_calculator(3.2, time(7, 47, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2322(self):
        out_time = time_diff_calculator(5, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2323(self):
        out_time = time_diff_calculator(3.1, time(7, 59, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test2324(self):
        out_time = time_diff_calculator(4.9, time(12, 5, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2325(self):
        out_time = time_diff_calculator(3.1, time(8, 0, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test2326(self):
        out_time = time_diff_calculator(4.9, time(12, 3, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2327(self):
        out_time = time_diff_calculator(3.5, time(7, 46, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test2328(self):
        out_time = time_diff_calculator(4.6, time(12, 0, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test2329(self):
        out_time = time_diff_calculator(3, time(7, 59, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2330(self):
        out_time = time_diff_calculator(4.9, time(12, 3, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2331(self):
        out_time = time_diff_calculator(3, time(7, 59, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2332(self):
        out_time = time_diff_calculator(5, time(11, 59, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2333(self):
        out_time = time_diff_calculator(3.5, time(8, 0, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test2334(self):
        out_time = time_diff_calculator(4.5, time(12, 30, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2335(self):
        out_time = time_diff_calculator(3.5, time(7, 28, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2336(self):
        out_time = time_diff_calculator(4.5, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test2337(self):
        out_time = time_diff_calculator(3, time(8, 0, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2338(self):
        out_time = time_diff_calculator(5.1, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test2339(self):
        out_time = time_diff_calculator(3, time(7, 59, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2340(self):
        out_time = time_diff_calculator(4.9, time(12, 0, 0))
        self.assertEquals(out_time, time(16, 54, 0))
    
    def test2341(self):
        out_time = time_diff_calculator(1.2, time(7, 30, 0))
        self.assertEquals(out_time, time(8, 42, 0))
    
    def test2342(self):
        out_time = time_diff_calculator(2, time(9, 30, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test2343(self):
        out_time = time_diff_calculator(4.8, time(12, 20, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test2344(self):
        out_time = time_diff_calculator(3, time(7, 59, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2345(self):
        out_time = time_diff_calculator(5.6, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 36, 0))
    
    def test2346(self):
        out_time = time_diff_calculator(3.2, time(7, 48, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2347(self):
        out_time = time_diff_calculator(5.7, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 42, 0))
    
    def test2348(self):
        out_time = time_diff_calculator(3.4, time(7, 38, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2349(self):
        out_time = time_diff_calculator(5.7, time(11, 30, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test2350(self):
        out_time = time_diff_calculator(3.9, time(7, 36, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test2351(self):
        out_time = time_diff_calculator(5.7, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 42, 0))
    
    def test2352(self):
        out_time = time_diff_calculator(3.9, time(7, 35, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test2353(self):
        out_time = time_diff_calculator(3.5, time(11, 59, 0))
        self.assertEquals(out_time, time(15, 30, 0))
    
    def test2354(self):
        out_time = time_diff_calculator(2.3, time(15, 38, 0))
        self.assertEquals(out_time, time(17, 54, 0))
    
    def test2355(self):
        out_time = time_diff_calculator(4.5, time(12, 30, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2356(self):
        out_time = time_diff_calculator(3.4, time(7, 37, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2357(self):
        out_time = time_diff_calculator(5.6, time(11, 32, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test2358(self):
        out_time = time_diff_calculator(3.2, time(7, 47, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2359(self):
        out_time = time_diff_calculator(5.6, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test2360(self):
        out_time = time_diff_calculator(3.2, time(7, 49, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2361(self):
        out_time = time_diff_calculator(6.4, time(11, 30, 0))
        self.assertEquals(out_time, time(17, 54, 0))
    
    def test2362(self):
        out_time = time_diff_calculator(4.5, time(7, 32, 0))
        self.assertEquals(out_time, time(12, 0, 0))
    
    def test2363(self):
        out_time = time_diff_calculator(5.1, time(12, 54, 0))
        self.assertEquals(out_time, time(18, 0, 0))
    
    def test2364(self):
        out_time = time_diff_calculator(3.5, time(7, 28, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2365(self):
        out_time = time_diff_calculator(6.2, time(11, 30, 0))
        self.assertEquals(out_time, time(17, 42, 0))
    
    def test2366(self):
        out_time = time_diff_calculator(3.5, time(7, 42, 0))
        self.assertEquals(out_time, time(11, 12, 0))
    
    def test2367(self):
        out_time = time_diff_calculator(5.6, time(12, 2, 0))
        self.assertEquals(out_time, time(17, 36, 0))
    
    def test2368(self):
        out_time = time_diff_calculator(3.2, time(7, 44, 0))
        self.assertEquals(out_time, time(10, 54, 0))
    
    def test2369(self):
        out_time = time_diff_calculator(6.1, time(11, 24, 0))
        self.assertEquals(out_time, time(17, 30, 0))
    
    def test2370(self):
        out_time = time_diff_calculator(3.3, time(7, 39, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2371(self):
        out_time = time_diff_calculator(5.6, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test2372(self):
        out_time = time_diff_calculator(3.6, time(7, 24, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2373(self):
        out_time = time_diff_calculator(6.5, time(11, 31, 0))
        self.assertEquals(out_time, time(18, 0, 0))
    
    def test2374(self):
        out_time = time_diff_calculator(2.8, time(11, 30, 0))
        self.assertEquals(out_time, time(14, 18, 0))
    
    def test2375(self):
        out_time = time_diff_calculator(3.4, time(7, 33, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2376(self):
        out_time = time_diff_calculator(5.6, time(11, 48, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test2377(self):
        out_time = time_diff_calculator(3.6, time(7, 27, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test2378(self):
        out_time = time_diff_calculator(6, time(11, 36, 0))
        self.assertEquals(out_time, time(17, 36, 0))
    
    def test2379(self):
        out_time = time_diff_calculator(3.5, time(7, 31, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2380(self):
        out_time = time_diff_calculator(7.2, time(11, 31, 0))
        self.assertEquals(out_time, time(18, 42, 0))
    
    def test2381(self):
        out_time = time_diff_calculator(10.2, time(7, 32, 0))
        self.assertEquals(out_time, time(17, 42, 0))
    
    def test2382(self):
        out_time = time_diff_calculator(3.3, time(7, 41, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2383(self):
        out_time = time_diff_calculator(6, time(11, 30, 0))
        self.assertEquals(out_time, time(17, 30, 0))
    
    def test2384(self):
        out_time = time_diff_calculator(3.3, time(7, 42, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2385(self):
        out_time = time_diff_calculator(5.3, time(11, 52, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test2386(self):
        out_time = time_diff_calculator(3.4, time(7, 36, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2387(self):
        out_time = time_diff_calculator(5.4, time(11, 31, 0))
        self.assertEquals(out_time, time(16, 54, 0))
    
    def test2388(self):
        out_time = time_diff_calculator(3.6, time(7, 21, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2389(self):
        out_time = time_diff_calculator(6.1, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 36, 0))
    
    def test2390(self):
        out_time = time_diff_calculator(3.6, time(7, 32, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test2391(self):
        out_time = time_diff_calculator(6, time(11, 35, 0))
        self.assertEquals(out_time, time(17, 36, 0))
    
    def test2392(self):
        out_time = time_diff_calculator(3.3, time(7, 41, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2393(self):
        out_time = time_diff_calculator(5.1, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test2394(self):
        out_time = time_diff_calculator(3.6, time(7, 27, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test2395(self):
        out_time = time_diff_calculator(5.7, time(11, 37, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test2396(self):
        out_time = time_diff_calculator(4.2, time(7, 34, 0))
        self.assertEquals(out_time, time(11, 48, 0))
    
    def test2397(self):
        out_time = time_diff_calculator(4.2, time(12, 54, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test2398(self):
        out_time = time_diff_calculator(3.4, time(7, 33, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2399(self):
        out_time = time_diff_calculator(5.8, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test2400(self):
        out_time = time_diff_calculator(3.5, time(7, 31, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2401(self):
        out_time = time_diff_calculator(5.9, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 24, 0))
    
    def test2402(self):
        out_time = time_diff_calculator(3.6, time(7, 31, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test2403(self):
        out_time = time_diff_calculator(5.5, time(11, 37, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test2404(self):
        out_time = time_diff_calculator(3.3, time(7, 37, 0))
        self.assertEquals(out_time, time(10, 54, 0))
    
    def test2405(self):
        out_time = time_diff_calculator(5.2, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test2406(self):
        out_time = time_diff_calculator(4.8, time(7, 45, 0))
        self.assertEquals(out_time, time(12, 36, 0))
    
    def test2407(self):
        out_time = time_diff_calculator(3.9, time(13, 7, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2408(self):
        out_time = time_diff_calculator(3.8, time(7, 44, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test2409(self):
        out_time = time_diff_calculator(5.2, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test2410(self):
        out_time = time_diff_calculator(3.4, time(7, 34, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2411(self):
        out_time = time_diff_calculator(5.5, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2412(self):
        out_time = time_diff_calculator(3.2, time(7, 39, 0))
        self.assertEquals(out_time, time(10, 54, 0))
    
    def test2413(self):
        out_time = time_diff_calculator(5.2, time(12, 2, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test2414(self):
        out_time = time_diff_calculator(3, time(8, 0, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2415(self):
        out_time = time_diff_calculator(5, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2416(self):
        out_time = time_diff_calculator(3, time(8, 1, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2417(self):
        out_time = time_diff_calculator(4.9, time(11, 51, 0))
        self.assertEquals(out_time, time(16, 48, 0))
    
    def test2418(self):
        out_time = time_diff_calculator(3.5, time(7, 30, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2419(self):
        out_time = time_diff_calculator(4.6, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test2420(self):
        out_time = time_diff_calculator(3.1, time(7, 59, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test2421(self):
        out_time = time_diff_calculator(4.9, time(12, 3, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2422(self):
        out_time = time_diff_calculator(3, time(7, 57, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2423(self):
        out_time = time_diff_calculator(5, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2424(self):
        out_time = time_diff_calculator(3.2, time(7, 49, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2425(self):
        out_time = time_diff_calculator(4.8, time(12, 6, 0))
        self.assertEquals(out_time, time(16, 54, 0))
    
    def test2426(self):
        out_time = time_diff_calculator(3, time(7, 50, 0))
        self.assertEquals(out_time, time(10, 48, 0))
    
    def test2427(self):
        out_time = time_diff_calculator(5, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2428(self):
        out_time = time_diff_calculator(2.4, time(7, 59, 0))
        self.assertEquals(out_time, time(10, 24, 0))
    
    def test2429(self):
        out_time = time_diff_calculator(5.6, time(11, 25, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2430(self):
        out_time = time_diff_calculator(3, time(7, 0, 0))
        self.assertEquals(out_time, time(10, 0, 0))
    
    def test2431(self):
        out_time = time_diff_calculator(6, time(11, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2432(self):
        out_time = time_diff_calculator(3, time(7, 59, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2433(self):
        out_time = time_diff_calculator(5, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2434(self):
        out_time = time_diff_calculator(3.1, time(7, 59, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test2435(self):
        out_time = time_diff_calculator(4.9, time(12, 3, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2436(self):
        out_time = time_diff_calculator(3.1, time(7, 59, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test2437(self):
        out_time = time_diff_calculator(4.9, time(12, 5, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2438(self):
        out_time = time_diff_calculator(3.3, time(7, 42, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2439(self):
        out_time = time_diff_calculator(4.7, time(12, 0, 0))
        self.assertEquals(out_time, time(16, 42, 0))
    
    def test2440(self):
        out_time = time_diff_calculator(3, time(7, 45, 0))
        self.assertEquals(out_time, time(10, 48, 0))
    
    def test2441(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2442(self):
        out_time = time_diff_calculator(3, time(8, 0, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2443(self):
        out_time = time_diff_calculator(5, time(12, 3, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test2444(self):
        out_time = time_diff_calculator(3, time(7, 58, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2445(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2446(self):
        out_time = time_diff_calculator(3, time(7, 59, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2447(self):
        out_time = time_diff_calculator(5, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2448(self):
        out_time = time_diff_calculator(3, time(7, 49, 0))
        self.assertEquals(out_time, time(10, 48, 0))
    
    def test2449(self):
        out_time = time_diff_calculator(5, time(11, 59, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2450(self):
        out_time = time_diff_calculator(3, time(7, 59, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2451(self):
        out_time = time_diff_calculator(4.4, time(12, 36, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2452(self):
        out_time = time_diff_calculator(3, time(7, 56, 0))
        self.assertEquals(out_time, time(10, 54, 0))
    
    def test2453(self):
        out_time = time_diff_calculator(4, time(12, 0, 0))
        self.assertEquals(out_time, time(16, 0, 0))
    
    def test2454(self):
        out_time = time_diff_calculator(3.5, time(7, 28, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2455(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2456(self):
        out_time = time_diff_calculator(3.5, time(7, 30, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2457(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2458(self):
        out_time = time_diff_calculator(3, time(7, 48, 0))
        self.assertEquals(out_time, time(10, 48, 0))
    
    def test2459(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2460(self):
        out_time = time_diff_calculator(3, time(7, 47, 0))
        self.assertEquals(out_time, time(10, 48, 0))
    
    def test2461(self):
        out_time = time_diff_calculator(5, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2462(self):
        out_time = time_diff_calculator(3.4, time(7, 31, 0))
        self.assertEquals(out_time, time(10, 54, 0))
    
    def test2463(self):
        out_time = time_diff_calculator(4.6, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test2464(self):
        out_time = time_diff_calculator(3, time(7, 55, 0))
        self.assertEquals(out_time, time(10, 54, 0))
    
    def test2465(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2466(self):
        out_time = time_diff_calculator(3, time(7, 58, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2467(self):
        out_time = time_diff_calculator(4.5, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 30, 0))
    
    def test2468(self):
        out_time = time_diff_calculator(3.3, time(7, 41, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2469(self):
        out_time = time_diff_calculator(5.2, time(11, 47, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2470(self):
        out_time = time_diff_calculator(3.3, time(7, 45, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test2471(self):
        out_time = time_diff_calculator(5, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2472(self):
        out_time = time_diff_calculator(3.7, time(7, 17, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2473(self):
        out_time = time_diff_calculator(4.2, time(11, 31, 0))
        self.assertEquals(out_time, time(15, 42, 0))
    
    def test2474(self):
        out_time = time_diff_calculator(2.8, time(7, 58, 0))
        self.assertEquals(out_time, time(10, 48, 0))
    
    def test2475(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2476(self):
        out_time = time_diff_calculator(3, time(8, 0, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2477(self):
        out_time = time_diff_calculator(5, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2478(self):
        out_time = time_diff_calculator(3.5, time(7, 58, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test2479(self):
        out_time = time_diff_calculator(6.9, time(12, 1, 0))
        self.assertEquals(out_time, time(18, 54, 0))
    
    def test2480(self):
        out_time = time_diff_calculator(8.3, time(8, 52, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test2481(self):
        out_time = time_diff_calculator(3.5, time(7, 28, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2482(self):
        out_time = time_diff_calculator(6.6, time(11, 31, 0))
        self.assertEquals(out_time, time(18, 6, 0))
    
    def test2483(self):
        out_time = time_diff_calculator(3.7, time(7, 37, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test2484(self):
        out_time = time_diff_calculator(5.9, time(11, 50, 0))
        self.assertEquals(out_time, time(17, 42, 0))
    
    def test2485(self):
        out_time = time_diff_calculator(2.8, time(11, 2, 0))
        self.assertEquals(out_time, time(13, 48, 0))
    
    def test2486(self):
        out_time = time_diff_calculator(3.5, time(7, 30, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2487(self):
        out_time = time_diff_calculator(5.3, time(11, 57, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test2488(self):
        out_time = time_diff_calculator(3.4, time(7, 38, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2489(self):
        out_time = time_diff_calculator(7.6, time(11, 32, 0))
        self.assertEquals(out_time, time(19, 6, 0))
    
    def test2490(self):
        out_time = time_diff_calculator(3.5, time(7, 32, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2491(self):
        out_time = time_diff_calculator(6.4, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 54, 0))
    
    def test2492(self):
        out_time = time_diff_calculator(6.2, time(12, 8, 0))
        self.assertEquals(out_time, time(18, 18, 0))
    
    def test2493(self):
        out_time = time_diff_calculator(3.3, time(7, 39, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2494(self):
        out_time = time_diff_calculator(5.1, time(11, 56, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2495(self):
        out_time = time_diff_calculator(3.3, time(7, 40, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2496(self):
        out_time = time_diff_calculator(5.5, time(11, 31, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2497(self):
        out_time = time_diff_calculator(3.4, time(7, 35, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2498(self):
        out_time = time_diff_calculator(4.6, time(12, 0, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test2499(self):
        out_time = time_diff_calculator(3, time(8, 0, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2500(self):
        out_time = time_diff_calculator(5, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2501(self):
        out_time = time_diff_calculator(4.1, time(6, 53, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2502(self):
        out_time = time_diff_calculator(3.9, time(12, 1, 0))
        self.assertEquals(out_time, time(15, 54, 0))
    
    def test2503(self):
        out_time = time_diff_calculator(3, time(7, 43, 0))
        self.assertEquals(out_time, time(10, 42, 0))
    
    def test2504(self):
        out_time = time_diff_calculator(5, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2505(self):
        out_time = time_diff_calculator(3, time(8, 0, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2506(self):
        out_time = time_diff_calculator(2.8, time(12, 0, 0))
        self.assertEquals(out_time, time(14, 48, 0))
    
    def test2507(self):
        out_time = time_diff_calculator(3.5, time(8, 0, 0))
        self.assertEquals(out_time, time(11, 30, 0))
    
    def test2508(self):
        out_time = time_diff_calculator(3, time(7, 59, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2509(self):
        out_time = time_diff_calculator(5, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2510(self):
        out_time = time_diff_calculator(1.8, time(7, 55, 0))
        self.assertEquals(out_time, time(9, 42, 0))
    
    def test2511(self):
        out_time = time_diff_calculator(0.4, time(10, 33, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2512(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2513(self):
        out_time = time_diff_calculator(3, time(7, 59, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2514(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2515(self):
        out_time = time_diff_calculator(2.2, time(7, 58, 0))
        self.assertEquals(out_time, time(10, 12, 0))
    
    def test2516(self):
        out_time = time_diff_calculator(3, time(7, 59, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2517(self):
        out_time = time_diff_calculator(5.1, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test2518(self):
        out_time = time_diff_calculator(3.2, time(7, 55, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test2519(self):
        out_time = time_diff_calculator(4.8, time(12, 12, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2520(self):
        out_time = time_diff_calculator(3.1, time(7, 48, 0))
        self.assertEquals(out_time, time(10, 54, 0))
    
    def test2521(self):
        out_time = time_diff_calculator(4.9, time(12, 7, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2522(self):
        out_time = time_diff_calculator(3, time(7, 56, 0))
        self.assertEquals(out_time, time(10, 54, 0))
    
    def test2523(self):
        out_time = time_diff_calculator(5, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2524(self):
        out_time = time_diff_calculator(3.1, time(7, 52, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2525(self):
        out_time = time_diff_calculator(4.9, time(12, 7, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2526(self):
        out_time = time_diff_calculator(3, time(7, 53, 0))
        self.assertEquals(out_time, time(10, 54, 0))
    
    def test2527(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2528(self):
        out_time = time_diff_calculator(2.9, time(7, 51, 0))
        self.assertEquals(out_time, time(10, 48, 0))
    
    def test2529(self):
        out_time = time_diff_calculator(5.1, time(11, 59, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test2530(self):
        out_time = time_diff_calculator(3, time(8, 2, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2531(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2532(self):
        out_time = time_diff_calculator(1.8, time(7, 49, 0))
        self.assertEquals(out_time, time(9, 36, 0))
    
    def test2533(self):
        out_time = time_diff_calculator(5.2, time(11, 48, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2534(self):
        out_time = time_diff_calculator(3, time(7, 59, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2535(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2536(self):
        out_time = time_diff_calculator(3, time(7, 45, 0))
        self.assertEquals(out_time, time(10, 48, 0))
    
    def test2537(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2538(self):
        out_time = time_diff_calculator(3.2, time(7, 51, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test2539(self):
        out_time = time_diff_calculator(4.7, time(12, 7, 0))
        self.assertEquals(out_time, time(16, 48, 0))
    
    def test2540(self):
        out_time = time_diff_calculator(3.1, time(7, 53, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2541(self):
        out_time = time_diff_calculator(5, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2542(self):
        out_time = time_diff_calculator(3.3, time(8, 0, 0))
        self.assertEquals(out_time, time(11, 18, 0))
    
    def test2543(self):
        out_time = time_diff_calculator(4.7, time(12, 19, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2544(self):
        out_time = time_diff_calculator(3.2, time(7, 48, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2545(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2546(self):
        out_time = time_diff_calculator(2.9, time(7, 53, 0))
        self.assertEquals(out_time, time(10, 48, 0))
    
    def test2547(self):
        out_time = time_diff_calculator(4.9, time(12, 4, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2548(self):
        out_time = time_diff_calculator(3, time(7, 56, 0))
        self.assertEquals(out_time, time(10, 54, 0))
    
    def test2549(self):
        out_time = time_diff_calculator(5, time(12, 2, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2550(self):
        out_time = time_diff_calculator(3, time(8, 1, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2551(self):
        out_time = time_diff_calculator(5, time(12, 2, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2552(self):
        out_time = time_diff_calculator(4.4, time(7, 53, 0))
        self.assertEquals(out_time, time(12, 18, 0))
    
    def test2553(self):
        out_time = time_diff_calculator(3.7, time(13, 20, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2554(self):
        out_time = time_diff_calculator(3.2, time(7, 50, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2555(self):
        out_time = time_diff_calculator(4.7, time(12, 13, 0))
        self.assertEquals(out_time, time(16, 54, 0))
    
    def test2556(self):
        out_time = time_diff_calculator(1.1, time(7, 58, 0))
        self.assertEquals(out_time, time(9, 6, 0))
    
    def test2557(self):
        out_time = time_diff_calculator(3.5, time(7, 33, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test2558(self):
        out_time = time_diff_calculator(4.5, time(11, 37, 0))
        self.assertEquals(out_time, time(16, 6, 0))
    
    def test2559(self):
        out_time = time_diff_calculator(4.5, time(7, 57, 0))
        self.assertEquals(out_time, time(12, 30, 0))
    
    def test2560(self):
        out_time = time_diff_calculator(3.7, time(13, 31, 0))
        self.assertEquals(out_time, time(17, 12, 0))
    
    def test2561(self):
        out_time = time_diff_calculator(3.1, time(7, 59, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test2562(self):
        out_time = time_diff_calculator(2.9, time(11, 37, 0))
        self.assertEquals(out_time, time(14, 30, 0))
    
    def test2563(self):
        out_time = time_diff_calculator(0.5, time(16, 3, 0))
        self.assertEquals(out_time, time(16, 36, 0))
    
    def test2564(self):
        out_time = time_diff_calculator(3.1, time(7, 56, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2565(self):
        out_time = time_diff_calculator(5, time(12, 2, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2566(self):
        out_time = time_diff_calculator(4.5, time(8, 0, 0))
        self.assertEquals(out_time, time(12, 30, 0))
    
    def test2567(self):
        out_time = time_diff_calculator(3.2, time(13, 31, 0))
        self.assertEquals(out_time, time(16, 42, 0))
    
    def test2568(self):
        out_time = time_diff_calculator(3, time(7, 59, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2569(self):
        out_time = time_diff_calculator(5.3, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 18, 0))
    
    def test2570(self):
        out_time = time_diff_calculator(3.1, time(7, 55, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2571(self):
        out_time = time_diff_calculator(4.9, time(12, 3, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2572(self):
        out_time = time_diff_calculator(3, time(8, 0, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2573(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2574(self):
        out_time = time_diff_calculator(3, time(7, 57, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2575(self):
        out_time = time_diff_calculator(4.7, time(12, 1, 0))
        self.assertEquals(out_time, time(16, 42, 0))
    
    def test2576(self):
        out_time = time_diff_calculator(3, time(7, 51, 0))
        self.assertEquals(out_time, time(10, 54, 0))
    
    def test2577(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2578(self):
        out_time = time_diff_calculator(3.2, time(7, 59, 0))
        self.assertEquals(out_time, time(11, 12, 0))
    
    def test2579(self):
        out_time = time_diff_calculator(4.8, time(12, 15, 0))
        self.assertEquals(out_time, time(17, 6, 0))
    
    def test2580(self):
        out_time = time_diff_calculator(3, time(7, 53, 0))
        self.assertEquals(out_time, time(10, 54, 0))
    
    def test2581(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2582(self):
        out_time = time_diff_calculator(3.1, time(7, 59, 0))
        self.assertEquals(out_time, time(11, 6, 0))
    
    def test2583(self):
        out_time = time_diff_calculator(4.9, time(12, 7, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2584(self):
        out_time = time_diff_calculator(3.1, time(7, 55, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2585(self):
        out_time = time_diff_calculator(4.9, time(11, 59, 0))
        self.assertEquals(out_time, time(16, 54, 0))
    
    def test2586(self):
        out_time = time_diff_calculator(3, time(7, 59, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2587(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2588(self):
        out_time = time_diff_calculator(3, time(8, 2, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2589(self):
        out_time = time_diff_calculator(5, time(12, 1, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    
    def test2590(self):
        out_time = time_diff_calculator(3, time(7, 57, 0))
        self.assertEquals(out_time, time(11, 0, 0))
    
    def test2591(self):
        out_time = time_diff_calculator(5, time(12, 0, 0))
        self.assertEquals(out_time, time(17, 0, 0))
    