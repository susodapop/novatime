from datetime import time
import re

def parse_time(time_string):
	'''Accepts a string input and returns a datetime.time() object.
	Raises a ValueError for bad inputs. See tests for behavior definition.'''

	# Determine if AM/PM is contained. If its, keep the first instance found.

	ampm_pattern = re.compile(r"([AaPp][Mm])")
	ampm_match = re.search(ampm_pattern, time_string)
	if ampm_match:
		ampm = ampm_match[0]
	else:
		ampm = "Nope"

	# Clean the string. Remove everything but numbers and a colon

	bad_char_pattern = re.compile(r"[^:0-9]")
	working_time = re.sub(bad_char_pattern, "", time_string)


	# Confirm we have a good time format of either 7:15 or 715
	proper_time_pattern = re.compile(r"^((\d{1,2}:\d{1,2})|(\d{1,2}\d{1,2}))$")
	if not re.match(proper_time_pattern, working_time):
		raise ValueError(f"Inputs must take the format 000, 0000, 00:00 or 00:00AM/PM. Not '{working_time}'")
	
	# Check for colon separator
	colon_pattern = re.compile(r"[:]")
	colon_match = re.findall(colon_pattern, working_time)

	if colon_match:
		time_components = working_time.split(':')
		time_components = [int(i) for i in time_components]
	else:
		component_minute = int(working_time[-2:])
		component_hour = int(working_time[:len(working_time)-2])
		time_components = [component_hour, component_minute]

	if time_components[0] > 12 and ampm_match:
		raise ValueError("Military time cannot include an AM or PM.")

	if ampm.upper() == 'PM':
		if time_components[0] < 12:
			time_components[0] = (time_components[0] + 12)

	return time(time_components[0], time_components[1])