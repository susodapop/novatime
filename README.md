This application should perform the following rounding automatically.

| ﻿FROM    | TO      | ROUND   | TENTHS |
|---------|---------|---------|--------|
| 7:57 AM | 8:02 AM | 8:00 AM | 0      |
| 8:03 AM | 8:08 AM | 8:06 AM | 0.1    |
| 8:09 AM | 8:14 AM | 8:12 AM | 0.2    |
| 8:15 AM | 8:20 AM | 8:18 AM | 0.3    |
| 8:21 AM | 8:26 AM | 8:24 AM | 0.4    |
| 8:27 AM | 8:32 AM | 8:30 AM | 0.5    |
| 8:33 AM | 8:38 AM | 8:36 AM | 0.6    |
| 8:39 AM | 8:44 AM | 8:42 AM | 0.7    |
| 8:45 AM | 8:50 AM | 8:48 AM | 0.8    |
| 8:51 AM | 8:56 AM | 8:54 AM | 0.9    |
| 8:57 AM | 9:02 AM | 9:00 AM | 1      |


To use the CLI

> $> from main import human_readable
> $> from datetime import time
> 
> $> human_readable(1.2, time(8,0,0))
> 
> You should clock out between 09:09:00 and 09:14:00.

Upcoming Features:
* ~~CLI that accepts human readable time entry~~
* ~~Function to parse time entries~~
* ~~Error handling for non-time inputs~~
* ~~Input confirmation~~
* ~~Error handling for intervals across days~~
* Graphical interface for use on Windows