from datetime import time
from time_parse import parse_time
import datetime
import unittest

def time_rounder(input_time):
    '''Accepts a time() object.
    Returns a time() object rounded to the nearest tenth of an hour'''

    whole_tenths = input_time.minute // 6
 
    # If the input value is 2+ minutes higher than an even tenth, Novatime
    # rounds to the next highest tenth.
    if input_time.minute % 6 > 2:
        whole_tenths += 1

    if whole_tenths == 10:
        results_hour = (input_time.hour + 1) % 24
        results_minute = 0
    else:
        results_hour = input_time.hour
        results_minute = whole_tenths * 6

    return time(hour=results_hour, minute=results_minute, second=0)


def time_diff_calculator(hours_needed,
                         current_time=datetime.datetime.now().time):
    '''Returns a time rounded time() object using time_rounder()'''

    # Hours to add is easy to get. Just take the non decimal portion of 
    # user entry.
    hours_to_add = hours_needed // 1

    # Multiply the decimal portion by 60 to get minutes needed
    minutes_to_add = int(round((hours_needed % 1) * 60, 2))

    # Always round to the next minute. If we need 1 minute and 1 second then
    # we really need 2 minutes.
    if minutes_to_add % 1 != 0:
        minutes_to_add = (minutes_to_add // 1) + 1

    # Handle the case where we cross over into a new hour.
    if minutes_to_add + current_time.minute >= 60:
        hours_to_add += 1
        minutes_to_add -= 60

    return time_rounder(time(current_time.hour + int(hours_to_add),
                             current_time.minute + int(minutes_to_add)))


def time_window(input_time):
    '''Accepts a time() object and returns the 6 minute interval that surrounds
    that time() object.
    '''

    rounded = time_rounder(input_time)

    upper_hour = rounded.hour
    upper_minute = rounded.minute + 2
    lower_hour = rounded.hour
    lower_minute = rounded.minute - 3

    if rounded.minute == 0:
        lower_hour -= 1
        lower_minute += 60

    return (time(lower_hour,lower_minute,0),
            time(upper_hour,upper_minute,0))


def main_loop():

    while True:
        print("\n\n")
        input_time = input("What time did you clock in? ")
        try:
            clock_in_time = parse_time(input_time)
        except ValueError:
            print(f"Sorry, we could not parse {input_time} as a time. Please try again.")
            continue
        needed = input("How many hours do you need to work? ")

        try:
            target_time = time_diff_calculator(float(needed), clock_in_time)
        except ValueError:
            print("Sorry, this calculator cannot process shifts that begin on one date and end on another.")
            continue
        window = time_window(target_time)

        print('==================================================')
        print(f'Since you clocked in at {clock_in_time}, \n' +
                f'You should clock out between {window[0]} and {window[1]}')

        print('==================================================')

        again = input('\nWould you like to calculate again? (Y/N): ')
        if again.upper() == 'N':
            break


if __name__ == '__main__':
    main_loop()
