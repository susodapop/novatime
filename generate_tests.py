import csv
from jinja2 import Environment, FileSystemLoader, select_autoescape
from main import time_rounder
from datetime import time

env = Environment(
	loader=FileSystemLoader(searchpath='tests/templates/'),
	autoescape=select_autoescape(['html', 'xml']))

my_template = env.get_template('time_rounder_tests.py')

with open('tests/test_data/time_rounding_test_data.csv', 'r', encoding='utf-8-sig') as fp:
	reader = csv.reader(fp, delimiter=',', quotechar='|', )
	data = []
	for row in reader:
		data.append(row)

time_rounding_data = []
for row in data:
	trans_list = []
	for item in row:
		for number in item.split(':'):
			trans_list.append(int(number))
	time_rounding_data.append(trans_list)

with open('tests/test_time_rounder_generated.py', 'w') as fp:
	fp.write(my_template.render(data=time_rounding_data))

my_template = env.get_template('time_diff_tests.py')

data = []
with open('tests/test_data/time_diff_test_data.csv', 'r', encoding='utf-8-sig') as fp:
	reader = csv.reader(fp, delimiter=',', quotechar='|', )
	data = []
	for row in reader:
		data.append(row)

# Split data and load it into a big list
time_diff_data = []
for row in data:
	trans_list = []
	trans_list.append(row[0])
	for item in row[1:3]:
		for number in item.split(':'):
			trans_list.append(int(number))
	trans_list.append(row[3])
	time_diff_data.append(trans_list)


# Call time_rounder to convert the 'out times' into rounded values
for row in time_diff_data:
	rounded_time = time_rounder(time(hour=int(row[3]), minute=int(row[4])))
	row[3] = rounded_time.hour
	row[4] = rounded_time.minute


with open('tests/test_time_diff_generated.py', 'w') as fp:
	fp.write(my_template.render(data=time_diff_data))


